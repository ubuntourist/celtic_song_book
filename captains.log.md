# The Celtic Song Book

An attempt to convert a 331-page badly scanned document with musical scores into
something more useful...

## <a id="TOC"></a>Table of Contents

* [2021.09.05](#2021.09.05)
* [2021.09.06](#2021.09.06)
* [2021.09.12](#2021.09.12)
* [2021.10.26](#2021.10.26)
* [2021.10.27](#2021.10.27)
* [2021.11.01](#2021.11.01)
* [2021.11.05](#2021.11.05)
* [2021.11.10](#2021.11.10)
* [2021.11.12](#2021.11.12)
* [2021.11.16](#2021.11.16)
* [2021.11.30](#2021.11.30)
* [2021.12.02](#2021.12.02)
* [2021.12.08](#2021.12.08)
* [2022.02.16](#2022.02.16)
* [2022.04.20](#2022.04.20)

----

## <a id="2021.09.05"></a>[2021.09.05 (KJC)](#TOC)

```
$ sphinx-quickstart
Welcome to the Sphinx 4.1.2 quickstart utility.

Please enter values for the following settings (just press Enter to
accept a default value, if one is given in brackets).

Selected root path: .

You have two options for placing the build directory for Sphinx output.
Either, you use a directory "_build" within the root path, or you
separate "source" and "build" directories within the root path.
> Separate source and build directories (y/n) [n]: y

The project name will occur in several places in the built documentation.
> Project name: The Celtic Song Book
> Author name(s): Alfred Perceval Graves
> Project release []: 0.1

If the documents are to be written in a language other than English,
you can select a language here by its language code. Sphinx will then
translate text that it generates into that language.

For a list of supported codes, see
https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-language.
> Project language [en]:

Creating file /home/kjcole/gits/Audio/celtic_song_book/source/conf.py.
Creating file /home/kjcole/gits/Audio/celtic_song_book/source/index.rst.
Creating file /home/kjcole/gits/Audio/celtic_song_book/Makefile.
Creating file /home/kjcole/gits/Audio/celtic_song_book/make.bat.

Finished: An initial directory structure has been created.

You should now populate your master file
/home/kjcole/gits/Audio/celtic_song_book/source/index.rst and create
other documentation source files. Use the Makefile to build the docs,
like so:
   make builder
where "builder" is one of the supported builders, e.g. html, latex or
linkcheck.
```

----

## <a id="2021.09.06"></a>[2021.09.06 (KJC)](#TOC)

As luck would have it, there's a [**Lilypond**
extension](https://sphinx-notes.github.io/lilypond/) --
`sphinxnotes.lilypond` -- for **Sphinx**.

----

## <a id="2021.09.12"></a>[2021.09.12 (KJC)](#TOC)

* LilyPond sees:

```
\relative c'' {
  c4 a b c
}
```

* ...and fills it out internally to become:

```
\book {
  \score {
    \new Staff {
      \new Voice {
        \relative c'' {
          c4 a b c
        }
      }
    }
    \layout { }
  }
}
```

----

## <a id="2021.10.26"></a>[2021.10.26 (KJC)](#TOC)

There's a lot of stuff I've neglected to put here, and thus have
probably forgotten lots of important bits. But before I forget
another:

* [LilyPond MIDI
  Instruments](https://lilypond.org/doc/v2.22/Documentation/notation/midi-instruments)

* Moving the tempo text to the right

```
\override Score.MetronomeMark.X-offset = X
```

----

## <a id="2021.10.27"></a>[2021.10.27 (KJC)](#TOC)

* To create a link to a specific page of the Internet Archive PDF,
  replace `n21` with the page number:

```
https://archive.org/details/the-celtic-song-book/page/n21/mode/1up
```

----

## <a id="2021.11.01"></a>[2021.11.01 (KJC)](#TOC)

* The default instrument is **piano** but **violin** sounded much better,
  and **flute** better still.  However, for variety, some tunes may get
  **orchestral harp**.

* I may add an explicit instrument name in the printed scores, though that
  would go against my initial intent.

----

## <a id="2021.11.05"></a>[2021.11.05 (KJC)](#TOC)

* `\\acciaccatura` is a particular kind of `\\grace` *grace note*. See
  [LilyPond: Special rhythmic concers - Grace
  notes](https://lilypond.org/doc/v2.20/Documentation/notation/special-rhythmic-concerns#grace-notes)

* The vague tempo information in the songs is really annoying. To get a
  vague sense of BPM for those vague phrases:

  + [Common Tempo Markings In
    Music](https://theonlinemetronome.com/blogs/12/tempo-markings-defined)

  + [Wikipedia: Tempo](https://en.wikipedia.org/wiki/Tempo)

----

## <a id="2021.11.10"></a>[2021.11.10 (KJC)](#TOC)

* In `shule_agra.ly` I used `\acciaccatura` grace notes. In
  retrospect, I believe I should have used `\appoggiatura` which I
  used in `kitty_coleraine.ly` (I also used `\afterGrace` in
  `kitty_coleraine.ly`.)

* `-!` means `\staccatissimo` which puts a little vertical single
  quote mark above the note. Dunno what it means, but presumably
  something like staccato.

----

## <a id="2021.11.12"></a>[2021.11.12 (KJC)](#TOC)

* Lilypond's default paper size is "a4". A4 = 210 mm * 297 mm = 8.27
  in * 11.69 in.  Sadly, some songs go over a page, and the Sphinx
  extension for Lilypond makes two images and scales one of them
  incorrectly, making the text on the second page too large.

  There may be a better solution, but for now, making the page length
  longer seems to work using `set! paper-alist` and `set-paper-size`
  as shown below:

```
#(set! paper-alist
  (cons '("long scroll" . (cons (* 210 ) (* 332 mm))) paper-alist))

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null

  #(set-paper-size "long scroll")
  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}
```

----

## <a id="2021.11.16"></a>[2021.11.16 (KJC)](#TOC)

* Because the list of songs is getting so long, I'm thinking about
  finding a way to collapse the nested listings... [StackOverflow:
  Sphinx, reStructuredText show/hide code
  snippets](https://stackoverflow.com/a/25543713/447830) offers some
  hope in this regard...

----

## <a id="2021.11.30"></a>[2021.11.30 (KJC)](#TOC)

* Attempting to get OpenGraph and Twitter Cards to a reasonably
  working state via an extention and settings in `conf.py`...  To that
  end, a [Pan-Celtic
  flag](https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Banniel_Keltia.svg/1280px-Banniel_Keltia.svg.png)...

* The preferred aspect ratios for images is roughly 1:2 (height:width)

```
 "<meta property='og:image:width'  content='1024' />", # FB: 1200 (Twitter Card: 1024)
 "<meta property='og:image:height' content='768'  />", # FB:  630 (Twitter Card:  512)

```

----

## <a id="2021.12.02"></a>[2021.12.02 (KJC)](#TOC)

* Trivia: According to
  [Wikipedia](https://en.wikipedia.org/wiki/List_of_aspect_ratios_of_national_flags),
  the most common flag aspect ratio is 2:3 (used by 85 of 195
  sovereign states) followed by 1:2 used by 54 sovereign states). These
  ratios appear to be stated counter-intuitively as height:width.

* The Pan-Celtic flag, at least, based on the Wikipedia image, follows
  the 2:3 standard.

----

## <a id="2021.12.08"></a>[2021.12.08 (KJC)](#TOC)

* A nice Celtic font. I like it slightly better than the one offered
  by `apt`: [Celtica](https://fontlibrary.org/en/font/celtica)

----

## <a id="2022.02.16"></a>[2022.02.16 (KJC)](#TOC)

Well... It's been a while. Over two months.  The most recent `make`
changed considerably more than expected, due to a recent update /
upgrade of all my locally-installed pips (and maybe an upgrade of
LilyPond. I don't recall.)

Most of the changes were minor, involving one or more of:

* generation of hashed filenames for score PNGs
* changes in creation date / modification date
* references to the version of Sphinx used to generate the book

However, potentially more significant, due to the Sphinx upgrade, are
changes to CSS and JavaScript files generated by Sphinx, including
`basic.css`, `doctools.js`, `searchtools.js`, and `search.html`

----

## <a id="2022.04.20"></a>[2022.04.20 (KJC)](#TOC)

* Instead of trying to figure out the exact page height, there's a way
  to remove all page breaks.  Eliminate:

```
#(set! paper-alist
  (cons '("long scroll" . (cons (* 210 ) (* .. mm))) paper-alist))
```

> and change

```
 \paper {
   oddHeaderMarkup = \markup \null
   evenHeaderMarkup = \markup \null
   #(set-paper-size "long scroll")  % BUG: page too long for sphinxnotes-lilypond
```

> to

```
 \paper {
   oddHeaderMarkup = \markup \null
   evenHeaderMarkup = \markup \null
   page-breaking = #ly:one-page-breaking
```

* I have the resolution set to 600, and am generating single-page
  PNGs. In most cases this works fine. The code that the extension
  creates is something like:

```
$ lilypond -o /tmp/sphinxnotes-lilypondfvinrg_w/ \
           --formats png \
           -dresolution=600 \
           hundred_pipers.ly
```

> However, for `hundred_pipers.ly` it generates a PNG with a file size
> of **1,017,890** bytes. This appears to be too much for the
> SphinxNotes Lilypond extension. It generates a 115-byte stub of a
> PNG. If I use the default resolution (leaving the `-dresolution=600`
> out) the file size shrinks to **126,896**.

* Temporarily, I hope, changing `conf.py` to use the default
  resolution, 300. (I'd prefer to keep the resolution high.)

* Switching to SVGs and MP3s. MP3s are smaller than WAV files.  I had
  HOPED that switching to SVGs would do intelligent things.  Instead,
  it now crops the right and bottom of each image.

* Running Lilypond directly creates a proper, and uncropped SVG:

```
$ lilypond -o song_of_the_heather
           -dbackend=svg
           song_of_the_heather.ly
```

* However, running the extension creates a PNG, applies a base-64
  encoding to it, and then wraps SVG code around it. And does it
  wrong. It kind of defeats the purpose of the **Scalable** in SVG.
  Yeuch.

----
