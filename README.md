# The Celtic Song Book

## Last modified 2022.05.17

This repository contains the reStructuredText (.rst) and LilyPond
(.ly) sources for a transcription of [The Celtic Song
Book](https://archive.org/details/the-celtic-song-book/mode/2up).

This new HTML version contains playable audio (instrumental only) for
each tune.

## Requirements

**Debian (or other) packages**

* [LilyPond](https://lilypond.org/) (version 2.22.1)
* [TiMidity++](https://sourceforge.net/projects/timidity/) (version 2.14.0)
* [ImageMagick](https://imagemagick.org/index.php) (version 8:6.9.11.60)

**Python packages**

* [sphinx](https://www.sphinx-doc.org/en/master/) (version 4.1.2)
* [cloud-sptheme](https://cloud-sptheme.readthedocs.io/en/latest/) (version 1.10.1.post20200504175005)
* [sphinxnotes-lilypond](https://sphinx-notes.github.io/lilypond/) (version 1.1)

Older versions of all of the above may work fine. This is just what
I've got installed...

## Installation and building on Debian-based systems

**NOTE:** At the moment, I'm using **Pop! OS 20.04 LTS (focal)**.

Set your working directory to wherever you've cloned this repository
and type:

```
$ sudo apt install timidity lilypond imagemagick python3-pip
$ pip3 install --user -U sphinx
$ pip3 install --user -U cloud-sptheme
$ pip3 install --user -U sphinxnotes-lilypond
$ make html
```

Or, if you prefer a virtual environment, `requirements.txt` and
`Pipfile` are provided for users of `virtualenv` or `pipenv`,
replacing the three `pip3` commands above. Using `pipenv`:

```
$ sudo apt install timidity lilypond imagemagick python3-pip pipenv
$ pipenv shell
$ pipenv install
$ pipenv shell
$ make html
```

Then point your favorite web browser to `./build/html/index.html`.

----

-- *ubuntourist 2021.09.09*
