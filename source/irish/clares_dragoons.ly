% Clare's Dragoons
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.02 (kjc)
%
% Source: The Celtic Song Book, p. 44
%         https://archive.org/details/the-celtic-song-book/page/n25/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Clare's Dragoons"
  poet = \markup { \smallCaps { "Thomas Davis." } }
  composer = \markup { \italic { "Air:" } "\"Vive là!\"" }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key a \major
  \time 2/4
  \tempo \markup { \italic "In march time." } 4 = 120
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 e'8^\f([ d8])                               | %   0
    cs8 e8 e8 fs8                                          | %   1
    a8. b16 a8 e8                                          | %   2
    cs8 e8 e8 cs'8                                         | %   3

    \break

    b8 a8 fs8 e8                                           | %   4
    cs8 e8 e8 fs16([ gs16])                                | %   5
    a8. b16 a16([ b16]) cs16([ d16])                       | %   6

    \break

    e8 e,8 e8 cs'8                                         | %   7
    b4 a8 b8                                               | %   8
    cs8 e8 b8 cs8                                          | %   9

    \break

    a8. b16 cs8 a8                                         | %  10
    cs8 e8 b8 cs8                                          | %  11
    d16([ cs16]) b16([ a16]) fs8 a8                        | %  12

    \break

    cs8 e8 b8 cs8                                          | %  13
    a8. b16 a16([ b16]) cs16([ d16])                       | %  14
    e8 e,8 e8 cs'8                                         | %  15

    \break

    b4 a8 r8                                               | %  16
    cs,8^\ff e8 e8. fs16                                   | %  17
    a8. b16 a4                                             | %  18

    \break

    cs,8 e8 e8 cs'8                                        | %  19
    b8 a8 fs8 e8                                           | %  20
    cs8 e8 e8 fs16([ gs16])                                | %  21

    \break

    a8. b16 a16([ b16]) cs16 d16                           | %  22
    e8 e,8 e8 cs'8                                         | %  23
    b4 a8 r8\fermata                                       | %  24

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  When                                         | %   0
  on Ram -- il -- lies'                        | %   1
  blood -- y field The                         | %   2
  baf -- fled French were                      | %   3
  forced to yield, The                         | %   4
  vic -- tor Sax -- on                         | %   5
  back -- ward reel'd  Be --                   | %   6
  fore the charge of                           | %   7
  Clare's men. The                             | %   8
  flags we con -- quer'd                       | %   9
  in that fray Look                            | %  10
  lone in Y -- pres'                           | %  11
  choir they say: We'll                        | %  12
  win them com -- pa --                        | %  13
  ny to -- day Or                              | %  14
  brave -- ly die, like                        | %  15
  Clare's men.                                 | %  16
  Vi -- ve là! for                             | %  17
  Ire -- land's wrong,                         | %  18
  Vi -- ve là! for                             | %  19
  Ire -- land's right, And                     | %  20
  Vi -- ve là! in....                          | %  21
  bat -- tle throng For a                      | %  22
  Span -- ish steed and                        | %  23
  sa -- bre.                                   | %  24
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { Another Clare is here to lead, }
      \line { The worthy son of such a breed, }
      \line { The French expect some famous deed }
      \line { When Clare leads on his warriors. }
      \line { Our Colonel comes from Brian's race, }
      \line { His wounds are in his breast and face, }
      \line { The gap of danger's still his place, }
      \line { The foremost of his squadron. }
      \line { \hspace #4 { Vive là! etc. } }

      \vspace #0.8

      \line { Oh, comrades, think how Ireland pines }
      \line { For exiled lords and rifled shrines, }
      \line { Her dearest hope the ordered lines }
      \line { And bursting charge of Clare's men. }
      \line { Then fling your green flag to the sky, }
      \line { Be Limerick your battle cry, }
      \line { And charge till blood floats fetlock high }
      \line { Around the track of Clare's men. }
      \line { \hspace #4 { Vive là! etc. } }
    }
  }
}
