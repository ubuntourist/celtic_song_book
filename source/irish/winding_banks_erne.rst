.. Page 64 (PDF Page 36, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
The Winding Banks of Erne
#########################

.. lilyinclude:: ./winding_banks_erne.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 64
<https://archive.org/details/the-celtic-song-book/page/n35/mode/1up>`_)
