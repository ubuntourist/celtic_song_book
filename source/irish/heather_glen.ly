% The Heather Glen
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.10 (kjc)
%
% Source: The Celtic Song Book, p. 59
%         https://archive.org/details/the-celtic-song-book/page/n32/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Heather Glen"
  poet = \markup { \smallCaps { "George Sigerson." } }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"An Smachdaoin Crón.\"" }
      \line { \italic { "(The copper-coloured stick of tobacco.)" } }
    }
  }
  tagline = ""
% instrument = "instrument"
% subtitle = "subtitle"
% arranger = "arranger"
% copyright = "copyright"
% composer = "composer"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key g \major
  \time 2/4
  \tempo \markup { \italic "With fervour." } 4 = 100
% \override TupletBracket.tuplet-slur = ##t              % Not available til v. 2.21
  \override TupletBracket.bracket-visibility = ##t
% \tupletUp
}

melody = {
  \relative {
    \global
    \partial 8 d'8                                     | %   0
    g8 g8 b8. g16                                      | %   1
    b8 d4.                                             | %   2
    g,8 g8 b8 a16([ g16])                              | %   3
    fs4. a8                                            | %   4

    \break

    g8 g8 b8. g16                                      | %   5
    b8 d4 d8                                           | %   6
    e4. d16([ b16])                                    | %   7
    g4. a8                                             | %   8
    g4. d8                                             | %   9

    \break

    g8 g8 b8. g16                                      | %  10
    b8 d4.                                             | %  11
    g,8 g16 g16 b8 a16([ g16])                         | %  12
    fs8 a4.                                            | %  13

    \break

    g8 g16 g16 b8. g16                                 | %  14
    b8 d4.                                             | %  15
    e4. d16([ b16])                                    | %  16
    g4. a8                                             | %  17
    g4. \tuplet 3/2 { d'16[ e16 fs16] }                | %  18

    \break

    g8-! fs8-! e8-! fs8-!                              | %  19
    e4 d4                                              | %  20
    g8-! fs8-! e8-! fs8-!                              | %  21
    d4. \tuplet 3/2 { d16[ e16 fs16] }                 | %  22

    \break

    g8-! fs8-! e8-! d8-!                               | %  23
    b8 e4.\fermata                                     | %  24
    d8 d4 b8                                           | %  25
    g4. a8                                             | %  26
    g4.\fermata \tuplet 3/2 { d'16[ e16 fs16] }        | %  27

    \break

    g8-! fs8-! e8-! fs8-!                              | %  28
    e4 d8 \tuplet 3/2 { d16[ e16 fs16] }               | %  29
    g8-! fs8-! e8-! fs8-!                              | %  30

    \break

    e8 d4 \tuplet 3/2 { d16[ e16 fs16] }               | %  31
    g8-! fs8-! e8-! d8-!                               | %  32
    b8 e4.\fermata                                     | %  33
    d4. b8                                             | %  34
    g4. a8                                             | %  35
    g4.\fermata                                          %  36

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  There                                                | %   0
  blooms a bon -- nie                                  | %   1
  flow -- er,                                          | %   2
  Up the heath -- er                                   | %   3
  glen; Tho'                                           | %   4
  bright in sun, in                                    | %   5
  show -- er 'Tis                                      | %   6
  just as                                              | %   7
  bright a --                                          | %   8
  gain. I                                              | %   9
  nev -- er can pass                                   | %  10
  by it,                                               | %  11
  I nev -- er dar' go                                  | %  12
  nigh it,                                             | %  13
  My heart it won't be                                 | %  14
  qui -- et,                                           | %  15
  Up the                                               | %  16
  heath -- er                                          | %  17
  glen. Sing...                                        | %  18
  O, the bloom -- ing                                  | %  19
  heath -- er!                                         | %  20
  O the heath -- er                                    | %  21
  glen! Where                                          | %  22
  fair -- est fair -- ies                              | %  23
  gath -- er                                           | %  24
  To lure in                                           | %  25
  mor -- tal                                           | %  26
  men. I...                                            | %  27
  nev -- er can pass                                   | %  28
  by it, I...                                          | %  29
  nev -- er dar' go                                    | %  30
  nigh it, My...                                       | %  31
  heart it won't be                                    | %  32
  qui -- et                                            | %  33
  Up the                                               | %  34
  heath -- er                                          | %  35
  glen.                                                  %  36
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { There sings a bonnie linnet, }
        \line { \hspace #2 { Up the heather glen, } }
        \line { The voice has magic in it }
        \line { \hspace #2 { Too sweet for mortal men! } }
        \line { It brings joy doon before us, }
        \line { Wi' winsome mellow chorus, }
        \line { But flies far, too far, o'er us, }
        \line { \hspace #2 { Up the heather glen. } }
        \line { \hspace #4 { Sing, O! the blooming heather, etc. } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -26) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { O, might I pull the flower }
        \line { \hspace #2 { Tha's blooming in that glen, } }
        \line { Nae sorrows that could lower }
        \line { \hspace #2 { Would make me sad again! } }
        \line { And might I catch that linnet, }
        \line { My heart⸺my hope are in it! }
        \line { O, heaven itself I'd win it, }
        \line { \hspace #2 { Up the heather glen! } }
        \line { \hspace #4 { Sing, O! the blooming heather, etc. } }
      }
    }
  }
}
