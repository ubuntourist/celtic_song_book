.. Page 69 (PDF Page 38, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

############################
His Home and His Own Country
############################

.. lilyinclude:: ./his_home_his_own_country.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 69
<https://archive.org/details/the-celtic-song-book/page/n37/mode/1up>`_)
