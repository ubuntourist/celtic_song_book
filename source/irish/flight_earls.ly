% The Flight of the Earls
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.02 (kjc)
%
% Source: The Celtic Song Book, p. 43
%         https://archive.org/details/the-celtic-song-book/page/n24/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Flight of the Earls"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = \markup { \italic { "Air:" } "\"The Boys of Wexford.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 4/4
  \tempo \markup { \italic "In moderate time." } 4 = 114
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 c'4                           | %   0
    f4. g8 f8([ e8]) d8([ c8])               | %   1
    c'4. d8 c4 bf4                           | %   2

    \break

    a4 f4 a8([ g8]) f8([ e8])                | %   3
    d2. e4                                   | %   4
    f4. g8 f8([ e8]) d8([ c8])               | %   5

    \break

    c'4. d8 c4 bf4                           | %   6
    a4 g4 f8([ g8]) a8([ b8])                | %   7
    c2. c4^\mf                               | %   8

    \break

    d4. bf8 e4 d4                           | %   9
    c4. bf8 a4 c4                           | %  10

    \break

    \once \override Hairpin.shorten-pair = #'(0 . 2)
    bf4^\< a4 g4 f4\!                        | %  11
    d2. e4                                   | %  12
    f4. g8 f8([ e8]) d8([ c8])               | %  13

    \break

    c'4. d8 c4 bf4                           | %  14
    a4 f4 a8([ g8]) f8([ e8])                | %  15
    f2.\fermata                                %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  To                                      | %   0
  o -- ther shores _ a -- _               | %   1
  cross the sea We                        | %   2
  speed with swell -- _ ing _             | %   3
  sail; Yet                               | %   4
  still there lin -- _ gers _             | %   5
  on our lee A                            | %   6
  phan -- tom In -- _ nis -- _            | %   7
  fail. Oh,                               | %   8
  fear not, fear not,                     | %   9
  gen -- tle ghost, Your                  | %  10
  sons shall turn un --                   | %  11
  true! Tho'                              | %  12
  fain to fly _ your _                    | %  13
  love -- ly coast, They                  | %  14
  leave their hearts _ with _             | %  15
  you.                                      %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { As slowly into distance dim }
        \line { \hspace #2 { Your shadow sinks and dies, } }
        \line { So o'er the ocean's utmost rim }
        \line { \hspace #2 { Another realm shall rise; } }
        \line { New hills shall swell, new vales expand, }
        \line { \hspace #2 { New rivers winding flow } }
        \line { But could we for a foster land }
        \line { \hspace #2 { Your mother love forgo? } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -26) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { Shall mighty Espan's martial praise }
        \line { \hspace #2 { Our patriot pulse still, } }
        \line { And o'er your mem'rys fervent rays }
        \line { \hspace #2 { For ever cast a chill? } }
        \line { Oh no! we live for your relief, }
        \line { \hspace #2 { Till, home from alien earth, } }
        \line { We share the smile that gilds your grief, }
        \line { \hspace #2 { The tear that gems your mirth. } }
      }
    }
  }
}
