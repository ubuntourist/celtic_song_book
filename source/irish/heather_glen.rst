.. Page 59 (PDF Page 33, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################
The Heather Glen
################

.. lilyinclude:: ./heather_glen.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 59
<https://archive.org/details/the-celtic-song-book/page/n32/mode/1up>`_)
