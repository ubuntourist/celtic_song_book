.. Page 47 (PDF Page 27, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###################
The Little Red Lark
###################

.. lilyinclude:: ./little_red_lark.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 47
<https://archive.org/details/the-celtic-song-book/page/n26/mode/1up>`_)
