% Let Erin remember the Days of Old
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.10.27 (kjc)
%
% Source: The Celtic Song Book, p. 39
%         https://archive.org/details/the-celtic-song-book/page/n22/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Let Erin remember the Days of Old"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"The Little Red Fox.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 4/4
  \tempo \markup { \italic "In moderate time." } 4 = 114
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 bf4^\f                                              | %   0
    ef4 ef8. f16 g4 g8. af16                                       | %   1
    <<
      { \voiceOne bf8 bf8 bf4 }
      \new Voice
      { \voiceTwo bf4. bf8 }
    >>
    \oneVoice
    af4 g8 af8                                                     | %   2

    \break

    bf4 c4 g4 ef4                                                  | %   3
    f2 ef4 bf4                                                     | %   4
    <<
      { \voiceOne ef4 ef8 f8 }
      \new Voice
      { \voiceTwo ef8. ef16 ef8([ f8])}
    >>
    \oneVoice
    g4 g8([ af8])                                                  | %   5

    \break

    bf4( c8) bf8 af4 g8 af8                                        | %   6
    bf4( c8) bf8 af8([ g8]) f8([ ef8])                             | %   7

    \break

    f2 ef4^\markup { \line { \italic più \dynamic f  } } bf'8 bf8  | %   8
    ef4 ef4 ef8([ d8]) c8 bf8                                      | %   9

    \break

    c4 c4 bf4 g8 bf8                                               | %  10
    ef4
    <<
      { \voiceOne ef4 }
      \new Voice
      { \voiceTwo ef8 ef8 }
    >>
    \oneVoice
    ef8([ d8])
    <<
      { \voiceOne c8( [bf8]) }
      \new Voice
      { \voiceTwo c8 bf8 }
    >>
    \oneVoice                                                      | %  11

    \break

    c2 bf4 bf8 bf8                                                 | %  12
    ef4 ef4 ef8([ d8]) c8 bf8                                      | %  13

    \break

    c4 c4 bf4\fermata g8([ f8])                                    | %  14
    ef8([ f8]) g8 ef8 f8([ ef8]) f8 g8                             | %  15
    ef2 ef4\fermata                                                  %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Let                                       | %   0
  E -- rin re -- mem -- ber the             | %   1
  days _ \skip1 of old, Ere her             | %   2
  faith -- less sons be --                  | %   3
  trayed her; When                          | %   4
  Ma -- _ la -- chi wore the _              | %   5
  col -- lar of gold Which he               | %   6
  won from her proud _ in -- _              | %   7
  va -- der; When her                       | %   8
  Kings, with stan -- _ dard of             | %   9
  green un -- furl'd, Led the               | %  10
  Red -- Branch _  Knights _  to........ _  | %  11
  dan -- ger; Ere the                       | %  12
  em -- 'rald gem _ of the                  | %  13
  west -- ern world Was _                   | %  14
  set _ in the crown _ of a                 | %  15
  stran -- ger.                             | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { On Lough Neagh's bank as the fisherman strays,}
      \line { \hspace #2 { When the clear cold eve's declining, } }
      \line { He sees the round tow'rs of other days }
      \line { \hspace #2 { In the wave beneath him shining; } }
      \line { Thus shall mem'r'y often, in dreams sublime}
      \line { \hspace #2 { Catch a glimpse of the days that are over; } }
      \line { Thus sighing, look thro' the waves of time, }
      \line { \hspace #2 { For long-faded glories they cover. } }
    }
  }
}
