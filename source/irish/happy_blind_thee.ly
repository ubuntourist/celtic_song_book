% Happy 'tis, thou Blind, for Thee
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.09 (kjc)
%
% Source: The Celtic Song Book, p. 56
%         https://archive.org/details/the-celtic-song-book/page/n31/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Happy 'tis, thou Blind, for Thee"
  poet = \markup { "From the Irish by" \smallCaps { "Douglas Hyde." } }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"Callino Casturame.\"" }
      \line { \italic { "(Colleen oge asthore.)" } }
    }
  }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 6/8
  \tempo \markup { \italic "With feeling." } 4 = 80
}

melody = {
  \relative {
    \global
    e''4 e8 e8.([ d16]) e8                       | %   1
    f4 f8 e4.                                    | %   2
    d4 d8 d8.([ e16]) f8                         | %   3

    \break

    e8.([ d16]) e8 d4.                           | %   4
    e8.([ f16]) g8 g8([ f8]) c8                  | %   5
    b8.([ c16]) d8 d8([ b8]) g8                  | %   6

    \break

    g8([ c8]) c8 c8.([ d16]) e8                  | %   7
    d8.([ c16]) d8 c4.                           | %   8

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Hap -- py tis.. thou                           | %   1
  blind, for thee,                               | %   2
  That thou se -- est                            | %   3
  not.. our star;                                | %   4
  Couldst thou see.. but                         | %   5
  as.. we see.. her,                             | %   6
  Thou.. wouldst be... but                       | %   7
  as... we are.                                  | %   8
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { Once I pitied sightless men, }
      \line { \hspace #2 { I as then unscathed by sight; } }
      \line { Now I envy those who see not, }
      \line { \hspace #2 { They can be not hurt by light. } }

      \vspace #0.8

      \line { Woe who once has seen her please, }
      \line { \hspace #2 { And then sees her not each hour; } }
      \line { Woe for him her love-mesh binding, }
      \line { \hspace #2 { Whose unwinding passes power. } }
    }
  }
}
