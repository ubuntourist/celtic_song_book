.. Page 71 (PDF Page 39, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##########
Dublin Bay
##########

.. lilyinclude:: ./dublin_bay.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 71
<https://archive.org/details/the-celtic-song-book/page/n38/mode/1up>`_)
