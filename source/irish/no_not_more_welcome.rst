.. Page 61 (PDF Page 34, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

####################
No, not more Welcome
####################

.. lilyinclude:: ./no_not_more_welcome.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 61
<https://archive.org/details/the-celtic-song-book/page/n33/mode/1up>`_)
