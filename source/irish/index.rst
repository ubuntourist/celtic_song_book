.. Page 21 (PDF Page 14, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

###############
IRISH FOLK SONG
###############

A revival is "a living again," and suggests an active anterior
life. This certainly is true of Irish folk song. In Ireland's dim
traditionary dawn, music is reputed to have been introduced into the
country by the Tuatha Da Dannan, whom an early legend describes as
coming up from Greece along what is known as the Amber route, to the
mouth of the Elbe, across Lochlann, now Norway and Sweden; thence
across the centre of Alba or Scotland into Erin. And, remarkably
enough, inscriptions of the very same kind as are found upon the tombs
of these Da Dannan kings at New Grange are also to be met with, as
pointed out by Mr. George Coffey, the celebrated Irish antiquary, all
along the line of the Amber route, and in a belt of Norway and Sweden
and Scotland and in Ireland, and nowhere else in Europe.

What was the nature of the music that this early people brought with
them to Ireland? According to Dr. Petrie, our leading Irish musical
antiquary, it consisted of plough tunes, lamentation airs, and
lullabies. And these would be accounted for in the weird old folk tale
which describes how the harper of the Tuatha Da Dannans recovered his
magical harp from his Fomorian foes by playing upon it the *Goltree*
airs, which turned their fury to weeping, and the *Soontree* tunes
which sent them all to sleep, so enabling the Harper Uaithne to escape
unscathed with the Daghda's harp. But the old tale also states that
the harper played upon another of the feelings of the Fomorians, by
turning their weeping into laughing before they fell asleep, through
his performance of the *Gentree*, or mirth-provoking music. If the old
tale speaks truth, the class of Irish music which to this day is to be
heard upon the harp and violin, setting us all dancing or quick-stepping,
and raising our spirits as well as our toes and heels, is of very
early origin. And, indeed, this may be well believed by students of
the manners and customs of the early Irish who were not, as some of
our poets suggest, a merely mystical or melancholy people, but a
joyous and festive race |---| at any rate, in the intervals of hard
fighting.

Dr. Petrie points out that the Irish lullabies are curiously like in
character to Indian and Persian hush songs, and this would tend to
support the belief in their early Eastern origin. The plough tunes
similarly suggest a primeval origin by their character and intervals;
and this is true of some of the earlier laments, such as the "Return
from 'Fingal,'" or the realm of the Dublin Danes, by the victorious
Dalcassians, chanting the death dirges of Brian Boru and his son
Murrough who had just fallen at the battle of Clontarf. It may be here
mentioned that as a rule the Irish marches are quick-step marches. We
have a number of these, and Mr. Arthur Darley, our famous Irish
violinist, has been engaged in collecting them in a volume of Irish
clan marches, which should be exceedingly interesting to all our O's
and Mac's. These quick-step marches have been further quickened into
jig tunes, whether in 6/8 or 9/8 time, and clan marches may be
recovered through this dance medium.

Irish music was now in the hands both of the bards and the
ecclesiastics, and the national instrument was the harp of from thirty
to sixty strings. To this instrument the bards of the princes and
chieftains, even upon the battlefield, would recite the achievements
of his fathers as an incitement to his hereditary lord. It is stated,
indeed, that the bard thus chanted on the old Irish battlefield,
surrounded by a group of harpers who accompanied him almost with the
effect of a military band.

In the Fenian tales there is reference made to the "Dord," which would
appear to be a concerted cry or chorus |---| a cry of warning, if not
a war cry.

As early as the close of the sixth century we gather from a passage in
Adamnan's "Life of St. Columba" that the Irish monks sang canticles
in counterpoint. St. Cellach, a student of Bangor, co. Down, the name
implying "fair choir," or "chief choir," gave his name to the
monastery of St. Gaul in Switzerland, which became like that of
Bangor, a famous music school. Again, St. Mailduff, the Irish founder
of Mailduffsburgh or Malmesbury in England, flourished in 670 and
composed many beautiful hymns. I may add that Dr. Joyce told me some
years since that a Latin hymn by Sedulius, whose Irish name was Shiel,
is still sung at the Irish College in Rome to a very early Irish air,
probably contemporary with the sixth-century Latin hymn.

Ireland, indeed, at this time was full of music; for, besides the
harp, we had as musical instruments the war-pipes blown through the
mouth by marching pipers, not played as are our beautiful union pipes,
by the hand, the air being supplied by bellows held under the arm,
while the musician remains seated. Great sums of money were paid to
bards and minstrels for their songs. In those days "not worth a song"
had no meaning in Ireland. There were hereditary families of
minstrels, instrumental players, and singers, and their names have
come down to us, thus the surname Ward means bard; Cronin has to do
with the word *Cronawn* |---| the crooning of a song; Crotty is
connected with the Irish *Cruit*, the Welsh *Crwth*, the English
*Crowd*, and so forth.

Irish music was heard abundantly during the Crusades. Dante speaks
with admiration of the Irish harp, and, indeed, there is a chorus of
praise for Irish minstrelsy all through early and mediaeval times,
abroad and in this country, which may well be summed up by Drayton's
lines in his *Polyolbion*:

| The Irish I admire
| And still cleave to that lyre,
|       As our Muse's mother;
| And think till I expire
|       Apollo's such another.

When Henry VIII became overlord of Ireland, though not king, the Irish
harp was added to the English arms, and his daughter, Queen Elizabeth,
took the greatest pleasure in Irish music. Sir Henry Sydney, in a
letter to her in 1569, waxes enthusiastic over the dancing of Irish
jigs by the ladies of Galway, whom he describes as very beautiful,
magnificently dressed, and excellent dancers. This, as Dr. Grattan
Flood points out, disposes of the suggestion that the jig-dance was
borrowed from the Italians in the latter half of the seventeenth
century. Meantime Ireland had become the music-school of Scotland, and
to a large extent of North Wales. There had always been much passing
backwards and forwards of minstrels from the north of Ireland and that
part of Scotland which in early times had been conquered by Ulster
warriors, and Griffith ap Cynan, Prince of North Wales, had, through
his Irish mother and residence in Ireland, brought Irish minstrels and
bards over to his country. There is considerable dispute as to how far
North Wales was ever actually conquered by the Irish. Sir John Rhys
maintains that there is good evidence of this. Certainly somehow or
other, as will be mentioned later, there is a great deal in common
between what are believed to be the earliest Welsh airs and early
Irish ones.

Of Shakespeare and Irish music Dr. Grattan Flood has written an
interesting chapter in his *History of Irish Music*. There is no doubt
that Irish music was, as he states, much in vogue in England during
the sixteenth century, and was in favour at Court during the last
year of Queen Elizabeth's reign; for the Earl of Worcester writes on
September 9, 1602, to the Earl of Shrewsbury in these terms: "We are
frolic here in Court, much dancing in the Privy Chamber of country
dances before the Queen's Majesty, who is exceedingly pleased
therewith.  Irish tunes are at this time most pleasing."

What are these dances? They are referred to as the *Hey*, a country
dance or round, long known in the Irish Pale, and which is the origin
of the English round or country dance, according to Dr. Grattan Flood;
and *Trenchmore*, an Anglicized corruption of *Rinnce Mor* or the
*Rinnce Fada* |---| that is, the long dance, the *Hey* being danced in
a circle. One of the earliest *Heys* is stated by Sir John Hawkins to
be "Sellenger's Round," which Sir Anthony St. Leger, or Sellinger, saw
danced in Ireland in 1540, and brought back with him to England in
1548, where its popularity was so great that it was arranged by the
famous master, Dr. William Byrd. Two Irish tunes mentioned under
various names by Shakespeare had previously been identified by Malone,
Dr. Petrie, and others. Dr. Grattan Flood claims to prove that nine
others have been identified by him. "Callino custurame," the
Anglicized form for "Little girl of my heart," "Colleen oge asthore,"
and "Ducdane," meaning "Will you come?" or "Diuca Tu," are the earlier
pair of finds. Dr. Grattan Flood claims "The Chevalier," "Fortune my
Foe," "Peg a Ramsay," "Bonny Sweet Robin," and "Whoop, do me no harm,
good man," referred to in *A Winter's Tale* twice over, but better
known in Ireland as "Paddy Wack," and adapted by Moore to his melody
"While History's Muse." "Well-a-day," or "Essex's Last Good-night," is
also claimed by Dr. Grattan Flood, though I think somewhat doubtfully;
but "The Fading," mentioned in the fourth act of *A Winter's Tale*, is
by William Chappell's testimony the Irish dance tune of the *Rinnce
Fada*, a dance to this day called *The Faddy* in Cornwall. I have not
so much faith in Dr. Grattan Flood's claim to "Light o' Love" and
"Come o'er the bourn, Bessie, to me," but "Yellow stockings" would
appear to be the Irish "Cuma Liom" ("It is indifferent to me" or "I
don't care").  Moore set it to his Irish melody, "Fairest, put on
awhile."

We now pass through a period of stress and struggle in Ireland. Its
chieftains, in spite of notable rallies made by the O'Neills, Owen Roe
O'Donnell, and the Geraldines, the Norman Irish lords who became more
Irish than the Irish, had less and less time to devote to the poetical
and musical arts, and gradually, though very gradually, the Irish
bard, famous for the three feats of solemn, gay, and sleep-compelling
music, degenerated under the stress of the internecine conflicts
between Saxon and Gael in Ireland, into the strolling minstrel, and
finally into the itinerant piper or fiddler or the street ballad
singer.

The Irish Jacobite poems and songs, though one of them, the
"Blackbird," is of great musical beauty, do not, for very good
reasons, show that passionate attachment for the Stewart cause that
pulses through Lady Nairne's beautiful Scottish Jacobite lyrics.

But some of them, such as the "Slender Red Steed" and the "Dawning of
the Day," are full of patriotic fervour. Perhaps, however, the "Lament
of the Irish Maiden for her Lover," who has gone to serve the Stewart
cause abroad, which is found in various Anglo-Irish versions under the
titles "Shule Agra" or "Shule Aroon," or "I wish I were on yonder
Hill" is for passionate melancholy the best musical exemplification
that could be given of these Irish Jacobite songs.

We now come to an important epoch in Irish folk and national music
|---| that of the Granard and Belfast Meetings of harpers |---|
promoted with the object of reviving the taste for Irish music, which
had begun to decline during the Hanoverian period, under its German
musical influences. These meetings, which took place between the years
1792 and 1800, were very successful, and awoke in the distinguished
Belfast musician, Mr. Bunting, such an enthusiasm for Irish music that
he henceforth devoted his main efforts to its collection and
publication. Of the Belfast meeting he writes thus vividly: "All the
best of the old class of harpers, a race of men then nearly extinct,
and now gone for ever, were present: Hempson, O'Neill, Fanning, and
seven others, the least able of whom has not left his equal behind.
Hempson, who was more than a hundred years old at the time, realized
the antique picture drawn by Cambrensis and Galilei, for he played
with long crooked nails, the left hand above the right, and in his
performance 'the tinkling of the small wires under the deep notes of
the bass' was particularly thrilling."

"He was the only one who played the very old music of the country, and
this in a style of such finished excellence as persuaded me that the
praises of the old Irish harp in Cambrensis, Fuller, and others, were
no more than a just tribute to that admirable instrument and its then
professors."

Bunting's first collection, consisting of sixty-six hitherto
unpublished pieces, was brought out in 1796, and its success, combined
with the establishment of the Irish Harp Society in Belfast as a
consequence of the meeting of harpers in that city, attracted the
attention of Thomas Moore. He was at the time still a student at
Trinity College, Dublin, and it is recorded that when he played the
tune of the "Fox's Sleep" to his friend Robert Emmet, that young
patriot strode about the room exclaiming, "Heavens! what an air for an
army to march to!" Moore then set himself to work to write words to
Irish airs, chiefly derived from Bunting's collection, but had long to
go a-begging with the MSS. of his earliest Irish Melodies.

It may have been that English publishers of music, however ready to
own the beauty of the airs and their accompanying words, did not think
them likely to pay, or possibly regarded some of them as perilously
national for publication so soon after the Rebellion of '98. But Moore
eventually secured the support of a compatriot in Power, the
publisher, and the assistance of a still more important Irishman in
Sir John Stevenson, the arranger of the Irish Melodies; we know now
with what a remarkable result.

Power in his first announcement mentions the promise of assistance in
the work from "other literary characters" beside Moore, though he
does not specify them by name. But these writers would appear to have
given way to Moore, whose strong zeal for his share of the work is
shown in a letter to Stephenson of the year 1807, from which I quote
an important passage:

"Our national music has never been properly collected, and while the
composers of the Continent have enriched their operas with melodies
borrowed from Ireland, very often without even the honesty of
acknowledgment, we have left these treasures to a great degree
unclaimed and fugitive. Thus our airs, like too many of our
countrymen, have, for want of protection at home, passed into the
service of foreigners. But we are come, I hope, to a better period
of both Politics and Music; and how much they are connected, in
Ireland at least, appears too plainly in the tone of sorrow and
depression which characterizes most of our early songs"

"The task which you propose to me, of adapting words to these airs, is
by no means easy. The poet, who would follow the various sentiments
which they express, must feel and understand that rapid fluctuation of
spirits, that unaccountable mixture of gloom and levity, which
composes the character of my countrymen and has deeply tinged their
music. Even in their liveliest strains we find some melancholy note
intrude |---| some minor third or flat seventh |---| which throws its
shade as it passes and makes even mirth interesting. If Burns had been
an Irishman (and I would willingly give up all our claims upon Ossian
for him) his heart would have been proud of such music, and his genius
would have made it immortal."

"Another difficulty (which is, however, purely mechanical) arises from
the irregular structure of many of those airs and the lawless kind of
metre which it will in consequence be necessary to adapt to them. In
these instances the poet must write, not to the eye, but to the ear;
and must be content to have his verses of that description which
Cicero mentions, *Quos si cantu spoliaveris nuda remanebit oratio*.
That beautiful air, the "Twisting of the Rope," which has all the
romantic character of the Swiss Ranz des Vaches, is one of those wild
and sentimental rakes which it will not be very easy to tie down in
sober wedlock with poetry. However, notwithstanding all these
difficulties, and the very little talent which I can bring to surmount
them, the design appears to me so truly National that I shall feel
much pleasure in giving it all the assistance in my power."

The melodies appeared in groups of sixteen at a time, and immediately
found favour, but not with the populace, whom they reached very
gradually. It was in the drawing-rooms of the upper classes, where
Moore himself sang his melodies with a small voice but exquisite
feeling, that the Irish melodies first became famous.

Moore was before his time in recognizing the artistic value of brevity
in the modern song and ballad. Moreover, his knowledge of lyrical
perspective is unrivalled, his thought is pellucid, never obscured by
condensation or dimmed by diffuseness. But he most asserts his mastery
in song-craft by the apparent ease with which he handles the most
intricate musical measures, and mates the striking notes of each tune
to the words most adapted to them both in sound and sense; to say
nothing of the art with which he almost Italianizes English speech by
a melodious sequence of varying vowels and alliterative consonants
which almost sing themselves. Yet whilst Moore has, in addition to
this vocal quality, the very perfection of playful wit and graceful
fancy, as in "Quick! We have but a second," and now and again real
pathos, as in "O breathe not his name!", "She is far from the land!";
and again, an irresistible martial spirit, as in "O the light
entrancing" and "Avenging and bright falls the swift sword of Erin!",
many of his melodies are not standing the test of time. This is either
because our fine airs have been altered in time or character by him
and Stevenson, and so depreciated, or have been assorted by Moore with
the sentimental, metaphorical, and pseudo-philosophical fancies that
took the taste of the English upper classes half a century ago, or
because the tunes to which some of his finer lyrics are set are not of
the first-rate quality.

If a great national collection of Irish melodies is to be formed it
will be our plain duty to divorce these ill-matched lyrics from their
present partners, and to mate them to worthy airs in the Petrie and
Joyce collections and in Bunting's last volume, which came after
Moore's last melodies, and of which he was so ill-advisedly
contemptuous. It is as plain an obligation to slip out of their golden
settings Moore's occasional bits of green glass and to slip into them
the occasional emeralds of his contemporaries and successors. [*]_

The collections of Bunting may be said to have brought about the first
revival of Irish folk music. His last volume |---| from which Thomas
Moore drew nothing, and of which he spoke with unjustified contempt
|---| considering the popular success of some of its melodies in Sir
Charles Stanford's hands |---| appeared as late as 1840. For Bunting
had long survived the romantic days of the northern Rebellion, when
the "Parting of Friends" was very sadly and, as it proved,
significantly sung in the presence of Wolfe Tone and that noble
Irishman, Thomas Russell, both of whom expiated their acts of
rebellion against British authority by the death penalty.

It had long been a matter of wonder that the Irish verses to which
these airs had been sung were not forthcoming, although English
renderings from them by Miss Balfour and others were published in this
*de luxe* volume of 1840. The mystery has been solved quite recently
by the late energetic secretary of the Irish Folk Song Society,
Mrs. Milligan Fox, under remarkable circumstances. Calling at
Morley's, the harp makers, she learnt that one of his customers had
recently ordered an Irish harp on the ground that his grandfather had
been a collector of Irish music. Mrs. Fox inquired his name and
address. The purchaser proved to be Dr. Louis Macrory, of Battersea,
who generously put a great amount of unpublished material inherited
from his grandfather, Edward Bunting, at Mrs. Fox's disposal. He,
furthermore, added to her delight by informing her that there were
other Bunting papers in a box in Dublin. This proved to contain a
great number of the Gaelic originals of the tunes in the Bunting
collection. Why had they lain neglected for fifty years or more?
Because Patrick Lynch, who had collected them round the country, had
turned king's evidence against Russell, one of his employers upon this
quest. Russell was sent to the gallows, the friendly company of
folk-song collectors was broken up, and there was a strong feeling
against the publication of manuscripts collected by Lynch, the
informer, and hence their suppression till their discovery by
Mrs. Milligan Fox. They have been in part translated into English by
Miss Alice Milligan, Mrs. Milligan Fox's brilliant sister. Their
entire translation and publication may be looked for in the future.

To revert to Dr. Petrie and that distinguished Irishman's great
services to his country's folk songs. When Dr. Joyce was quite a young
man he sent Petrie some beautiful folk songs which he had as a lad
collected in his native Glenosheen. Petrie was delighted with these,
and Joyce became a frequent caller at the doctor's house and heard his
songs sung by Petrie's daughter Mary, who in her youth was very
beautiful; Sir Frederick Burton's picture of the "Blind Girl at the
Well" is an admirable likeness of her at that period. "How well,"
writes Dr. Joyce to me, "I recollect the procedure when I returned to
Dublin for my vacation. One of the first things was to spend an
evening with the whole family, the father and the four daughters, when
Mary went through my new collection on the piano with the rest
listening, especially Petrie himself, in rapt delight, as she came
across some exquisite air he had not heard before. But of all the airs
he was most delighted with the 'Wicked Kerry Man,' now in my *Ancient
Irish Music*, p. 84."

Here is an anecdote of Petrie recorded by Dr. Joyce in another
communication to me, showing how early his love for Irish music had
been: "When Petrie was a boy he was a good player upon a little single
keyed flute. One day he and another of his young companions set out
for a visit to Glendalough, then in its primitive state of
solitude. While passing Luggelaw they heard a girl near at hand
singing a beautiful air.  Instantly out came paper and pencil, and
Petrie took it down and then played it on his little flute. His
companions were charmed with it; and for the rest of the journey,
every couple of miles when they sat down to rest, they cried, "Here,
Petrie, out with your flute, and give us that lovely tune." That tune
is now known as "Luggelaw," and to it Thomas Moore, to whom Petrie
gave it, wrote his words (as lovely as the music):

| No not more welcome the fairy numbers
|   Of music fall on the sleeper's ear,
| When half awaking from fearful slumbers,
|   He thinks the full choir of heaven is near, |---|

| Than came that voice, when, all forsaken,
|   This heart long had sleeping lain,
| Nor thought its cold pulse would ever waken
|   To such benign, blessed sounds again.

And this brings us to George Petrie's famous collection of Irish
music, in the gathering of which he had been engaged with passionate
interest from his seventeenth till after his seventieth year.

At first he freely gave these folk airs to Thomas Moore and Francis
Holden, and even offered the use of his whole collection to Edward
Bunting. But finally, for fear that the priceless hoard might be
neglected or lost after his death, and also as a protest against the
methods of noting and dealing with the airs pursued by Edward Bunting
and Moore and Stevenson respectively, Petrie agreed to edit his
collection for the Society for the Preservation and Publication of the
Ancient Music of Ireland, which was founded in December, 1851.

One volume of this collection, comprising, however, only about a tenth
part of it, saw the light in 1857. A supplement contains thirty-six
airs, some of which, Dr. Stokes tells us, were sent to Petrie by
personal friends, such as Thomas Davis, the patriot, William
Allingham, the poet, Frederick Burton, the painter, and Patrick
Macdowell, the sculptor; "whilst physicians, students, parish priests,
Irish scholars, and college librarians all aided in the good work. But
most of Petrie's airs have been noted by himself from the singing of
the people, the chanting of some poor ballad-singer, the song of the
emigrant, of peasant girls while milking their cows or performing
their daily round of household duties, from the playing of wandering
musicians, or from the whistling of farmers and ploughmen." And this
description is typical of the method by which the airs were obtained,
in this instance on the islands of Aran:

"Inquiries having been made as to the names of persons 'who had music'
|---| that is, who were known as possessors and singers of the old
airs |---| an appointment was made with one or two of them to meet the
members of the party at some cottage near the little village of
Kilronan, which was their headquarters."

"To this cottage, when evening fell, Petrie, with his manuscript
music-book and violin, and always accompanied by his friend, Professor
Eugene O'Curry, the famous Irish scholar, used to proceed."

"Nothing could exceed the strange picturesqueness of the scenes which
night after night were thus presented."

"On approaching the house, always lighted up by a blazing turf fire,
it was seen to be surrounded by the islanders while its interior was
crowded by figures, the rich colours of whose dresses, heightened by
the firelight, showed with a strange vividness and variety, while
their fine countenances were all animated with curiosity and
pleasure."

"It would have required a Rembrandt to paint the scene. The minstrel
|---| sometimes an old woman, sometimes a beautiful girl or a young
man |---| was seated on a low stool in the chimney corner, while
chairs for Petrie and O'Curry were placed opposite, the rest of the
crowded audience remaining standing. The singer commenced, stopping at
every two or three bars of the melody to permit the writing of the
notes, and often repeating the passage until it was correctly taken
down, and then going on with the melody exactly from the point where
the singing was interrupted. The entire air being at last obtained,
the singer |---| a second time |---| was called to give the song
continuously, and when all corrections had been made, the violin,
an instrument of great sweetness and power, was produced, and the air
played as Petrie alone could play it, and often repeated."

"Never was the inherent love of music among the Irish people more
shown than on this occasion: they listened with deep attention, while
their heartfelt pleasure was expressed, less by exclamation than by
gestures; and when the music ceased, a general and murmured
conversation, in their own language, took place, which would continue
till the next song was commenced."

Some further airs drawn from the Petrie collection, after the
publication of the volume of 1857, have appeared in the form of piano
arrangements by Francis Hoffmann, and in vocal settings in *Songs of
Old Ireland*, *Songs of Erin*, and *Irish Folk Songs*, published by
Boosey and Co., and in *Irish Songs and Ballads*, published by
Novello, Ewer and Co. After this, however, the entire collection of
about eighteen hundred airs in purely melodic form, exactly as they
were noted down by Petrie, a vast treasure-house of folk song, was
published by Messrs. Boosey and Co. for the Irish Literary Society
under the editorship of the late Sir Charles Villiers Stanford.

Of recent collectors of Irish folk songs the longest at work, the most
learned, most indefatigable, and the most enthusiastic |---| at any
rate, on this side of the Atlantic Ocean |---| was Dr.  Patrick Weston
Joyce, who forty-one years ago published *Ancient Irish Music*,
containing a hundred airs never printed before. At the age of eighty
he published, in 1909, with Longmans' *Old Irish Folk Music and
Songs*, a collection of no less than eight hundred and forty-two Irish
airs and songs, hitherto unpublished, with a masterly preface dealing
with the "Forde" and "Pigot" collections contained in his volume, the
characteristics of Irish narrative airs, the origin of various
settings of Irish airs, the relation between Irish and Danish music,
the question as to how far harmony existed amongst the ancient Irish,
the various kinds of dance tunes, the pace at which different kinds of
Irish music should be played, and the total number of Irish airs,
probably some five thousand, in existence.

This volume is a mine of beautiful airs and of interesting Anglo-Irish
song and ballad words.

Mr. Francis O'Neill, for long chief of police in Chicago, is a famous
living Irish collector of folk music. This enthusiast, beginning by
setting down the Irish airs, learnt at his Irish-speaking mother's
knee, and then through the course of years tapping the memories of
fellow-countrymen who had drifted to Chicago from all the four corners
of the Green Isle, has succeeded in getting together a collection of
some eighteen hundred and fifty Irish airs, of which at least five
hundred had never been before in print. The great value of this
collection consists in the number of instrumental airs which it
contains. Levy's book of Irish dance music is dwarfed beside it. But
to go back a little. A good selection from the Petrie collection,
harmonized for the pianoforte but without words, was published after
Petrie's death by Piggot of Dublin. The music was arranged by
Hoffmann, a German resident in Dublin. The brothers Frank and Joseph
Robinson also arranged Irish airs, and so did Sir Robert Stewart. But
the first serious departure in this direction was made by Sir Charles
Villiers Stanford in his arrangements of Irish airs, chiefly from the
Petrie collection, to my words, in three volumes |---| *Songs of Old
Ireland* and *Songs of Erin*, published by Boosey, and *Irish Songs
and Ballads*, published by Novello. Dr. Charles Wood and I have also
done a collection with Boosey, entitled *Irish Folk Songs*, and
Stainer and Bell have fifty more Irish folk songs, which are gradually
seeing the light, half the lyrics of which are by Thomas Davis, Gerald
Griffin, Ferguson, Allingham, MacCall, and other well-known Irish song
writers, the remainder being from my pen. Latterly, Mr. Herbert Hughes
has brought out his *Songs of Ulla* and *Songs of Connaught*, with
lyrics by Mr. Joseph Campbell, Mr. Paudraic Colum, and others; while
Mrs. Milligan Fox, in conjunction with her sister, Alice Milligan, and
her poetic friend, Ethnea Carbery, and others, has arranged several
groups of beautiful North Country Irish airs, besides putting the
musical public much in her debt by her *Annals of the Irish Harpers*.

There has been indeed a gradually growing demand for Irish folk music,
but this has been largely due to the appearance of several notable
Irish folk singers. One of them, a truly great and versatile singer of
folk songs, Denis O'Sullivan, has, alas ! passed from our midst; but
we have in Mr. Harry Plunket Greene the most remarkable interpreter of
Irish songs, tragic, impetuous, dreamy, rollicking, who has appeared
in our day. And not only is he a great singer, but a fine teacher, as
anybody who has studied his volume on *The Interpretation of Song*
must acknowledge. To every folk singer I commend what he says about
the right way to sing folk songs, for with him precept and example are
finely identical. Other fine singers of Irish songs have been Mr.
Ledwich, better known as Herr Ludwig, and, of course, Joseph O'Mara.

The Gaelic League has not been idle in the collection and singing of
folk songs. They have reintroduced the singing of Irish-Gaelic into
the concert room, and collected many traditional songs and pipe and
fiddle airs. The Feis Ceoil, the Irish Music Festival, which sprung
out of lectures on Irish folk song, delivered before the National
Literary Society of Dublin by Dr. Annie Patterson and myself,
encourages Irish National music by prizes for singing in Irish and
English, and some of its members have taken down songs and pipers' and
fiddlers' tunes from the phonograph; but the Feis Ceoil does not, like
the Welsh Eisteddfod, offer prizes for collections of folk songs.

A collection of some seventy-five folk airs collected and edited by
Arthur Darley and P. J. McCall for the Feis Ceoil Association,
Senator Mrs. Costello's most interesting book of recently gathered
Galway and Mayo airs with Gaelic words, and the volumes of the Irish
Folk Society's publications, which come down to the year 1926, are
the latest contributions to Irish folk-song literature.

.. [*] The above criticism of Moore's powers as a lyrical writer is
       quoted from the introduction to my *Irish Song Book*, one of
       the volumes in the New Irish Library, published by Mr. Fisher
       Unwin.

----

.. toctree::

   Erin, the Tear and the Smile       <erin_tear_smile>
   Go where Glory waits Thee          <go_where_glory_waits_thee>
   Let Erin remember the Days of Old  <let_erin_remember_days_old>
   Silent, oh Moyle                   <silent_oh_moyle>
   Avenging and Bright                <avenging_bright>
   The Flight of the Earls            <flight_earls>
   Clare's Dragoons                   <clares_dragoons>
   At the Mid Hour of Night           <mid_hour_night>
   The Little Red Lark                <little_red_lark>
   I've found my Bonny Babe a Nest    <bonny_babe_nest>
   Shule Agra                         <shule_agra>
   Kitty of Colraine                  <kitty_coleraine>
   The Lark in Clear Air              <lark_clear_air>
   The Snowy-Breasted Pearl           <snowy_breasted_pearl>
   Down by the Sally Gardens          <sally_gardens>
   Pastheen Fionn                     <pastheen_fionn>
   The Welcome                        <welcome>
   The Heather Glen                   <heather_glen>
   No, not more Welcome               <no_not_more_welcome>
   Little Mary Cassidy                <little_mary_cassidy>
   The Winding Banks of Erne          <winding_banks_erne>
   The Red-Haired Man's Wife          <red-haired_mans_wife>
   Emer's Farewell                    <emers_farewell>
   Happy 'tis, thou Blind, for Thee   <happy_blind_thee>
   His Home and His Own Country       <his_home_his_own_country>
   Dublin Bay                         <dublin_bay>
   The Meeting of the Waters          <meeting_waters>
   The Song of the Woods              <song_woods>
   The Widow Malone                   <widow_malone>
   Father O'Flynn                     <father_oflynn>

----

(Source: `The Celtic Song Book, p. 21 - 36
<https://archive.org/details/the-celtic-song-book/page/n13/mode/1up>`_)
