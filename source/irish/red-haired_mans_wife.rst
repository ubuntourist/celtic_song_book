.. Page 66 (PDF Page 37, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
The Red-Haired Man's Wife
#########################

.. lilyinclude:: ./red-haired_mans_wife.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 66
<https://archive.org/details/the-celtic-song-book/page/n36/mode/1up>`_)
