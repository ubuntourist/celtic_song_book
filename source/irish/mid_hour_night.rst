.. Page 46 (PDF Page 27, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

########################
At the Mid Hour of Night
########################

.. lilyinclude:: ./mid_hour_night.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 46
<https://archive.org/details/the-celtic-song-book/page/n26/mode/1up>`_)
