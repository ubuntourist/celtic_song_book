.. Page 67 (PDF Page 37, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###############
Emer's Farewell
###############

.. lilyinclude:: ./emers_farewell.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

.. note::

   Cucullain was one of the most famous of the Irish legendary heroes,
   and is said to have withstood all Queen Maive of Connaught's
   champions at the great battle of the Ford.

----

(Source: `The Celtic Song Book, p. 67
<https://archive.org/details/the-celtic-song-book/page/n36/mode/1up>`_)
