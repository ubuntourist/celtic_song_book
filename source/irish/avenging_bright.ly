% Avenging and Bright
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.01 (kjc)
%
% Source: The Celtic Song Book, p. 42
%         https://archive.org/details/the-celtic-song-book/page/n24/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Avenging and Bright"
  subtitle = "(Cruachan na feine)"
  composer = \markup { \italic { "Poem by" } \smallCaps "Moore." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 3/4
  \tempo \markup { \italic "Quickly and fiercely." } 4 = 138
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4
    <<
      { \voiceOne e'4^\f }
      \new Voice
      { \voiceTwo e8 e8 }
    >>                                                     | %   0
    \oneVoice

    a4 a8([ b8]) c8([ a8])                                 | %   1
    b4 e4.-> d8                                            | %   2
    c4 a4 a4                                               | %   3
    g4 e4 e4                                               | %   4

    \break

    a4 a8([ b8]) c8([ a8])                                 | %   5
    b4 e4. d8                                              | %   6
    c4 a4 b4                                               | %   7
    a2
    <<
      { \voiceOne b4 }
      \new Voice
      { \voiceTwo b8 b8 }
    >>                                                     | %   8
    \oneVoice

    \break

    c4 c8([ d8]) e8([ c8])                                 | %   9
    b4 g4 e4                                               | %  10
    f4 f8([ a8]) g8([ f8])                                 | %  11
    e4 c4 g'4                                              | %  12

    \break

    c4 c8([ d8]) e8([ c8])                                 | %  13
    b4 g4 \tempo \markup { \italic "Slower." } 4 = 120 e4  | %  14
    f8([ e8]) d8([ c'8]) b8.([ a16])                       | %  15
    a2\fermata                                               %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  A -- _                                       | %   0
  veng -- ing _ and _                          | %   1
  bright fall the                              | %   2
  swift sword of                               | %   3
  E -- rin On                                  | %   4
  him who _ the _                              | %   5
  brave sons of                                | %   6
  Us -- na be --                               | %   7
  tray'd; For _                                | %   8
  ev -- 'ry _ fond _                           | %   9
  eye he hath                                  | %  10
  wa -- ken'd _ a _                            | %  11
  tear in, A                                   | %  12
  drop from _ his _                            | %  13
  heart -- wounds shall                        | %  14
  weep _ o'er _ her _                          | %  15
  blade                                          %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { By the red cloud that hung over Conor's dark dwelling, }
      \line { \hspace #2 { When Ulad's three champions lay sleeping in gore⸺   } }
      \line { By the billows of war, which so often, high swelling, }
      \line { \hspace #2 { Have wafted thes heroes to victory's shore, } }

      \vspace #0.8

      \line { We swear to avenge them! No joy shall be tasted, }
      \line { \hspace #2 { The harp shall be silent, the maiden unwed, } }
      \line { Our halls shall be mute, and our fields shall lie wasted, }
      \line { \hspace #2 { Till vengeance is wreak'd on the murderer's head. } }

      \vspace #0.8

      \line { Yes, monarch! tho' sweet are our home recollections, }
      \line { \hspace #2 { Tho' sweet are the tears that from tenderness fall; } }
      \line { Tho' sweet are our friendships, our hopes, our affections, }
      \line { \hspace #2 { Reveng on a tyrant is sweetest of all! } }

    }
  }
}
