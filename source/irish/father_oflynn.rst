.. Page 76 (PDF Page 42, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##############
Father O'Flynn
##############

.. lilyinclude:: ./father_oflynn.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 76
<https://archive.org/details/the-celtic-song-book/page/n41/mode/1up>`_)
