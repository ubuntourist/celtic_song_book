% Erin, the Tear and the Smile
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.10.14 (kjc)
%
% Source: The Celtic Song Book, p. 37
%         https://archive.org/details/the-celtic-song-book/page/n21/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Erin, the Tear and the Smile"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"Eibblín a rún.\"" }
      \line { \italic { "By" } \smallCaps { "Carol O'Daly" } \italic { "in Fourteenth Century." } }
    }
  }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key bf \major
  \time 3/4
  \tempo \markup { \italic "Slowly." } 4 = 98
}

melody = {
  \relative {
    \global
    f'4 g4 a4                                | %   1
    bf4. c8 d4                               | %   2
    f,4 g4 a4                                | %   3
    bf2 r4                                   | %   4

    \break

    f4 g4 a4                                 | %   5
    bf4. c8 d4                               | %   6
    f,4 g4 a4                                | %   7
    bf2 r4                                   | %   8

    \break

    \bar ".|:"

    d4 d4. d8                                | %   9
    ef4. g,8 g4                              | %  10
    d'4 d4 c4                                | %  11
    bf4. g8 f4                               | %  12

    \break

    f'4 ef8( [ d8]) c8( [ bf8])              | %  13
    bf4. c8 d4\fermata                       | %  14
    f,4 g4 a4                                | %  15
    bf2 r4                                   | %  16

    \bar ":|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  E -- rin, the
  tear and the
  smile in thine
  eyes
  Blend like the
  rain -- bow that
  hangs in the
  skies;

  Shin -- ing thro'
  sor -- row's stream,
  Sad -- d'ning thro'
  plea -- sure's beam,
  Thy suns, with
  doubt -- ful gleam,
  Weep while they
  rise.
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Erin, thy silent tear never shall cease⸺   }
      \line { Erin, thy languid smile ne'er shall increase⸺   }
      \line { \hspace #4 { Til, like the rainbow's light, } }
      \line { \hspace #4 { Thy various tints unite } }
      \line { \hspace #4 { And form in Heaven's sight, } }
      \line { \hspace #8 { One arch of peace! } }
    }
  }
}
