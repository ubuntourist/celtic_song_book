.. Page 42 (PDF Page 25, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###################
Avenging and Bright
###################

.. lilyinclude:: ./avenging_bright.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 42
<https://archive.org/details/the-celtic-song-book/page/n24/mode/1up>`_)
