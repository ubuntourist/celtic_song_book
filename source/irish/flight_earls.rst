.. Page 43 (PDF Page 25, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#######################
The Flight of the Earls
#######################

.. lilyinclude:: ./flight_earls.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 43
<https://archive.org/details/the-celtic-song-book/page/n24/mode/1up>`_)
