.. Page 62 (PDF Page 35, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###################
Little Mary Cassidy
###################

.. lilyinclude:: ./little_mary_cassidy.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 62
<https://archive.org/details/the-celtic-song-book/page/n34/mode/1up>`_)
