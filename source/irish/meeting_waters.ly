% The Meeting of the Waters
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.17 (kjc)
%
% Source: The Celtic Song Book, p. 72
%         https://archive.org/details/the-celtic-song-book/page/n39/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Meeting of the Waters"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"The Old Head of Denis.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key a \major
  \time 6/8
  \tempo \markup { \italic "With expression." } 4 = 80
}

melody = {
  \relative {
    \global
    \partial 8 e''16. d32                        | %   0
    cs8. b16 a8 a8 fs8 e8                        | %   1
    e8 fs8 a8 a4 b16. cs32                       | %   2

    \break

    d8 d8 cs16([ b16]) b8 cs8 a8                 | %   3
    e'8 cs8 a8 b4 b16 cs16                       | %   4

    \break

    d8 d8 cs16([ b16]) b8 cs8 a8                 | %   5
    e'8 cs8 a8 b4 a16. b32                       | %   6

    \break

    cs8. b16 a8 a16 fs8. e8                      | %   7
    e8 fs8 a8 d4 d16 cs16                        | %   8

    \break

    b8 b8 a8 a16 fs8. e8                         | %   9
    e8 fs8 a8 a4                                   %  10

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  There is                                       | %   0
  not in the wide world a                        | %   1
  val -- ly so sweet, As that                    | %   2
  vale in whose bo -- som the                    | %   3
  bright wa -- ters meet. Oh! the                | %   4
  last rays of feel -- ing and                   | %   5
  life must de -- part, Ere the                  | %   6
  bloom of that val -- ley shall                 | %   7
  fade from my heart! Ere the                    | %   8
  bloom of that val -- ly shall                  | %   9
  fade from my heart!                              %  10
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Yet it was not that Nature had shed o'er the scene }
      \line { Her purest of crystal and brightest of green; }
      \line { 'Twas not the soft magic of streamlet orh hill; }
      \line { Oh, no⸺it wass something more exquisite still;⸺   }

      \vspace #0.8
      \line { 'Twas that friends, the beloved of my bosom, were near, }
      \line { Who made ev'ry dear scene of enchantment more dear; }
      \line { And who felt how the best charms of Nature improve }
      \line { When we see them reflected in the looks that we love. }

      \vspace #0.8
      \line { Sweet vale of Avoca! how calm could I rest }
      \line { In thy bosom of shade with the friends I love best, }
      \line { Where the storms that we feel in this cold world should cease, }
      \line { And our hearts, like thy waters, be mingled in peace. }
    }
  }
}
