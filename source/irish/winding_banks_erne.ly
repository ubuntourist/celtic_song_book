% The Winding Banks of Erne
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.04 (kjc)
%
% Source: The Celtic Song Book, p. 47
%         https://archive.org/details/the-celtic-song-book/page/n26/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Winding Banks of Erne"
  poet = \markup { \smallCaps { "William Allingham." } }
  composer = \markup { \italic { "Air:" } "\"The River Roe.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key e \major
  \time 6/8
  \tempo \markup { \italic "Moderato." } 4 = 85
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 e'16[ fs16]                   | %   0
    gs4 fs8 e4 cs8                           | %   1
    fs4. e4 cs8                              | %   2
    b4 cs8 e4 e8                             | %   3

    \break

    e4.( e8) r8 e16([ fs16])                 | %   4
    gs4 fs8 gs8([ a8]) b8                    | %   5
    cs4 cs8 b4 gs8                           | %   6

    \break

    e4 e8 fs4 e8                             | %   7
    cs4.( cs8) r8 e16([ fs16])               | %   8
    gs4 fs8 gs8([ a8]) b8                    | %   9

    \break

    cs4 cs8 b4 cs8                           | %  10
    e4 e8 fs4 e8                             | %  11
    cs,4.( cs8) r8 e16([ fs16])              | %  12

    \break

    gs4 fs8 e4 cs8                           | %  13
    fs4 fs8 e4 cs8                           | %  14
    b4 cs8 e4 e8                             | %  15
    e4.( e8) r8 e16([ fs16])                 | %  16

    \break

    gs4 fs8 e4 cs8                           | %  17
    fs4. e4 cs8                              | %  18
    b4 cs8 e4 e8                             | %  19
    e4.( e8) r8 e16([ fs16])                 | %  20

    \break

    gs4 fs8 gs4 b8                           | %  21
    cs4 cs8 b4 gs8                           | %  22
    e4 e8 fs4 e8                             | %  23
    cs4.( cs8) r8 e8                         | %  24

    \break

    gs4 fs8 gs4 b8                           | %  25
    cs4 cs8 b4 cs8                           | %  26
    e,4 e8 fs4 e8                            | %  27
    cs4.( cs8) r8 e8                         | %  28

    \break

    gs4 fs8 e4 cs8                           | %  29
    fs4 fs8 e4 cs8                           | %  30
    b4 cs8 e4 e8                             | %  31
    e4.( e8) r8                                %  32

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  A -- _                                  | %   0
  dieu to Bel -- a --                     | %   1
  shan -- ny, where                       | %   2
  I was bred and                          | %   3
  born; _ Go _                            | %   4
  where I may, _ I'll                     | %   5
  think of you, As                        | %   6
  sure as night and                       | %   7
  morn... _ The _                         | %   8
  kind -- ly spot, _ the                  | %   9
  friend -- ly town, where                | %  10
  ev' -- ry -- one is                     | %  11
  known, _ And _                          | %  12
  not a face in                           | %  13
  all the place but                       | %  14
  part -- ly seems my                     | %  15
  own; _ There's _                        | %  16
  not a house or                          | %  17
  win -- dow, There's                     | %  18
  not a tree or                           | %  19
  hill, _ But _                           | %  20
  east or west, in                        | %  21
  for -- eign lands, I'll                 | %  22
  re -- col -- lect them                  | %  23
  still... _ I                            | %  24
  leave my warm heart                     | %  25
  with you, tho' my                       | %  26
  back I'm forced to                      | %  27
  turn; _ A --                            | %  28
  dieu to Bel -- a --                     | %  29
  shan -- ny, and the                     | %  30
  wind -- ing banks of                    | %  31
  Erne. _                                 | %  32
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { No more on pleasant evenings we'll saunter down the Mall, }
      \line { When the trout is rising to the fly, the salmon to the fall! }
      \line { The boat comes straining on her net, and heavily she creeps, }
      \line { Cast off, cast off⸺she feels the oars, and to her berth she sweeps; }
      \line { Now fore and aft keep hauling, and gathering up the clew, }
      \line { Till a silver wave of salmon roll in among the crew. }
      \line { Then they may sit with pipes a-lit, and many a joke and yarn⸺   }
      \line { Adieu to Belashanny, and the winding banks of Erne! }
      \vspace #0.8
      \line { The thrush will call through Camlin groves the livelong summer day; }
      \line { The waters run by mossy cliffs and banks with wild flowers gay; }
      \line { The girls will bring their work and sing beneath the twisted thorn, }
      \line { Or stray with sweetheards down the path among the growing corn; }
      \line { Along the river-side they go, where I have often been, }
      \line { Oh never shall I see again the days that I have seen! }
      \line { A thousand chances are to one I never may return⸺   }
      \line { Adiew to Belashanny, and the winding banks of Erne! }
      \vspace #0.8
      \line { Now measure from the Commons down to each end of the Purt, }
      \line { Round the Abbey, Moy, and Knather⸺I wish not one any hurt;  }
      \line { The Main Street, Back Street, College Lane, the Mall and Portnasun, }
      \line { If any foes of mine are there, I pardon every one. }
      \line { I hope that man and womankind will do the same by me; }
      \line { For my heart is sore and heavy at voyaging the sea. }
      \line { My loving friends I'll bear in mind, and often fondly turn }
      \line { To think of Belashanny, and the winding banks of Erne. }
    }
  }
}
