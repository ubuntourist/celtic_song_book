% Dublin Bay
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.17 (kjc)
%
% Source: The Celtic Song Book, p. 71
%         https://archive.org/details/the-celtic-song-book/page/n38/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Dublin Bay"
  poet = \markup { \smallCaps { "Lady Dufferin." } }
  composer = \markup { \italic { "Air:" } "\"Dublin Bay.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 3/4
  \tempo \markup { \italic "Smoothly." } 4 = 70
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global

    \bar ".|:"

    \partial 4. ef'8 ef8 ef8                       | %   0
    <<
      { \voiceOne g8.([ f16]) }
      \new Voice
      { \voiceTwo g8. f16 }
    >>
    \oneVoice
    ef8
    <<
      { \voiceOne f16 g16 }
      \new Voice
      { \voiceTwo f16([ g16]) }
    >>
    \oneVoice
    af8 bf8                                        | %   1

    \break

    c8.([ bf16]) af8 g16([ af16]) bf8 c16([ d16])  | %   2
    ef8 ef8 d8 ef8 f8 d8                           | %   3

    \break

    ef4. d16([ ef16]) f8 d8                        | %   4
    ef8 ef8 df8 c8 bf8 df8                         | %   5

    \break

    c8.([ bf16]) af8 g16([ af16]) bf8 ef8          | %   6
    bf8. af16 f8 ef16([ f16]) ef8 ef8              | %   7
    ef4.                                             %   8

    \bar ":|."
  }
}

verseOne = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  O Bay of                                | %   0
  Dub -- _ lin, how my heart you're       | %   1
  troub -- _ lin', Your _ beau -- ty.. _  | %   2
  haunts me like a fe -- ver              | %   3
  dream; Like _ fro -- zen                | %   4
  foun -- tains that the sun sets         | %   5
  bub -- _ lin', My _ heart's blood       | %   6
  warms when I but _ hear your            | %   7
  name.                                     %   8
}

verseTwo = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  And nev -- er                           | %   0
  till this life's pul -- _ sa -- tion    | %   1
  ceas -- _ es, My _ ear -- liest, _      | %   2
  lat -- est thought you'll fail to       | %   3
  be. Oh, _ none here                     | %   4
  knows how ve -- ry fair that            | %   5
  place _ is! And _ no one                | %   6
  cares how dear it _ is to               | %   7
  me.                                       %   8
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \verseOne }
    \addlyrics { \verseTwo }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Sweet Wicklow mountains! the soft sunlight sleepin' }
      \line { \hspace #2 { On your green uplands is a picture rare; } }
      \line { You crowd around me, like young maidens peepin', }
      \line { \hspace #2 { And puzzlin' me to say which is most fair, } }
      \line { As tho' you longed to see your own sweet faces }
      \line { \hspace #2 { Reflected in that smooth and silver sea. } }
      \line { My fondest blessin' on those lovely places, }
      \line { \hspace #2 { Tho' no one cares how dear they are to me. } }
      \vspace #0.8
      \line { How often, when alone at work I'm sittin', }
      \line { \hspace #2 { And musing sadly on the days of yore, } }
      \line { I think I see my pretty Katie knittin', }
      \line { \hspace #2 { The childer playin' round the cabin door; } }
      \line { I think I see the neighbours' kindly faces }
      \line { \hspace #2 { All gathered round, their long-lost friend to see; } }
      \line { Though none here knows how very fair that place is, }
      \line { \hspace #2 { Heav'n knows how dear my poor home was to me. } }
    }
  }
}
