% Go where Glory waits Thee
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.10.21 (kjc)
%
% Source: The Celtic Song Book, p. 38
%         https://archive.org/details/the-celtic-song-book/page/n22/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Go where Glory waits Thee"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"Maid of the Valley.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key d \major
  \time 3/4
% \tempo \markup { \italic "Moderately slow." } 4 = 74
  \tempo \markup { \italic "Moderately slow." } 4 = 60  % slower
}

melody = {
  \relative {
    \global
    d'8^\mf d8 e16([ fs16]) g8 a8 fs8                     | %   1
    g8. a16 b8. g16 a8 fs8                                | %   2

    \break

    \once \override Score.MetronomeMark.X-offset = #4
    \tempo \markup { \italic "a little slower." } 4 = 55
    d4^\pp e8([ fs16 g16] fs8) e8                         | %   3
    d8. d16 d4. r8                                        | %   4

    \break

    \tempo \markup { \italic "In time." } 4 = 60
    d8. d16 e16([ fs16]) g8 a8 fs8                        | %   5
    g8. a16 b8 g8 a8 fs8                                  | %   6

    \break

    \once \override Score.MetronomeMark.X-offset = #4
    \tempo \markup { \italic "a little slower." } 4 = 55
    d4^\pp e8([ fs16 g16] fs8) e8                         | %   7
    d8. d16 d4. r8                                        | %   8

    \break

    \once \override Score.MetronomeMark.X-offset = #4
    \tempo \markup { \italic "In time." } 4 = 60
    d'8.^\mf d16 cs8 b8 cs8 d8                            | %   9
    cs8. d16 cs8 b8 a8 a8                                 | %  10

    \break

    a8. g16 fs8 e8 fs8 a8                                 | %  11
    \once \override Score.MetronomeMark.X-offset = #3
    \tempo \markup { \italic "a little slower." } 4 = 55
    b8.^\p cs16 d8 e8 d4                                  | %  12

    \break

    \tempo \markup { \italic "In time." } 4 = 60
    d,8 d8 e16([ fs16]) g8 a8^\cresc fs8                  | %  13
    g8. a16 b8 g8 a8 fs8                                  | %  14

    \break

    \once \override Score.MetronomeMark.X-offset = #4
    \tempo \markup { \italic "a little slower." } 4 = 55
    d4^\pp e8([ fs16 g16] fs8) e8                         | %  15
    d8. d16 d2\fermata                                    | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Go where glo -- ry waits thee,               | %   1
  But, while fame e -- lates thee.             | %   2
  Oh! still............................ re --  | %   3
  mem -- ber me.                               | %   4
  When the praise thou meet -- est             | %   5
  To thine ear is sweet -- est                 | %   6
  Oh! then.......................... re --     | %   7
  mem -- ber me.                               | %   8
  O -- ther arms may press thee,               | %   5
  Dear -- er friends ca -- ress thee,          | %   6
  All the joys that bless thee                 | %   7
  Sweet -- er far may be;                      | %   8
  But when friends are near -- est,            | %   9
  And when joys are dear -- est,               | %  10
  Oh! then............................ re --   | %  11
  mem -- ber me.                               | %  12
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { When, at eve thou rovest }
        \line { By the star thou lovest }
        \line { \hspace #2 { Oh! then remember me. } }
        \line { Think, when home returning, }
        \line { Bright we've seen it burning, }
        \line { \hspace #2 { Oh! thus remember me. } }
        \line { Oft as summer closes, }
        \line { When thine eye reposes, }
        \line { On its ling'ring roses }
        \line { Once so loved by thee, }
        \line { Thin of her who wove them, }
        \line { Her who made thee love them, }
        \line { \hspace #2 { Oh! then remember me. } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -40) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { When, around thee dying, }
        \line { Autumn leaves are lying, }
        \line { \hspace #2 { Oh! then remember me. } }
        \line { And, at night, when gazing }
        \line { On the gay hearth blazing, }
        \line { \hspace #2 { Oh! still remember me. } }
        \line { Then should music, stealing }
        \line { All the soul of feeling, }
        \line { To thy heart appealing, }
        \line { Draw one tear from thee; }
        \line { Then let mem'ry bring thee }
        \line { Strains I used to sing thee,⸺   }
        \line { \hspace #2 { Oh! then remember me. } }
      }
    }
  }
}
