% I've found my Bonny Babe a Nest
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.04 (kjc)
%
% Source: The Celtic Song Book, p. 48
%         https://archive.org/details/the-celtic-song-book/page/n27/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "I've found my Bonny Babe a Nest"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = "Two Irish Lullabies combined."
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 3/4
  \tempo \markup { \italic "Slowly and softly." } 4 = 60
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 c'8^\p                        | %   0
    f16 g16 a16 bf16 c8 bf8 a8. g16          | %   1
    f16([ g16]) a16([ bf16]) g4. c,8         | %   2

    \break

    f16 g16 a16 bf16 c8 bf8 a8. g16          | %   3
    f8 a8 f4 r8 c8                           | %   4

    \break

    f16 g16 a16 bf16 c8 bf8 a8. g16          | %   5
    f16([ g16]) a16([ bf16]) g4. c,8         | %   6

    \break

    f16 g16 a16 bf16 c8 bf8 a8. g16          | %   7
    f8 a8 f4 r8 a8                           | %   8
    f4 r8 a8 f4~                             | %   9   FIX ME

    \break

    f4 r8^\p c8^\( c8 d8                     | %  10   FIX ME
    f4.\) a8^\( g8 a8                        | %  11
    f4.\) c8 c8 d8                           | %  12

    \break

    f4. a8 g8 a8                             | %  13
    c4.^\< a8\! f'8 e8                       | %  14
    d8 c8 bf8^\>([ a8]) g8\!([ a8])          | %  15

    \break

    f4. a8^\p g8 a8                          | %  16
    f4. a8^\pp g8 a8                         | %  17
    f2\fermata                                 %  18

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  I've                                    | %   0
  found my bon -- ny babe a nest On       | %   1
  Slum -- _ ber _ Tree, I'll              | %   2
  rock you there to ro -- sy rest, As --  | %   3
  tore Ma --  chree! I've                 | %   4
  found my bon -- ny babe a nest On       | %   5
  Slum -- _ ber _ Tree, I'll              | %   6
  rock you there to ro -- sy rest, As --  | %   7
  tore Ma -- chree! Hush --               | %   8
  o, Hush -- o! _                           %   9    FIX ME
  Oh, lul -- la --                        | %  10    FIX ME
  lo! sing all the                        | %  11
  leaves On Slum -- ber                   | %  12
  Tree, on Slum -- ber                    | %  13
  Tree, Till ev -- 'ry --                 | %  14
  thing that hurts _ or _                 | %  15
  grieves A -- far must                   | %  16
  flee, a -- far must                     | %  17
  flee.                                     %  18
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { I'd put my pretty child to float }
        \line { \hspace #2 { Away from me, } }
        \line { Within the new moon's silver boat }
        \line { \hspace #2 { On Slumber Sea. } }
        \line { I'd put my pretty child to float }
        \line { \hspace #2 { Away from me, } }
        \line { Within the new moon's silver boat  }
        \line { \hspace #2 { On Slumber Sea. } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -26) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { Hush-o, Hush-o! }
        \line { \hspace #2 { And when your starry sail is o'er } }
        \line { From Slumber Sea, from Slumber Sea, }
        \line { \hspace #2 { My precious one, you'll step to shore } }
        \line {  On Mother's knee, on Mother's knee.}
      }
    }
  }
}
