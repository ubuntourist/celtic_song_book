% Shule Agra
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.05 (kjc)
%
% Source: The Celtic Song Book, p. 50
%         https://archive.org/details/the-celtic-song-book/page/n28/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Shule Agra"
  poet = \markup {
    \center-column {
      \line { \smallCaps { "Anon." } }
      \line { \italic { "(Adapted by the Editor.)" } }
    }
  }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"Siúbhail a ghrádh.\"" }
      \line { \italic { "(Come, my love!)" } }
    }
  }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key a \minor
  \time 4/4
  \tempo \markup { \italic "With feeling." } 4 = 70
}

melody = {
  \relative {
    \global
    \partial 4 e'4                                          | %   0
    a4. b8 c4 b4                                            | %   1
    a4 e4 e4 r8 f8                                          | %   2
    g4 e4 c'4. b8                                           | %   3

    \break

    a4 d,4 d4 r8 d8                                         | %   4
    e4 e8 d8 c4 d4                                          | %   5
    e4 a4 c4 c8([ d8])                                      | %   6

    \break

    e4.( d8) c4. a8                                         | %   7
    c8([ b8]) \appoggiatura { a16 [ b16] } a8.([ gs16]) a2  | %   8
    a4.( b8) c4( b4)                                        | %   9

    \break

    a4 e4 e4.( f8)                                          | %  10
    g4 e4 c'4. b8                                           | %  11
    a4 d,4 d4 d8 d8                                         | %  12

    \break

    e4 e8 d8 c4 d4                                          | %  13
    e4 a4 c4 c8([ d8])                                      | %  14

    \break

    e4.( d8) c4. a8                                         | %  15
    a8([ b8] \appoggiatura { a16 [ b16] } a8.) gs16 a2      | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  His                                        | %   0
  hair was black, his                        | %   1
  eye was blue, His                          | %   2
  arm was stout, his                         | %   3
  word was true. I                           | %   4
  wish in my heart I                         | %   5
  was with you. Go --                        | %   6
  dé -- thu, ma --                           | %   7
  vour -- neen slaun.                        | %   8
  Shule,..  shule,..                         | %   9
  shule a -- gra!...                         | %  10
  On -- ly death can                         | %  11
  ease my woe, Since the                     | %  12
  lad of my heart from                       | %  13
  me did go, Go --                           | %  14
  dé -- thu, ma --                           | %  15
  vour -- neen slaun!                        | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { 'Tis oft I sat on my true love's knee; }
        \line { Many a fond story he told to me. }
        \line { He told me things that ne'er shall be. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { I sold my rock, I sold my reel; }
        \line { When my flax was spun I sold my wheel, }
        \line { To buy my love a sword of steel. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { But when King James was forced to flee, }
        \line { The Wild Geese spread their wings to sea, }
        \line { And bore ma brouchal far from me. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -44) \hspace #10
      \left-column {
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { I saw them sail from Brandon Hill, }
        \line { Then down I sat and cried my fill, }
        \line { That every tear would turn a mill. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { I wish the King would return to reign, }
        \line { And bring my true love back again; }
        \line { I wish, and wish, but I wish in vain. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
        \combine \null \vspace #0.8 % adds vertical spacing between verses
        \line { I'll dye my petticoat, I'll dye it red, }
        \line { And round the world I'll beg my bread, }
        \line { Till I find my love, alive or dead. }
        \line { \hspace #4 { Go-dé-thu, mavourneen slaun, etc. } }
      }
    }
  }
}
