.. Page 56 (PDF Page 32, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##############
Pastheen Fionn
##############

.. lilyinclude:: ./pastheen_fionn.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 56
<https://archive.org/details/the-celtic-song-book/page/n31/mode/1up>`_)
