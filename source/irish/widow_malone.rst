.. Page 74 (PDF Page 41, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################
The Widow Malone
################

.. lilyinclude:: ./widow_malone.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 74
<https://archive.org/details/the-celtic-song-book/page/n40/mode/1up>`_)
