% Pastheen Fionn
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.09 (kjc)
%
% Source: The Celtic Song Book, p. 56
%         https://archive.org/details/the-celtic-song-book/page/n31/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Pastheen Fionn"
  poet = \markup { "From the Irish by" \smallCaps { "Sir Samuel Ferguson." } }
  composer = \markup { \italic { "Air:" } "\"Pasteen Fionn.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 6/8
  \tempo \markup { \italic "Passionately." } 4 = 80
}

melody = {
  \relative {
    \global
    \partial 8 g'16 g16                          | %   0
    c8.([ d16]) c8 bf8 c8 d8                     | %   1
    ef8.([ d16]) c8 d4 ef16([ d16])              | %   2

    \break

    c8.([ bf16]) g16([ f16]) ef8 f8 g16([ a16])  | %   3
    bf4 g8 g4 g16 g16                            | %   4

    \break

    c8.([ d16]) c8 bf8 c8 d8                     | %   5
    ef4 c8 d4 ef16 d16                           | %   6

    \break

    c8 bf8 g8 f4 g16 f16                         | %   7
    ef8([ c8]) c8 c4                               %   8

    \bar "|."

    \tempo \markup { \italic "Faster." } 4 = 90
    \partial 8 c16([ d16])                       |

    \break

    ef8. f16 g16 a16 bf8 g8 g8                   | %   9
    af8 f8 f8 bf8 g8 f8                          | %  10

    \break

    ef8. f16 g16 a16 bf8 g8 f8                   | %  11
    g8([ c8]) c8 c4 c,16([ d16])                 | %  12

    \break

    ef8.([ f16]) g16 a16 bf8([ g8]) f8           | %  13
    g8([ c8]) d8 ef8.([ f16]) d16([ c16])        | %  14

    \break

    bf8 g8 ef8 f8.([ af16]) g16 f16              | %  15
    ef8([ c8]) c8 c4                               %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh, my                                         | %   0
  fair Pas -- theen is my                        | %   1
  heart's de -- light, Her                       | %   2
  gay heart laughs in her                        | %   3
  blue eye bright, Like the                      | %   4
  ap -- ple blos -- some her                     | %   5
  bo -- som white, And her                       | %   6
  neck like the swan's on a                      | %   7
  March morn bright.                               %   8

  Then                                           | %   9
  O -- ro, will you come with me,                | %  10
  come with me, come with me?                    | %  11
  O -- ro, will you come with me,                | %  12
  brown girl sweet? For                          | %  13
  oh!...... I would go...... thro'               | %  14
  snow and sleet, If                             | %  15
  you would but come with me,                    | %  16
  brown girl sweet.                                %  17
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { \hspace #2 { Love of my heart, my fair Patheen! } }
        \line { \hspace #2 { Her cheeks are as red as the rose's sheen; } }
        \line { \hspace #2 { But my lips have tasted no more, I ween, } }
        \line { \hspace #2 { Than the glass I drank to the health of my queen! } }
        \line { Then Oro, come with me! come with me! }
        \line { \hspace #4 { come with me! etc. } }
        \combine \null \vspace #0.8
        \line { \hspace #2 { Were I in the town, where's mirth and glee, } }
        \line { \hspace #2 { Or 'twixt two barrels of barley bree, } }
        \line { \hspace #2 { With my fair Patheen upon my knee, } }
        \line { \hspace #2 { 'Tis I would drink to her pleasantly! } }
        \line { Then Oro, come with me! come with me! }
        \line { \hspace #4 { come with me! etc. } }
      }
      \hspace #4 \raise #1 \draw-line #'(0 . -41) \hspace #4
      \left-column {
        \combine \null \vspace #0.8
        \line { \hspace #2 { Nine nights I lay in longing and pain, } }
        \line { \hspace #2 { Betwixt two bushes, beneath the rain, } }
        \line { \hspace #2 { Thinking to see you, love, once again; } }
        \line { \hspace #2 { But whistle and call were all in vain! } }
        \line { Then Oro, come with me! come with me! }
        \line { \hspace #4 { come with me! etc. } }
        \combine \null \vspace #0.8
        \line { \hspace #2 { I'll leave my people, both friend and foe; } }
        \line { \hspace #2 { From all the girls in the world I'll go; } }
        \line { \hspace #2 { But from you, sweetheart, oh, never! oh no! } }
        \line { \hspace #2 { Till I lie in the coffin, stretched cold and low! } }
        \line { Then Oro, come with me! come with me! }
        \line { \hspace #4 { come with me! etc. } }
      }
    }
  }
}
