.. Page 44 (PDF Page 26, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################
Clare's Dragoons
################

.. lilyinclude:: ./clares_dragoons.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 44
<https://archive.org/details/the-celtic-song-book/page/n25/mode/1up>`_)
