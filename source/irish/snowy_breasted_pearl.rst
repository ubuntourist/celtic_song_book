.. Page 54 (PDF Page 31, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

########################
The Snowy-Breasted Pearl
########################

.. lilyinclude:: ./snowy_breasted_pearl.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 54
<https://archive.org/details/the-celtic-song-book/page/n30/mode/1up>`_)
