% The Lark in Clear Air
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.06 (kjc)
%
% Source: The Celtic Song Book, p. 53
%         https://archive.org/details/the-celtic-song-book/page/n29/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Lark in Clear Air"
  poet = \markup { \smallCaps { "Sir Samuel Ferguson." } }
  composer = \markup { \italic { "Air:" } "\"The Tailor\"" }
  tagline = ""
% instrument = "instrument"
% subtitle = "subtitle"
% arranger = "arranger"
% copyright = "copyright"
% composer = "composer"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 3/4
  \tempo \markup { \italic "Tenderly." } 4 = 70
% \override TupletBracket.tuplet-slur = ##t              % Not available til v. 2.21
  \override TupletBracket.bracket-visibility = ##t
% \tupletUp
}

melody = {
  \relative {
    \global
    \partial 4 c''8([ a8])                             | %   0
    f8. e16 e8([ f8]) d8([ bf8])                       | %   1
    c2 \tuplet 3/2 { c8([ d8]) e8 }                    | %   2
    f4. g8([ a8]) bf8                                  | %   3

    \break

    g4 c4 c8 a8                                        | %   4
    f8. e16 e8([ f8]) d8([ bf8])                       | %   5
    c2 \tuplet 3/2 { c8([ d8]) e8 }                    | %   6

    \break

    f8([ a8]) d,4 g8 e8                                | %   7
    f2 c8 c8                                           | %   8
    f4. e8 f16([ g16]) a16([ bf16])                    | %   9

    \break

    c2 d8. c16                                         | %  10
    c8([ a8]) f8([ g8]) \tuplet 3/2 { a8([ c8 bf8]) }  | %  11
    a4 g4 c8 a8                                        | %  12

    \break

    f8. e16 e8([ f8]) d8([ bf8])                       | %  13
    c2 \tuplet 3/2 { c8([ d8]) e8 }                    | %  14
    f8([ a8]) d,4 g8([ e8])                            | %  15
    f2\fermata                                           %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Dear                                                 | %   0
  thoughts are in my                                   | %   1
  mind, and my                                         | %   2
  soul soars en --                                     | %   3
  chant -- ed as I                                     | %   4
  hear the sweet lark                                  | %   5
  sing in the                                          | %   6
  clear air of the                                     | %   7
  day. For a                                           | %   8
  ten -- der, beam -- ing                              | %   9
  smile to my                                          | %  10
  hope has been                                        | %  11
  grant -- ed, and to --                               | %  12
  mor -- row she shall                                 | %  13
  hear all my                                          | %  14
  fond heart would                                     | %  15
  say.                                                   %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { I shall tell her all my love, all my soul's adoration, }
      \line { And I think she will hear me, and will not say me nay. }
      \line { It is this that gives my soul all its joyous elation, }
      \line { As I hear the sweet lark sing in the clear air of the day. }
    }
  }
}
