% The Little Red Lark
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.04 (kjc)
%
% Source: The Celtic Song Book, p. 47
%         https://archive.org/details/the-celtic-song-book/page/n26/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Little Red Lark"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = \markup { \italic { "Air:" } "\"The Little Red Lark.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key e \major
  \time 6/8
  \tempo \markup { \italic "Flowing and not too quickly." } 4 = 85
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 b8^\mf                        | %   0
    e4 fs8 e8 cs8 ds8                        | %   1
    e4 fs8^\< gs8\! e'8 a,8                  | %   2

    \break

    b8^\> gs8 e8\! gs8([ fs8]) e8            | %   3
    cs4.( b4) b8                             | %   4
    e8 e8 fs8 e8 cs8 ds8                     | %   5

    \break

    e4 fs8 gs16([ e'8.])^\< cs8\!            | %   6
    b8 gs8 e8 gs8^\>([ fs8\!]) e8            | %   7
    e4.( e4) b'8                             | %   8

    \break

    e8([ ds8]) e8
    <<
      { \voiceOne cs8([ b8]) }
      \new Voice
      { \voiceTwo cs8 b8 }
    >>
    \oneVoice
    gs'8                                    | %   9
    e8 ds8 e8
    <<
      { \voiceOne cs8([ e8]) }
      \new Voice
      { \voiceTwo cs8 e8 }
    >>
    \oneVoice
    cs8                                     | %  10

    \break

%   \once \override Hairpin.shorten-pair = #'(0 . 2)
    b8 gs8 e8 gs8([ fs8]) e8                 | %  11
    <<
      { \voiceOne cs4.( b8.) b16 }
      \new Voice
      { \voiceTwo cs4. b4 }
    >>
    \oneVoice
    b8                                       | %  12
    <<
      { \voiceOne e8 e8 fs8 e8^\< }
      \new Voice
      { \voiceTwo e4 fs4 }
    >>
    \oneVoice
    cs8\! ds8                                | %  13

    \break

    e4^\f fs8
    <<
      { \voiceOne gs16 e'8. }
      \new Voice
      { \voiceTwo gs,16([ e'8.]) }
    >>
    \oneVoice
    cs8                                      | %  14
    b8 gs8 e8 gs8([ fs8.]) e16               | %  15
    e4.( e8) r8\fermata                        %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh                                      | %   0
  swan of slen -- der -- ness,            | %   1
  Dove of ten -- der -- ness,             | %   2
  Jew -- el of joy, _ a --                | %   3
  rise!............ _ The                 | %   4
  lit -- tle red lark, Like a             | %   5
  soar -- ing spark _ Of                  | %   6
  song, to his sun -- _ burst             | %   7
  flies............. _ But                | %   8
  till......... _ thou'st ris -- _ en,    | %   9
  Earth is a pris -- _ on                 | %  10
  Full of my lone -- _ some               | %  11
  sighs; _ Then a --                      | %  12
  wake and dis -- co -- ver To            | %  13
  thy fond lov -- er The                  | %  14
  morn of thy match -- _ less             | %  15
  eyes!............ _                       %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { The dawn is dark to me, }
        \line { Hark, oh hark to me,  }
        \line { Pulse of my heart, I pray! }
        \line { And out of thy hiding }
        \line { With blushes gliding, }
        \line { Dazzle me with thy day. }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -20) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { Ah, then once more to thee }
        \line { Flying I'll pour to thee }
        \line { Passion so sweet and gay, }
        \line { The lark shall listen, }
        \line { And the dewdrops glisten, }
        \line { Laughing on ev'ry spray. }
      }
    }
  }
}
