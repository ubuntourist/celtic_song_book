% The Song of the Woods
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.18 (kjc)
%
% Source: The Celtic Song Book, p. 73
%         https://archive.org/details/the-celtic-song-book/page/n39/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Song of the Woods"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = \markup { \italic { "Air:" } "\"The Song of the Woods\"" }
  tagline = ""
% instrument = "instrument"
% subtitle = "subtitle"
% arranger = "arranger"
% copyright = "copyright"
% composer = "composer"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 3/4
% \override TupletBracket.tuplet-slur = ##t              % Not available til v. 2.21
  \override TupletBracket.bracket-visibility = ##t
% \tupletUp
}

%    c2 \tuplet 3/2 { c8([ d8]) e8 }                    | %   2

melody = {
  \relative {
    \global
    r4 r4 e'4                                          | %   1
    f8. e16 e8. d16 d8. c16                            | %   2
    c2 e8. g16                                         | %   3

    \break

    a4( g8) e8 d8. c16                                 | %   4
    c2 e8([ g8])                                       | %   5
    g8. f16 f8 e8 e8 d8                                | %   6

    \break

    g2 c8. a16                                         | %   7
    g4 e4 e8. d16                                      | %   8
    c2 r4                                              | %   9
    r4 r4 g'4                                          | %  10

    \break

    c4 b4 \tuplet 3/2 { a8[ b8] c8 }                   | %  11
    g4 e4 \tuplet 3/2 { c8[ e8 g8] }                   | %  12
    c8 b8 a8 g8 a8 b16([ c16])                         | %  13

    \break

    d,2 e8([ g8])                                      | %  14
    g8 f8 f8 e8 e8 d8                                  | %  15
    g2 c8. a16                                         | %  16
    g4 e4 e8. d16                                      | %  17

    \break

    c2 r4                                              | %  18
    r2.                                                | %  19
    r2.                                                | %  20
    r4 r4 e4                                           | %  21
    f8. e16 e8 d8 d8. c16                              | %  22
    c2 e8. g16                                         | %  23

    \break

    a4( g8) e8. d16 c8                                 | %  24  PDF = a4( g8) e8 d16. c8 ???
    c2 e8.[ g16]                                       | %  25
    g8. f16 f8 e8 e8 d8                                | %  26

    \break

    g2 c8. a16                                         | %  27
    g4. e8 e8. d16                                     | %  28
    c2 r4                                              | %  29
    r4 r4 g'4                                          | %  30

    \break

    c4 b4 \tuplet 3/2 { a8[ b8] c8 }                   | %  31
    g4 e4 \tuplet 3/2 { c8[ e8 g8] }                   | %  32
    c8. b16 a8 g8 a8 b16([ c16])                       | %  33

    \break

    d,2 e8([ g8])                                      | %  34
    g8 f8 f8 e8 e8 d8                                  | %  35
    g4 c4. a8                                          | %  36

    \break

    g4 e4 e8. d16                                      | %  37
    c2 r4                                              | %  36
    r2.                                                | %  39
    r2.                                                | %  40

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Not                                                  | %   1
  on -- ly where Thy bless -- ed                       | %   2
  bells Peal a --                                      | %   3
  far... for praise and                                | %   4
  prayer Or...                                         | %   5
  where Thy sol -- emn or -- gan                       | %   6
  swells, Lord not                                     | %   7
  on -- ly art thou                                    | %   8
  there.                                                 %   9
  Thy                                                  | %  10
  voice of many... _                                   | %  11
  wa -- ters from...                                   | %  12
  out the o -- cean com -- fort                        | %  13
  speaks, Thy                                          | %  14
  pres -- ence to a ra -- diant                        | %  15
  rose thrills a                                       | %  16
  thou -- sand vir -- gin                              | %  17
  peaks.                                                 %  18
                                                         %  19
                                                         %  20
  And                                                  | %  21
  here where in one won -- drous                       | %  22
  woof aisle on                                        | %  23
  aisle... and choir on                                | %  24
  choir. To..                                          | %  25
  rear they rar -- est tem -- ple                      | %  26
  roof Pillar -- ed                                    | %  27
  oak and pine as --                                   | %  28
  pire.                                                  %  29
  Life                                                 | %  30
  wea -- ry here we                                    | %  31
  wan -- der when..                                    | %  32
  lo! the Sa -- viour's glea -- ming                   | %  33
  stole! 'Tis..                                        | %  34
  caught un -- to our crav -- ing                      | %  35
  lips kissed, and                                     | %  36
  straight -- way  we are                              | %  37
  whole.                                               | %  38
                                                       | %  39
                                                       | %  40
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}
