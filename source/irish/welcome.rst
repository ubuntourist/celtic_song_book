.. Page 58 (PDF Page 33, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###########
The Welcome
###########

.. lilyinclude:: ./welcome.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 58
<https://archive.org/details/the-celtic-song-book/page/n32/mode/1up>`_)
