% Kitty of Coleraine
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.06 (kjc)
%
% Source: The Celtic Song Book, p. 51
%         https://archive.org/details/the-celtic-song-book/page/n28/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Kitty of Coleraine"
  poet = \markup { \smallCaps { "Anon." } }
  composer = \markup { \italic { "Air:" } "\"Kitty of Coleraine.\"" }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key g \major
  \time 6/8
  \tempo \markup { \italic "Playfully." } 4 = 70
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 d'8                                         | %   0

    d8 g8 g8 g8 b8 d8                                      | %   1
    a8. b16 a16([ g16]) g8 fs8 d16 d16                     | %   2

    \break

    d8 g8 g8 g8 b8 d8                                      | %   3
    \appoggiatura d16 c8
    \appoggiatura b16 a8
    \appoggiatura g16 fs8
    g4 d16 d16                                             | %   4

    \break

    d8 g8 g8 g8 b8 d8                                      | %   5
    a8. b16 g8 g8 fs8 d8                                   | %   6

    \break

    g8 b8 d8 g,8 c8 e8                                     | %   7
    d,8. e16 fs8 g4.                                       | %   8

    \bar "||"

    \break

    \partial 8 d'8
    d8 b8 g8 a8 fs8 d8                                     | %   9
    d'8 b8 g8 a8 fs8 d8                                    | %  10

    \break

    d'8 a8 fs8 e8 g8 b8                                    | %  11
    a8 b8 cs8 d4 d,16 c16                                  | %  12

    \break

    b8 d8 g8 g8 b8 d8                                      | %  13
    a8 b8 a16([ g16]) g8 fs8 d8                            | %  14

    \break

    g8 b8 d8 g,8 c8 \afterGrace e8\fermata {
      fs16 [e16 ds16 e16 fs16 g16 e16 c16 a16]
    }                                                      | %  15
    d,8 e8 fs8 g4                                            %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  As                                                       | %   0
  beau -- ti -- ful Kit -- ty one                          | %   1
  morn -- ing was trip -- ping, With a                     | %   2
  pitch -- er of milk from the                             | %   3
  fair of Cole -- raine, When she                          | %   4
  saw me she stum -- bled, The                             | %   5
  pitch -- er it tum -- bled, And                          | %   6
  all the sweet but -- ter -- milk                         | %   7
  wa -- ter'd the plain.                                   | %   8
  “Oh, what shall I do now? 'Twas                          | %   9
  Look -- ing at you, now! Sure,                           | %  10
  sure, such a pitch -- er I'll                            | %  11
  ne'er meet a -- gain! 'Twas the                          | %  12
  pride of my dai -- ry; O                                 | %  13
  Bar -- ney Mc -- Clea -- ry, You've                      | %  14
  sent as a plague to the                                  | %  15
  girls of Cole -- raine.”                                   %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { I sat down beside her, and gently did chide her, }
      \line { \hspace #2 { That such a misfortune should give her such pain; } }
      \line { A kiss then I gave her, and, before I did leave her, }
      \line { \hspace #2 { She vowed, for such pleasure, she'd break it again. } }
      \line { 'Twas hay-making season; I can't tell the reason, }
      \line { \hspace #2 { Misfortune will never come single, 'tis plain; } }
      \line { For very soon after poor Kitty's disaster }
      \line { \hspace #2 { The devil a pitcher was whole in Coleraine. } }
    }
  }
}
