% Silent, oh Moyle
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.01 (kjc)
%
% Source: The Celtic Song Book, p. 41
%         https://archive.org/details/the-celtic-song-book/page/n23/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Silent, oh Moyle"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"Arrah! my dear Eveleen.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 4/4
  \tempo \markup { \italic "Slowly and sadly." } 4 = 60
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global

    \bar ".|:"

    c''4^\p c8 c8 c8([ b8]) b8 b8            | %   1
    a8([ c8]) b8 a8 g8 e4.                   | %   2

    \break

    a4 a8. a16 g8 e8 r8 e8                   | %   3
    a4 b8.^\cresc b16 c4 r8 b8^\p            | %   4

    \break

    c4 e8 c8 c8^\<([ b8\!]) c8^\> b8\!       | %   5
    a8([ c8]) b8 a8 g8 e8 r4                 | %   6

    \break

    a4 a8. a16 a8([ g8]) f8 e8               | %   7
    a4^\<
    <<
      { \voiceOne b4 }
      \new Voice
      { \voiceTwo b8. b16 }
    >>
    \oneVoice
    c4.\! r8                                 | %   8

    \break

    a8^mf([ b8]) c8 d8 e4. e8                | %   9
    f4 e8([ d8]) e8 a,8 r4                   | %  10

    \break

    a4. a8 f'4. e8                           | %  11
    e8([ d8]) d8([ c8]) b2^\<                | %  12
    \once \override Hairpin.shorten-pair = #'(0 . 8)
    e4.\> a,8\! a4 r8 c8                     | %  13

    \break

    b8([ c8]) b8([ a8]) a8([ g8]) f8([ e8])  | %  14
    a4. a8 c8 b8 a8 g8                       | %  15
    a4 b4 c2\fermata                         | %  16

    \bar ":|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Si -- lent oh Moyle, _ be the           | %   1
  roar _ of thy wa -- ter,                | %   2
  Break not, ye breez -- es, your         | %   3
  chain of re -- pose; While              | %   4
  mur -- mur -- ing mourn -- _ ful -- ly, | %   5
  Lir's _ lone -- ly daugh -- ter         | %   6
  Tells to the night -- _ star her        | %   7
  tale of _ woes.                         | %   8
  When _ shall the swan, her              | %   9
  death -- note _ sing -- ing,            | %  10
  Sleep, with wings in                    | %  11
  dark -- _ ness _ furl'd?                | %  12
  When will Heav'n, its                   | %  13
  sweet _ bells _ ring -- _ ing, _        | %  14
  Call my spi -- rit from this            | %  15
  storm -- y world?                       | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Sadly, oh Moyle to thy winter-wave weeping; }
      \line { \hspace #2 { Fate bids me languish long ages away; } }
      \line { Yet still in her darkness doth Erin lie sleeping, }
      \line { \hspace #2 { Still doth the pure light its dawning delay. } }
      \line { When will that day-star, mildly springing, }
      \line { \hspace #2 { Warm our isle with peace and love? } }
      \line { When will Heav'n, its sweet bells ringing, }
      \line { \hspace #2 { Call my spirit to the fields above? } }
    }
  }
}
