.. Page 39 (PDF Page 23, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#################################
Let Erin remember the Days of Old
#################################

.. lilyinclude:: ./let_erin_remember_days_old.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 39
<https://archive.org/details/the-celtic-song-book/page/n22/mode/1up>`_)
