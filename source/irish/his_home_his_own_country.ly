% His Home and His Own Country
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.09 (kjc)
%
% Source: The Celtic Song Book, p. 69
%         https://archive.org/details/the-celtic-song-book/page/n37/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "His Home and His Own Country"
  poet = \markup { \smallCaps { "Emily H. Hickey." } }
  composer = \markup { \italic { "Air:" } "\"All Alive.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \minor
  \time 6/8
  \tempo \markup { \italic "Joyfully." } 4 = 100
}

melody = {
  \relative {
    \global
    \partial 8 f'16([ g16])                      | %   0
    af8([ c8]) af8 bf8 df8 bf8                   | %   1
    af16([ f8.]) f8 f4 g8                        | %   2

    \break

    af8([ c8]) af8 bf8 c8 af8                    | %   3
    g16([ ef8.]) ef8 ef4 g8                      | %   4

    \break

    af8.([ g16]) af8 bf8 af8 bf8                 | %   5
    c4 f8 f8([ ef8]) df8                         | %   6
    c8.([ bf16]) af8 g16([ af16]) bf8 g8         | %   7

    \break

    af8([ f8-.]) f8-. f4-. g8                    | %   8
    af8([ c8]) f8-. ef8 c8 af8-.                 | %   9
    ef'8([ c8]) af8-. ef'8([ c8]) af8            | %  10

    \break

    g8([ bf8]) ef8 bf8 g8 ef8-.                  | %  11
    bf'8([ g8]) ef8-. bf'8([ g8]) ef8-.          | %  12
    af8 c8 f8-. ef8 c8 af8-.                     | %  13

    \break

    af'8([ g8]) f8 ef8([ c8]) af8-.              | %  14
    bf16[ c16( df8)] bf8 g16[ af16] bf8 g8-.     | %  15
    af8([ f8]) f8-. f4-.                           %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  I..                                            | %   0
  know not whe -- ther to                        | %   1
  laugh or cry, So                               | %   2
  great -- ly, ut -- ter -- ly                   | %   3
  glad.. am I; For                               | %   4
  one whose beau -- ti -- ful,                   | %   5
  love -- lit face.. The                         | %   6
  dis -- tance hid for a                         | %   7
  wea -- ry space, Has                           | %   8
  come this day of all                           | %   9
  days to me, Who                                | %  10
  am his home and his                            | %  11
  own coun -- try; Has                           | %  12
  come on this day of all                        | %  13
  days to me, Who                                | %  14
  am.. his home and his                          | %  15
  own coun -- try.                               | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { What shall I say who am hear at rest, }
      \line { Led from the good things up to the best? }
      \line { Little my knowledge, but this I know, }
      \line { It was God said, “Love each other so.” }
      \line { O love, my love, who hast come to me, }
      \line { Thy love, thy home, and thy own country. }
    }
  }
}
