.. Page 51 (PDF Page 29, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##################
Kitty of Coleraine
##################

.. lilyinclude:: ./kitty_coleraine.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 51
<https://archive.org/details/the-celtic-song-book/page/n28/mode/1up>`_)
