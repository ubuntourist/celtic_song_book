% Down by the Sally Gardens
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.03 (kjc)
%
% Source: The Celtic Song Book, p. 46
%         https://archive.org/details/the-celtic-song-book/page/n26/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Down by the Sally Gardens"
  poet = \markup { \smallCaps { "W. B. Yeats." } }
  composer = \markup { \italic { "Air:" } "\"The Maids of Mourne Shore.\"" }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 4/4
  \tempo \markup { \italic "Pensively." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8
    <<
      { \voiceOne f'16[[ g16 ]) }
      \new Voice
      { \voiceTwo f16 g16 }
    >>
    \oneVoice                                              | %   0
    a4( g8[ f8]) g4 a8([ c8])                              | %   1
    d2 c4 f8([ c8])                                        | %   2
    d4 c8([ a8]) g4. f8                                    | %   3

    \break

    f2 r4. f16([ g16])                                     | %   4
    a4 g8([ f8]) g4 a8([ c8])                              | %   5
    d2 c4 f8([ c8])                                        | %   6

    \break

    d4 c8([ a8]) g4 f4                                     | %   7
    f2. c'4                                                | %   8
    f4  e8([ c8]) d4. f8                                   | %   9
    e2 c4 a8 c8                                            | %  10

    \break

    d4 c8([ a8]) c8([ d8]) f8([ g8])                       | %  11
    f2. f,8([ g8])                                         | %  12
    a4 g8([ f8]) g4 a8([ c8])                              | %  13

    \break

    d2 c4 f8([ c8])                                        | %  14
    d4 c8([ a8]) g4. f8                                    | %  15
    f2.                                                      %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Down _                                       | %   0
  by the _ sal -- ly _                         | %   1
  gar -- dens, my _                            | %   2
  love and _ I did                             | %   3
  meet; She _                                  | %   4
  passed the _ sal -- ly.... _                 | %   5
  gar -- dens, with _                          | %   6
  lit -- tle _ snow -- white                   | %   7
  feet. She                                    | %   8
  bid me _ take love                           | %   9
  ea -- sy, as the                             | %  10
  leaves grow _ on _ the _                     | %  11
  tree; But _                                  | %  12
  I, be -- ing young and _                     | %  13
  fool -- ish, with _                          | %  14
  her did _ not a --                           | %  15
  gree.                                        | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { In a field by the river my love and I did stand, }
      \line { And on my leaning shoulder she laid her snow-white hand. }
      \line { She bid me take life easy as the grass grows on the weirs; }
      \line { But I was young and foolish, and now am full of tears. }
    }
  }
}
