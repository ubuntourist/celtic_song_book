.. Page 37 (PDF Page 22, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

############################
Erin, the Tear and the Smile
############################

.. lilyinclude:: ./erin_tear_smile.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 37
<https://archive.org/details/the-celtic-song-book/page/n21/mode/1up>`_)
