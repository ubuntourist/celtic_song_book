% Father O'Flynn
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.19 (kjc)
%
% Source: The Celtic Song Book, p. 76
%         https://archive.org/details/the-celtic-song-book/page/n41/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Father O'Flynn"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = \markup { \italic { "Air:" } "\"The Top of Cork Road.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key bf \major
  \time 6/8
  \tempo \markup { \italic "Playfully" } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 f'8                           | %   0
    bf8 f8 d8 bf8 d8 f8                      | %   1
    bf8 c8 bf8 a8 g8 f8                      | %   2

    \break

    bf4 bf8 c8([ d8]) ef8                    | %   3
    d8 bf8 d8 c8 a8 f8                       | %   4

    \break

    bf8 f8 d8 bf8 d8 f8                      | %   5
    bf8 c8 bf8 a8 g8 f8                      | %   6

    \break

    bf8 bf8 bf8 c8 d8 ef8                    | %   7
    d8 bf8 bf8 bf4.                          | %   8

    \break

    d4 d8 d8 ef8 f8                          | %   9
    c8 c8 c8 f4.                             | %  10

    \break

    bf,8. bf16 bf8 c8. c16 bf8               | %  11
    a8. f16 f8 f4.                           | %  12

    \break

    g8 g8 g8 g8 a8 bf8                       | %  13
    f8 d8 bf8 bf8 d8 f8                      | %  14

    \break

    bf8 bf8 bf8 c8 d8 ef8                    | %  15
    d8 bf8 bf8 bf4                             %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Of                                      | %   0
  priests we can of -- fer a              | %   1
  charm -- in' va -- ri -- e -- ty        | %   2
  Far re -- nown'd for                    | %   3
  larn -- in' and pi -- e -- ty,          | %   4
  Still I'd ad -- vance ye wid --         | %   5
  out im -- pro -- pri -- e -- ty         | %   6
  Fa -- ther O' -- Flynn, as the          | %   7
  flow'r of them all.                     | %   8
  Here's a health to you,                 | %   9
  Fa -- ther O' -- Flynn.                 | %  10
  Slain -- té and slain -- té and         | %  11
  slain -- té a -- gin;                   | %  12
  Pow'r -- ful -- est preach -- er and    | %  13
  tin -- der -- est tea -- cher, and      | %  14
  kind -- li -- est crea -- ture in       | %  15
  ould Don -- e -- gal.                     %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Don't talk of your Provost and Fellows of Trinity, }
      \line { Famous for ever at Greek and Latinity, }
      \line { Faix and the divils and all at Divinity, }
      \line { Father O'Flynn'd make hares of the all!  }
      \line { Come, I venture to give ye my word, }
      \line { Never the likes of his logic was heard, }
      \line { Down from mythology into thayology, }
      \line { Troth! and conchology if he'd the call. }
      \line { \hspace #4 { Here's a health, etc. } }
      \vspace #0.8
      \line { Och, Father O'Flynn, you've a wonderful way wid you, }
      \line { All ould sinners are wishful to pray wid you, }
      \line { All the young chider are wild for to play wid you, }
      \line { You've such a way wid you, Father avick! }
      \line { Still for all you've so gental a soul, }
      \line { Gad, you've your flock in the grandest control; }
      \line { Checking the crazy ones, coaxin' onaisy ones, }
      \line { Lifting the lazy ones on with the stick. }
      \line { \hspace #4 { Here's a health, etc. } }
      \vspace #0.8
      \line { And tho' quite avoidin' all foolish frivolity, }
      \line { Still at all seasons of innocent jollity, }
      \line { Where was the playboy, could claim an equality }
      \line { \hspace #2 { At comicality, Father, wid you? } }
      \line { Once a Bishop looked grave at your jest, }
      \line { Til this remark set him off with the rest: }
      \line { Is it lave gaiety all to the laity? }
      \line { \hspace #2 { Cannot the Clergy be Irishmen too? } }
      \line { \hspace #4 { Here's a health, etc. } }
    }
  }
}
