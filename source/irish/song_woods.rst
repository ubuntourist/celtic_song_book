.. Page 73 (PDF Page 40, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#####################
The Song of the Woods
#####################

.. lilyinclude:: ./song_woods.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 73
<https://archive.org/details/the-celtic-song-book/page/n39/mode/1up>`_)
