% At the Mid Hour of Night
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.03 (kjc)
%
% Source: The Celtic Song Book, p. 46
%         https://archive.org/details/the-celtic-song-book/page/n26/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "At the Mid Hour of Night"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"Molly, my dear.\"" }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key d \major
  \time 3/4
  \tempo \markup { \italic "Moderately slow." } 4 = 70
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 fs'8^\p g8                                  | %   0
    a4 a4 b4                                               | %   1
    g2  e4                                                 | %   2
    fs2 d4                                                 | %   3

    \break

    e4 d8 r8 d4                                            | %   4
    d2 fs8 g8                                              | %   5
    a4 a4 b4                                               | %   6
    g2 e4                                                  | %   7

    \break

    fs2 d4                                                 | %   8
    e4 d4 d4                                               | %   9
    d2 a'8^\pp g8                                          | %  10
    fs4 d4 fs4                                             | %  11

    \break

    a4. b8 cs4                                             | %  12
    d4 e4. d8                                              | %  13
    cs4 a4 fs4                                             | %  14

    \break

    g2
    <<
      { \voiceOne a8 g8 }
      \new Voice
      { \voiceTwo a8([ g8]) }
    >>
    \oneVoice                                              | %  15
    fs4^\cresc d4 fs4                                      | %  16
    a4. b8 cs4                                             | %  17

    \break

    d4 e4.\f d8                                            | %  18
    cs4 a4 fs4                                             | %  19
    \tempo \markup { \italic "Slower." } 4 = 65
    g2^\>
    <<
      { \voiceOne fs8\!\p([ g8]) }
      \new Voice
      { \voiceTwo fs8 g8 }
    >>
    \oneVoice                                              | %  20
    a4 a4 b4                                               | %  21

    \break

    g2
    <<
      { \voiceOne e8 e8 }
      \new Voice
      { \voiceTwo e4 }
    >>
    \oneVoice                                              | %  22
    fs2 d4                                                 | %  23
    e4 d4 d4                                               | %  24
    d2.\fermata                                            | %  25

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  At the                                       | %   0
  mid hour of                                  | %   1
  night, when                                  | %   2
  stars are                                    | %   3
  weep -- ing, I                               | %   4
  fly To the                                   | %   5
  lone vale we                                 | %   6
  lov'd, when                                  | %   7
  life shone                                   | %   8
  warm in thine                                | %   9
  eye; And I                                   | %  10
  think oft, if                                | %  11
  spi -- rits can                              | %  12
  steal from the                               | %  13
  re -- gions of                               | %  14
  air To re --                                 | %  15
  vis -- it past                               | %  16
  scenes of de --                              | %  17
  light, thou wilt                             | %  18
  come to me                                   | %  19
  there, And _                                 | %  20
  tell me our                                  | %  21
  love is re --                                | %  22
  mem -- ber'd                                 | %  23
  even in the                                  | %  24
  sky!                                         | %  25
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { Then I sing the wild song 'twas once such pleasure to hear, }
      \line { When our voices, commingling, breath'd, like one, on the ear; }
      \line { And, as Echo far off thro' the vale my sad orison rolls, }
      \line { I think, O my love! 'tis thy voice from the Kingdom of Souls,}
      \line { Faintly answering still the notes that once were so dear. }

    }
  }
}
