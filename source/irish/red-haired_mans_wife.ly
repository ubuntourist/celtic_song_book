% The Red-Haired Man's Wife
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.15 (kjc)
%
% Source: The Celtic Song Book, p. 56
%         https://archive.org/details/the-celtic-song-book/page/n36/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Red-Haired Man's Wife"
  poet = \markup {
    \center-column {
      \line { \smallCaps { "Katharine Hinkson" } "(Tynan)." }
      \line { \italic { "(Adapted to Music by the Editor.)" } }
    }
  }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"Bean an fhir ruaidh.\"" }
      \line { \italic { "(The red-haired man's wife.)" } }
    }
  }
  tagline = ""
% instrument = "instrument"
% subtitle = "subtitle"
% arranger = "arranger"
% copyright = "copyright"
% composer = "composer"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 3/4
  \tempo \markup { \italic "Slowly and smoothly." } 4 = 70
% \override TupletBracket.tuplet-slur = ##t              % Not available til v. 2.21
  \override TupletBracket.bracket-visibility = ##t
% \tupletUp
}

melody = {
  \relative {
    \global
    \partial 8 g'8                                     | %   0
    c8.([ d16]) e8([ f8]) d8([ b8])                    | %   1
    c2 d8([ b8])                                       | %   2
    g2 f8([ e8])                                       | %   3

    \break

    f4 g4 a8([ b8])                                    | %   4
    c2 \tuplet 3/2 { g8 [a8 b8] }                      | %   5
    c8.([ d16]) e8([ f8]) d8([ b8])                    | %   6

    \break

    c4.( e8) d8( b8)                                   | %   7
    g4.( f8) e8 f8                                     | %   8
    d4 c4 c4                                           | %   9
    c2                                                   %  10

    \bar "|."

    \break

    \partial 4 c'8.([ a16])                            | %  11
    bf8([ a8]) g8([ f8]) d8([ e8])                     | %  12
    c4 bf'4 a8([ g8])                                  | %  13
    f4. g8 f8 e8                                       | %  14

    \break

    f4 g4 a8 b8                                        | %  15
    c2 \tuplet 3/2 { g8[ a8 b8] }                      | %  16
    c8.([ d16]) e8([ f8]) d8 b8                        | %  17

    \break

    c2 d8([ b8])                                       | %  18
    g4. f8 e8 f8                                       | %  19
    d4 c4 c4                                           | %  20
    c2 r8                                                %  21

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Though                                               | %   0
  full as 'twill                                       | %   1
  hold of                                              | %   2
  gold the                                             | %   3
  har -- vest has                                      | %   4
  smiled, I'll....                                     | %   5
  ne'er have re --                                     | %   6
  lief.... from                                        | %   7
  grief... for that                                    | %   8
  fond grey -- eyed                                    | %   9
  child,                                                 %  10
  Whom                                                 | %  11
  kin -- dred most                                     | %  12
  cru -- el, poor                                      | %  13
  jew -- el, in -- to                                  | %  14
  love -- less wed -- ded                              | %  15
  life, With....                                       | %  16
  an -- guish be it                                    | %  17
  told, have                                           | %  18
  sold to be the                                       | %  19
  Red -- haired Man's                                  | %  20
  wife.                                                  %  21
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { That fond valentine of mine a letter I sent, }
      \line { That I'd soon sail with store galore to wed her ere Lent. }
      \line { Her friends stole the note I wrote, and far worse than a knife }
      \line { Have slain my bright pearl for a churl: she's the Red-haired Man's wife. }

      \vspace #0.8

      \line { Oh, child and sweetheart, their art had you but withstood }
      \line { Till I had come home o'er the foam for our great joy and good, }
      \line { I had not now to go under woe o'er the salt sea's strife }
      \line { A wanderer to France from the glance of the Red-haired Man's wife. }

    }
  }
}
