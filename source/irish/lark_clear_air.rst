.. Page 53 (PDF Page 30, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#####################
The Lark in Clear Air
#####################

.. lilyinclude:: ./lark_clear_air.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 53
<https://archive.org/details/the-celtic-song-book/page/n29/mode/1up>`_)
