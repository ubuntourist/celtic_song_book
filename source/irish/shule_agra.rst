.. Page 50 (PDF Page 29, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##########
Shule Agra
##########

.. lilyinclude:: ./shule_agra.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 50
<https://archive.org/details/the-celtic-song-book/page/n28/mode/1up>`_)
