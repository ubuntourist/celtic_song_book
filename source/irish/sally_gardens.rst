.. Page 55 (PDF Page 31, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
Down by the Sally Gardens
#########################

.. lilyinclude:: ./sally_gardens.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 55
<https://archive.org/details/the-celtic-song-book/page/n30/mode/1up>`_)
