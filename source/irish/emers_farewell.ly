% Emer's Farewell
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.15 (kjc)
%
% Source: The Celtic Song Book, p. 67
%         https://archive.org/details/the-celtic-song-book/page/n36/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Emer's Farewell"
  poet = \markup { \smallCaps { "Alfred Perceval Graves." } }
  composer = "Londonderry Air."
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 4/4
% \tempo \markup { \italic "Unlisted." } 4 = ...
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4. d'8 ef8 f8                                 | %   0
    g4. f8 g8 c8 bf8 g8                                    | %   1
    f8([ ef8]) c4 r8 ef8 g8 af8                            | %   2

    \break

    bf4. c8 bf8 g8 ef8 g8                                  | %   3
    f2 r8 d8-. ef8-. f8-.                                  | %   4
    g4. f8 g8 c8 bf8-. g8-.                                | %   5

    \break

    f8([ ef8]) c8-. bf8-. c8 (d8 ef8) f8                   | %   6
    g4. af8 g8 (f8 ef8) f8                                 | %   7

    \break

    ef4 ef4 ef8 bf'8 c8 d8                                 | %   8
    ef4. d8 d8 c8 bf8 g8                                   | %   9

    \break

    bf8([ g8]) ef4 r8 bf'8 c8 d8                           | %  10
    ef4. d8 d8 c8 bf8 g8                                   | %  11

    \break

    f2 r8 bf8^\< bf8 bf8^\!                                | %  12
    g'4.^\f f8 f8 ef8 c8 ef8                               | %  13

    \break

    bf8([ g8]) ef4^\>( ef8^\!) d8 ef8 f8                   | %  14
    g8([ c8 bf8]) g8 f8([ ef8]) c8([ d8])                  | %  15
    ef4 ef4 ef4 r4                                         | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  O might a                                    | %   0
  maid con -- fess her se -- cret              | %   1
  long -- _ ing To one who                     | %   2
  dear -- ley loves, but may not               | %   3
  speak! A -- las! I                           | %   4
  had not hid -- den to thy                    | %   5
  wrong -- _ ing A bleed -- _  _ ing           | %   6
  heart be -- neath... _ _ a                   | %   7
  smil -- ing cheek; I had not                 | %   8
  stemmed my bit -- ter tears from             | %   9
  start -- _ ing, And thou hadst               | %  10
  learned my bo -- som's dear dis --           | %  11
  tress, And half the                          | %  12
  pain, the cru -- el pain of                  | %  13
  part -- _ ing... _ Had passed Cu --          | %  14
  cul -- _ _ lain, in _ thy _                  | %  15
  fond ca -- ress.                             | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \fill-line {
    \center-column {
      \line { \italic { (Reproduced by kind permission of Messrs. Boosey and Co., Ltd.) } }
    }
  }
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { But go! Connacia's hostile trumpets call thee, }
      \line { \hspace #2 { Thy chariot mount and ride the Ridge of War, } }
      \line { And prove whatever feat of arms befall thee,  }
      \line { \hspace #2 { The hope and pride of Emer of Lismore; } }
      \line { Ah, then return, my hero, girt with glory, }
      \line { \hspace #2 { To knit my virgen heart so near to thine, } }
      \line { That all who seek thy name in Erin's story }
      \line { \hspace #2 { Shall find its loving letters linked with mine. } }
    }
  }
}
