.. Page 72 (PDF Page 40, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
The Meeting of the Waters
#########################

.. lilyinclude:: ./meeting_waters.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 72
<https://archive.org/details/the-celtic-song-book/page/n39/mode/1up>`_)
