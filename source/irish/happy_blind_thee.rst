.. Page 69 (PDF Page 38, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################################
Happy 'tis, thou Blind, for Thee
################################

.. lilyinclude:: ./happy_blind_thee.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 69
<https://archive.org/details/the-celtic-song-book/page/n37/mode/1up>`_)
