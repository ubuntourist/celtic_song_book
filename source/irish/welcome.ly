% The Welcome
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.10 (kjc)
%
% Source: The Celtic Song Book, p. 58
%         https://archive.org/details/the-celtic-song-book/page/n32/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Welcome"
  poet = \markup { \smallCaps { "Thomas Davis." } }
  composer = \markup {
    center-column {
      \line { \italic { "Air:" } "\"Mo Bhuachailin Buidhe.\"" }
      \line { \italic { "(My yellow-haired little boy.)" } }
    }
  }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key bf \major
  \time 6/8
  \tempo \markup { \italic "Brightly." } 4 = 90
}

melody = {
  \relative {
    \global
    f'8 bf8 bf8 d8 bf8 bf8                       | %   1
    c8 bf8 bf8 d8([ bf8]) bf8                    | %   2

    \break

    f8 bf8 bf8 d8 bf8 bf8                        | %   3
    c8 bf8 g8 g8([ f8]) d8                       | %   4

    \break

    f8 bf8 bf8 d8 bf8 bf8                        | %   5
    c8 bf8 bf8 d8 bf8 bf16 c16                   | %   6

    \break

    d8 d8 f8 ef8 d8 c8                           | %   7
    d8 bf8 f8 g4 bf8                             | %   8

    \break

    f8.([ g16]) f16 d16 bf8 d8 f8                | %   9
    bf,8 d8 f8 g4 bf8                            | %  10

    \break

    f8.([ g16]) f16 d16 bf8 d8 f8                | %  11
    d8 c8 f8 g4 bf16 bf16                        | %  12

    \break

    f8.([ g16]) f16 d16 bf8 d8 f8                | %  13
    bf,8 d8 f8 bf8 bf8 bf16 c16                  | %  14

    \break

    d8 c8 bf8 ef8 d8 c8                          | %  15
    d8 bf8 f8 g8 bf4                             | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Come in the ev'n -- ing or                     | %   1
  come in the morn -- ing,                       | %   2
  Come when you're look'd for or                 | %   3
  come with -- out warn -- ing;                  | %   4
  Kiss -- es and wel -- come you'll              | %   5
  find here be -- fore you, And the              | %   6
  oft -- 'ner you come here, the                 | %   7
  more I'll a -- dore you.                       | %   8
  Light is my heart since the                    | %   9
  day we were plight -- ed,                      | %  10
  Red is my cheek that they                      | %  11
  told me was blight -- ed, The                  | %  12
  green of the trees looks far                   | %  13
  green -- er than ev -- er, And the             | %  14
  lin -- nets are sing -- ing, ”True             | %  15
  lov -- ers don't sev -- er!“                   | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Ill pull you sweet flowers to wear if you choose 'em, }
      \line { Or, after you've kiss'd them they'l lie on my bosom; }
      \line { We'll look on the stars, and we'll list on the river, }
      \line { Till you ask of your darling what gift can you give her. }
      \line { Oh! she'll whisper you, “Love as unchangeably beaming, }
      \line { And trust, all in secret, as tunefully streaming, }
      \line { Till the starlight of heaven above us shall quiver, }
      \line { And our souls flow in one down eternity's river.“ }
    }
  }
}
