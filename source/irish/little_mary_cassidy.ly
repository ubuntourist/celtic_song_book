% Little Mary Cassidy
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.12 (kjc)
%
% Source: The Celtic Song Book, p. 62
%         https://archive.org/details/the-celtic-song-book/page/n34/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Little Mary Cassidy"
  poet = \markup { \smallCaps { "Francis A. Fahy." } }
  composer = \markup { \italic { "Air:" } "\"The Little Stack of Barley.\"" }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key df \major
  \time 4/4
  \tempo \markup { \italic "With buoyant tenderness." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 f'8 af8                                     | %   0
    bf8. c16 bf8. af16 f8. bf16 af8. f16                   | %   1

    \break

    ef8. df16 ef8. df16 ef8. gf16 f8.
    <<
      { \voiceOne ef16 }
      \new Voice
      { \voiceTwo ef32 ef32 }
    >>
    \oneVoice                                              | %   2

    \break

    df8. c16 df8. ef16 f8. ef16 f8. af16                   | %   3
    bf8. ef,16 ef8. ef16 ef4
    <<
      { \voiceOne f8 af8 }
      \new Voice
      { \voiceTwo f8[ af8] }
    >>
    \oneVoice                                              | %   4

    \break

    bf8. c16 bf8. af16
    <<
      { \voiceOne f8.[ bf16] }
      \new Voice
      { \voiceTwo f8. bf16 }
    >>
    \oneVoice
    af8. f16                                               | %   5
    ef8. df16 ef8. df16 ef8. gf16 f8
    <<
      { \voiceOne ef8 }
      \new Voice
      { \voiceTwo ef16 ef16 }
    >>
    \oneVoice                                              | %   6

    \break

    df8. c16 df8. ef16 f8. af16 ef8. f16                   | %   7
    df4 df4 df4
    <<
      { \voiceOne af'8. gf16 }
      \new Voice
      { \voiceTwo af8.[ gf16] }
    >>
    \oneVoice                                              | %   8

    \break

    f8. df16 f8. af16 df4 c8([ df8])                       | %   9
    ef8. df16 c8. af16 bf4
    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8 c8 }
    >>
    \oneVoice                                              | %  10

    \break

    df8. c16 bf8. af16
    <<
      { \voiceOne f8.[ ef16] }
      \new Voice
      { \voiceTwo f8. ef16 }
    >>
    \oneVoice
    f8. af16                                               | %  11
    <<
      { \voiceOne bf8. ef,16 ef8. ef16 ef4 }
      \new Voice
      { \voiceTwo bf'4 ef,4 ef4 }
    >>
    \oneVoice
    c'8 df8                                                | %  12

    \break

    ef8. ef16 c8. af16
    <<
      { \voiceOne df8. df16 }
      \new Voice
      { \voiceTwo df4 }
    >>
    \oneVoice
    c8. bf16                                               | %  13
    af8. f16 ef8. df16 ef8. gf16 f8. ef16                  | %  14

    \break

    df8. c16 df8. ef16 f8. af16 ef8. f16                   | %  15
    df8. df16 df8 df8 df4\fermata                            %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh, 'tis                                     | %   0
  lit -- tle Ma -- ry Cas -- si -- dy's the    | %   1
  cause of all my mis -- er -- y, The _        | %   2
  rai -- son that I am not now the             | %   3
  boy I used to be; Oh, she                    | %   4
  bates the beau -- ties all.. _ that we       | %   5
  read a -- bout in his -- to -- ry, Sure _    | %   6
  half the coun -- try side's as lost for      | %   7
  her as me. Trav -- el                        | %   8
  Ire -- land up and down, hill, _             | %   9
  vil -- lage, vale, and town, Girl _          | %  10
  like my col -- leen dhown _ you'll be        | %  11
  look -- ing for in vain. Oh, I'd             | %  12
  ra -- ther live in po -- ver -- ty with      | %  13
  lit -- tle Ma -- ry Cas -- si -- dy, Than    | %  14
  Em -- per -- or with -- out her be o'er      | %  15
  Ger -- ma -- ny or Spain.                    | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { 'T'was at the dance at Darmody's that first I caught sight of her, }
      \line { And hear her sing the Drinan Donn, till thears came in my eyes, }
      \line { And ever since that blessed hour I'm dreaming day and night of her; }
      \line { The divil a wink of sleep at all I get from bed to rise. }
      \line { Cheeks like the rose in June, song like the lark in tue, }
      \line { Working, resting, night or noon, she never leaves my mind; }
      \line { Oh, till singing by my cabin fire sits little Mary Cassidy, }
      \line { 'Tis little aise or happiness I'm sure I'll never find. }
      \vspace #0.8
      \line { What is wealth, what is fame, what is all that people fight about, }
      \line { To a kind word from her lips or a love-glace from her eye? }
      \line { Oh, though troubles throng my breast, sure they'd soon go to the right-about }
      \line { If I thought the curly head of her would rest there by and by. }
      \line { Take all I own to-day, kith, kin, and care away, }
      \line { Ship them all across the say, or to the frozen zone; }
      \line { Lave me an orhphan bare⸺but lave me Mary Cassidy, }
      \line { I never would feel lonely with the two of us alone. }
    }
  }
}
