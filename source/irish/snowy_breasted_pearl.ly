% The Snowy-Breasted Pearl
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.08 (kjc)
%
% Source: The Celtic Song Book, p. 54
%         https://archive.org/details/the-celtic-song-book/page/n30/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Snowy-Breasted Pearl"
  poet = \markup { Translated from the Irish by \smallCaps { "George Petrie." } }
  composer = \markup {
    \center-column {
      \line { \italic { "Air:" } "\"Péarla an bhrollaigh bháin.\"" }
      \line { \italic { "(The Pearl of the White Breast.)" } }
    }
  }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 4/4
  \tempo \markup { \italic "Not too slowly." } 4 = 90
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 4 g'8 a16([ b16])               | %   0
    c8 b8 a8 g8 c4. a16 a16                  | %   1

    \break

    g8 e8 d8 c8 d4 e8 f8                     | %   2
    g8 g8 e8 c8 f8 e8 d8. c16                | %   3

    \break

    c2.\fermata g'8 a16([ b16])              | %   4
    c8 b8 a8 g8 c,4 e8([ a8])                | %   5

    \break

    g8 e8 d8 c8 d4 e8 f8                     | %   6
    g8. g16 e8 c8 f8. e16 d8 c8              | %   7

    \break

    c2.  g'8. f16                            | %   8
    e8 g8 a8 c8 b4 r8 g8                     | %   9

    \break

    c8 b8 a8. g16 g4 r8 g8                   | %  10
    a8 b8 c8 e8 d8 c8 b8 a8                  | %  11

    \break

    g2. g8([ a16 b16])                       | %  12
    c8 b8 a8 g8 c4 e,8([ a8])                | %  13

    \break

    g8 e8 d8 c8 d4 e8 f8                     | %  14
    g8 g8 e8 c8 f8 e8 d8. c16                | %  15
    c2.                                        %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  There's a _                                  | %   0
  col -- leen fair as May, For a               | %   1
  year and for a day I have                    | %   2
  sought by ev' -- ry way Her heart to         | %   3
  gain. There's no _                           | %   4
  art of tongue or eye Fond _                  | %   5
  youths with maid -- ens try. But I've        | %   6
  tried with cease -- less sigh, Yet tried in  | %   7
  vain. If to                                  | %   8
  far -- off France or Spain She               | %   9
  crossed the rag -- ing main, Her             | %  10
  face to see a -- gain The seas I'd           | %  11
  brave. But................. _ _              | %  12
  if 'tis Heav'n's de -- cree That _           | %  13
  mine she may not be, May the                 | %  14
  Son of Ma -- ry me In mer -- cy              | %  15
  save.                                          %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { Oh, thou blooming milk-white dove }
        \line { To whom I've given my love, }
        \line { Do not ever thus reprove }
        \line { \hspace #2 { My constancy. } }
        \combine \null \vspace #0.8
        \line { There are maidens would be mine, }
        \line { With wealth in land and kine, }
        \line { If my heart would but incline }
        \line { \hspace #2 { To turn from thee. } }
      }
      \hspace #10 \raise #1 \draw-line #'(0 . -28) \hspace #10
      \left-column {
        \combine \null \vspace #0.8
        \line { But a kiss with welcome bland }
        \line { And touch of thy fair hand, }
        \line { Is all that I demand, }
        \line { \hspace #2 { Wouldst thou not spurn. } }
        \combine \null \vspace #0.8
        \line { For if not mine, dear girl, }
        \line { Oh, snowy-breasted pearl, }
        \line { May I never from the fair }
        \line { \hspace #2 { With life return. } }
      }
    }
  }
}
