.. Page 41 (PDF Page 24, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################
Silent, oh Moyle
################

.. lilyinclude:: ./silent_oh_moyle.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 41
<https://archive.org/details/the-celtic-song-book/page/n23/mode/1up>`_)
