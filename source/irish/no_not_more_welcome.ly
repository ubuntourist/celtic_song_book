% No, not more Welcome
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.10 (kjc)
%
% Source: The Celtic Song Book, p. 61
%         https://archive.org/details/the-celtic-song-book/page/n33/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "No, not more Welcome"
  subtitle = "(Erin to Grattan)"
  poet = \markup { \smallCaps { "Thomas Moore." } }
  composer = \markup { \italic { "Air:" } "\"Luggelaw.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 3/4
  \tempo \markup { \italic "With expression." } 4 = 70
}

%   c8([ b8]) \appoggiatura { a16 [ b16] } a8.([ gs16]) a2  | %   8
melody = {
  \relative {
    \global
    r4 r8 ef'16([ f16]) g8 g8                               | %   1
    f8.([ g16]) af8 bf8 c8 c8                               | %   2
    bf8.([ g16]) ef8 f8 g8 bf8                              | %   3

    \break

    bf4 g8 ef8 \appoggiatura g8 f8. ef16                    | %   4
    ef4 r8 ef16([ f16]) g8 g8                               | %   5

    \break

    f8.([ g16]) af8 bf8 c8 c8                               | %   6
    bf8.([ g16]) ef8 f8 g8 bf8                              | %   7

    \break

    c8([ bf8]) g8 ef8 \appoggiatura g16 f8. ef16            | %   8
    ef4 r8 bf'8 bf8. c16                                    | %   9

    \break

    c4 ef4 \appoggiatura d16 c8. bf16                       | %  10
    g8([ bf8]) bf4 c8([ d16 ef16])                          | %  11
    g,4 af8 g8 \appoggiatura g16 f8 ef8                     | %  12

    \break

    \appoggiatura ef8 f4 r8 g8 af8 g8                       | %  13
    f8.([ g16]) af8 bf8 c16 c8.                             | %  14

    \break

    bf8.([ g16]) ef8. f16 g8 bf8                            | %  15
    c8([ bf8]) g8 ef8 \appoggiatura g16 f8. ef16            | %  16
    ef4. r8 r4                                              | %  17

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  No, not more                               | %   1
  wel -- come the fai -- ry                  | %   2
  num -- bers Of mu -- sic                   | %   3
  fall on the sleep -- er's                  | %   4
  ear, When half a --                        | %   5
  wak -- ing from fear -- ful                | %   6
  slum -- bers, He thinks the                | %   7
  full... quire of heav'n is                 | %   8
  near, Than came that                       | %   9
  voice, when all for --                     | %  10
  sak -- en, This......                      | %  11
  heart long had sleep -- ing                | %  12
  lain, Nor thought its                      | %  13
  cold pulse would ev -- er                  | %  14
  wak -- en to such be --                    | %  15
  nign, bless -- ed sounds a --              | %  16
  gain.                                      | %  17
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Sweet voice of comfort! 'twas like the stealing }
      \line { \hspace #2 { Of summer wind through some wreathed shell: } }
      \line { Each secret winding, each inmost feeling }
      \line { \hspace #2 { Of my soul echoed to its spell! } }
      \line { 'T'was whisper'd balm⸺'twas sunshine spoken: }
      \line { \hspace #2 { I'd live years of grief and pain } }
      \line { To have my long sleep of sorrow broken }
      \line { \hspace #2 { By such benign, blessed sounds again } }
    }
  }
}
