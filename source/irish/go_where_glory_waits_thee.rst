.. Page 38 (PDF Page 23, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########################
Go where Glory waits Thee
#########################

.. lilyinclude:: ./go_where_glory_waits_thee.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 38
<https://archive.org/details/the-celtic-song-book/page/n22/mode/1up>`_)
