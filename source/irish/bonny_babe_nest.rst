.. Page 48 (PDF Page 28, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###############################
I've found my Bonny Babe a Nest
###############################

.. lilyinclude:: ./bonny_babe_nest.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 48
<https://archive.org/details/the-celtic-song-book/page/n27/mode/1up>`_)
