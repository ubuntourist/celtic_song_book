% The Widow Malone
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.18 (kjc)
%
% Source: The Celtic Song Book, p. 74
%         https://archive.org/details/the-celtic-song-book/page/n40/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Widow Malone"
  poet = \markup { \smallCaps { "Charles Lever." } }
  composer = \markup { \italic { "Air:" } "\"The Gap in the Hedge.\"" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key bf \major
  \time 6/8
  \tempo \markup { \italic "Briskly." } 4 = 100
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 d''16 c16                     | %   0
    bf8. g16 f8 f8. g16 bf8                  | %   1
    bf4 bf8 bf8 r8
    <<
      { \voiceOne f'16([ ef16]) }
      \new Voice
      { \voiceTwo f16 ef16 }
    >>
    \oneVoice                                | %   2

    \break

    d8. c16 bf8 bf8. c16 d8                  | %   3
    ef4 ef8 ef,8 r8 g16 f16                  | %   4

    \break

    ef8. f16 ef8 ef'4 d16 ef16               | %   5
    f8 ef8 d8 c4
    <<
      { \voiceOne bf16([ c16]) }
      \new Voice
      { \voiceTwo bf16 c16 }
    >>
    \oneVoice                                | %   6

    \break

    d8 bf8 g8 f8 g8 bf8                      | %   7
    c4 d8 c4\fermata
    <<
      { \voiceOne f16([ ef16]) }
      \new Voice
      { \voiceTwo f16 ef16 }
    >>
    \oneVoice                                | %   8

    \break

    d8 bf8 g8 f8 g8 bf8                      | %   9
    bf4.( bf8) r8                              %  10

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Did ye                                  | %   0
  hear of the Wi -- dow Ma --             | %   1
  lone, O -- hone! Who _                  | %   2
  lived in the town of Ath --             | %   3
  lone? A -- lone! Oh, she                | %   4
  melt -- ed the hearts of the            | %   5
  swains in them parts, So _              | %   6
  love -- ly the Wi -- dow Ma --          | %   7
  lone, O -- hone! So _                   | %   8
  love -- ly the Wi -- dow Ma --          | %   9
  lone.                                     %  10
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺  
\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { Of lovers she had a full score, }
        \line { \hspace #25 { Or more; } }
        \line { And fortunes they all had galore, }
        \line { \hspace #25 { In store; } }
        \line { \hspace #2  { From the Minister down } }
        \line { \hspace #2  { To the Clerk of the Crown, } }
        \line { All were courting the widow Malone, }
        \line { \hspace #25 { Ohone! } }
        \line { All were courting the widow Malone. }
        \combine \null \vspace #1.8
        \line { But so modest was Mrs. Malone, }
        \line { \hspace #25 { 'Twas known } }
        \line { No one ever could see her alone, }
        \line { \hspace #25 { Ohone! } }
        \line { \hspace #2  { Let them ogle and sigh, } }
        \line { \hspace #2  { They could ne'er catch her eye, } }
        \line { So bashful the widow Malone, }
        \line { \hspace #25 { Ohone! } }
        \line { So bashful the widow Malone. }
      }
      \hspace #4 \raise #1 \draw-line #'(0 . -62) \hspace #4
      \left-column {
        \combine \null \vspace #0.8
        \line { Till one Mr. O'Brien from Clare⸺   }
        \line { \hspace #25 { How quare! } }
        \line { It's little for blushing they care }
        \line { \hspace #25 { Down there⸺   } }
        \line { \hspace #2  { Put his arms round her waist, } }
        \line { \hspace #2  { Gave ten kisses at laste⸺   } }
        \line { “Oh,” says he, ”you're my Molly Malone, }
        \line { \hspace #25 { My own!” } }
        \line { ”Oh,” says he, ”you're my Molly Malone!” }
        \combine \null \vspace #1.8
        \line { And the wido they all thought so shy, }
        \line { \hspace #25 { My eye! } }
        \line { Ne'er thought of a simper or sigh⸺   }
        \line { \hspace #25 { For why? } }
        \line { \hspace #2  { But ”Lucius,” says she, } }
        \line { \hspace #2  { ”Since you've now made so free, } }
        \line { You may marry your Molly Malone, }
        \line { \hspace #25 { Ohone! } }
        \line { You may marry your Molly Malone!” }
      }
    }
  }
}
