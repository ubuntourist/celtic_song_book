.. Page 332 (PDF Page 170)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########
back page
#########

Malgré le vent du nord-est nous resterons toujours la main dans la
main, et nous verrons de toutes parts les recrues accourir à l'armée
pour défendre les droits du pays.

Pour défendre le Breton aussi honorable que le Français, le plus petit
sera courageux et le plus faible sera fort, car reculer serait un
honte.

Chaque Breton nous est parent, chaque Bretonne nous est soeur, dans ce
pays nous sommes tous égaux, en nous entr'aidant l'un l'autre, la
Bretagne sera glorieuse bientôt.

Nous avons semé le blé dans les champs des Bretons, nous
le moissonnerons aussi, et le bon grain dans notre maison
nourrira encore plusieurs générations.

Avant d'achever ma chanson, il me reste à souhaiter que s'augmente
journellement le nombre de ceux qui ont au fond de la poitrine un
véritable amour pour leur Bretagne.

.. container:: center

   | PRINTED IN GREAT BRITAIN BY
   | BILLING AND SONS, LTD., GUILDFORD AND ESHER

----

(Source: `The Celtic Song Book, p. 332
<https://archive.org/details/the-celtic-song-book/page/n169/mode/1up>`_)
