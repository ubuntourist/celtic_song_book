.. Page 332 (PDF Page 170)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#########
back page
#########

Despite the north-easterly wind, we will always stand hand in hand,
and we will see recruits from all sides rushing to the army to defend
the rights of the country.

To defend the Breton as honorable as the French, the smallest will be
courageous and the weak will be strong, because to retreat would be a
shame.

Each Breton is a parent to us, each Breton is a sister to us, in this
country we are all equal, by helping each other, Brittany will soon be
glorious.

We have sown wheat in the fields of the Britons, we will also reap it,
and the good grain in our house will nourish many more generations.

Before finishing my song, it remains for me to wish that the number of
those who have deep in their hearts a real love for their Brittany
will increase daily.

.. container:: center

   | PRINTED IN GREAT BRITAIN BY
   | BILLING AND SONS, LTD., GUILDFORD AND ESHER

----

(Source: `The Celtic Song Book, p. 332
<https://archive.org/details/the-celtic-song-book/page/n169/mode/1up>`_)
