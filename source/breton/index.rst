.. Page 281 (PDF Page 144, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

########################
CELTIC MUSIC IN BRITTANY
########################

One must not conclude that Breton music, because it has been kept only
amongst the people, is without rules of any kind; this would be a
great mistake. "The rusticity of these folk songs," says a Breton
composer, M. Duhamel, "really veils a very complex and clever
tradition, hides very precise and strict rules, however ignored by
those who observe them without knowledge."

The popular melodies of Brittany can be classified as follows: songs,
hymns, dancing tunes.

The songs are of two kinds: the historical song called *Gwerz* and
the lyric song called *Son*. The *Gwerz* has no refrain and the melody
is often very simple. The *Son*, which is generally a love song, sometimes
a lullaby, has always a refrain |---| the *Diskan* |---| which is repeated
by the crowd listening to the singer.

The melody of hymns is built on the same type as that of the *Gwerz*,
and hymns have often been written to be sung to the same tune as
well-known *gwerziou*. The dance music is extremely abundant, and can
compete in number with the Scottish dancing tunes. It is very curious
to notice that the rhythm most generally adopted is very much the same
as that of the Scottish reel. Indeed, many Breton dances are *in* reel
tune measures.

The instrument used by the Bretons is the bagpipe, but if in its
construction it is more primitive and smaller than the Scottish, in
sound it is more powerful and more piercing. The bagpipe is never
played by itself; it is always accompanied by another instrument, a
kind of hautboy, called a *Bombard*, and sometimes, but very rarely,
by a small drum. It is an interesting fact that the first two
instruments *are not in tune*. There is between the two a small
interval, which at the beginning is quite perceptible, but very soon
disappears, without any doubt under the efforts of the player blowing
into his instrument with all his strength and "forcing the note".

There is nothing more strange than to see the pipers at a Breton
dance, generally in a field or some public place. In a corner are two
big casks, on the top of which stand the pipers, doing their best to
make as much noise as possible, and marking the time with their right
foot. They play as long as they can bear the strain, and then stop
suddenly with an *appoggiatura*, finishing on a high-pitched note.

Before going further, I should like to point out most emphatically
that our national music must not be confused with a swarm of
third-rate songs, generally, but unfortunately, not always, in French,
written by French composers for music halls; sometimes I am sorry to
say, by some of my countrymen, and which are to be found all over the
market, where they are sold as Breton music. Their rhythm and the mode
in which they are written, generally minor, will allow a trained ear
to detect them at once.

For there is amongst those who are not well acquainted with Breton
music |---| I ought to say with the music of every Celtic country
|---| a very curious belief that the Celts always sing in the
minor. This belief arises quite naturally from the fact that the
learned music, the only kind actually taught in the schools, knows
only two modes, the major and the minor, and that on hearing a Celt
sing in another mode, the ear of the foreign listener is always led,
by a natural process, to assimilate this old mode to one for which it
has been trained. The unavoidable conclusion is that the singer sings
quite *out of tune*.

As a matter of fact, the Celtic tunes are infinitely richer than the
learned music in what forms the very elements of any kind of music, I
mean to say rhythm and modes; and what is true for the Celtic
countries in general applies also to Brittany.

There are rules in learned music, according to which a phrase of
melody must be composed of four or eight bars. After being observed
for many centuries, this has been partially abandoned in modern times,
but after long discussions. "Such a rule," notes a Breton musician,
"has been quietly violated by our countrymen ever since there have
been Celts who sing." And if you study the folk songs of the Celtic
countries, you will find phrases of 2, 3, 4, 5, 6, and 7 bars, a
licence which, far from doing any harm, gives them a freedom and a
variety not to be found in learned music.

The time also presents peculiarities worthy to be noticed. Besides the
double, triple, and quadruple time, the Celtic music makes an
exhaustive use of measures of 5, , and even 9 beats to the bar. It is
quite true that we may now find the latter used in the work of modern
composers, but their introduction is very recent. The Celts, on the
contrary, have always been accustomed to use them.

In many Breton songs, we may even find the unit of time to be the
crotchet for the first half of the bar, and the dotted crotchet for
the other half. This gives us a 2½-4 time, a 3½-4 time and a 4½-4
time. Such facts must evidently give quite a shock to professional
musicians, but it does not hurt the Celtic ears; and, after all, the
effect must not be so bad, since one of the best modern French
composers, M. Florent Schmid, had in recent years the boldness to make
use of 2½-4 and 3½-4 time measures. Lastly, we must notice that the
greatest freedom exists for the use of the measures in a tune, and
that the time may change according to the will of the composer. I know
even of the existence of a Breton tune which, unfortunately, I failed
to get, where the time changes at each bar.

The modes used by the Celts are still more curious than their rhythms.
The learned music, which we all know, uses only two modes, the major
and the minor, and very few people, except specialists, know that
there are many others in existence. A Breton musician, M. Duhamel, who
has deovted all his time and knowledge to the study of the modal
system of the Celts, told me that he had found in Celtic music twenty
different modes for the complete scales only, a figure which becomes
thirty if we count also the incomplete scales.

Personally, I know of thirteen modes in the diatonic system, the only
one with a few exeptions actually found in Brittany. The pentatonic
and hexatonic scales, so frequent in the music of the Gaels, have not
yet been discovered, as far as I am aware, in Breton tunes.

It was, I believe, a Breton musician, Bourgault Ducoudray, Professor
of the History of Music at the French Conservatoire, who was first to
notice in Celtic music the extensive use of modes similar to those
known to have existed in ancient Greece. This discovery was made
public in 1885, when Bourgault Ducoudray published the results in his
book *Trente Mélodies de Basse Bretagne*.

This work of the much-lamented master was carried on by his pupils,
and, in 1911, M. Maurice Duhamel was able to publish for the first
time a complete exposition of the Celtic modal system. This modal
system, which is only to be found in its completeness in the melodies
of Brittany, has at least fifteen modes, of which thirteen are
diatonic, whilst two make use of chromatic tones.

If we classify these modes according to their tonic, or key-note, in
taking as a type the scale of DOH, we shall see that:

* Three modes have DOH as a tonic,
* Two have RAY,
* Three have FAH,
* Three have SOH,
* Four have LAH,
* and none has either ME or TE.

Since the seventeenth century, learned musicians have been attracted
by the treasures which were to be found in folk music, and we owe to
them many collections of of songs. Unfortunately, their ignorance of
the existence of the old modes led them to correct these songs, and
rewrite them in modern scales, mostly minor. Such mutilation had the
effect of utterly destroying the character of the tune, and one can
judge of the havoc in Celtic music by looking at the collections
published in the eighteenth and nineteenth centuries. But these
mutilations were indeed as nothing compared with what the melodies
were going to suffer at the attempts of musicians attempting to
harmonize them. Look at the collections of Welsh and Breton songs
arranged for pianoforte, and published in the nineteenth century;
nothing poorer can be found, except perhaps similar collections of
Irish and Scottish songs. The best example I ever came across was that
of a Scottish reel in the hypophrygian mode, otherwise in the key of G
without accidentals, the accompaniment of which was written in the
major key of D!

Brittany did not escape the common fate. The first extensive
collection of Breton tunes was, I believe, published by Hersart
de la Villemarqué in 1839, at the end of the first edition of his
much-discussed *Barzaz Breiz*. I shall not speak here of the value of
the book. M. de la Villemarqué has been, a little unjustly I think,
called the Breton Macpherson, and the authenticity of many of his
songs is evidently doubtful; but if, in my opinion, most of the
melodies are genuine, some of them, at any rate, have been more or
less mutilated. In a subsequent edition, accompaniments were published
with the tunes, but fortunately have since been suppressed.

In 1885 appeared the collection published by Bourgault Ducoudray,
*Trente Mélodies de Basse Bretagne*, already mentioned above, which is
the first reliable document on Breton music, and the first publication
where we find accompaniments not only written by a good musician, but
by a musician who was thoroughly aware of the characteristics of our
national music, and had had the honour of discovering in it the old
modes of ancient Greece.

In 1889 was published *Chansons et Danses de Bretons*, by N. Quellien,
containing twenty-seven dancing tunes and a good number of other
melodies, a very sincere, but unfortunately very limited attempt.

It is with the awakening of Brittany, at the close of the nineteenth
century, that we begin to see people take interest in collecting folk
songs. Special mention should be made of the Breton reviews *Le
Clocher Breton* and *Dihunamb* for their good work in publishing folk
songs, and to the collections made by MM. Laterre, Gourvil, Abbé
Henry, Loeiz Herrieu, Maurice Duhamel.

It was only a few years before the war that the work was really
started on scientific lines. A group of Breton patriots, headed by M.
Vallée, began to go throroughly and methodically through the country,
collecting the folk songs on phonograph rolls from the lips of the
singers themselves, and formed a splendid collection which was
deposited at the University of Rennes.

Professional musicians also entered the field of research. Some of
them, such as Paul Le Flem and especially Paul Ladmirault, winner of
the first prize for harmony at the French "Conservatoire," began to
write learned music out of the material supplied by the Breton folk
songs. Others, like Maurice Duhamel, published a great number of
popular melodies and tried, in my opinion with great success, to
continue the work of Bourgault Ducoudray and to unravel the theory
and rules of Celtic music.[*]_

The foundation of the *Association des Compositeurs Bretons*, two
years before the war, was the natural consequence of this movement,
and the original compositions written by its members were found
good enough to be performed in the great concerts, and even, I
believe, on the operatic stage in Paris.

This shows us that if a study of the Celtic tunes is interesting and
fruitful because it enlightens what we already know of the old modes,
thanks to the ancient Greek writers, it must not be believed that our
national tunes are only important from an archaeological point of
view. The real rôle of Celtic music has not yet begun, and it depends
upon modern composers to make a proper use of it and acquire through
it an originality which could not be got anywhere else.

The question is whether our Celtic composers will follow the common
pathway and condemn themselves to write a music which, no doubt, will
be very fine, but more or less an imitation of German, French, or
English music; or, following the example given a few years ago by some
Russian musicians, if they will look for inspiration to their national
music, and, providing for it an adequate harmony, attain a real
originality.

What the Russians have done, the Celts can do by a serious study of
their folk lore and of the characteristics and atmosphere of the
Celtic countries. For it would be a great mistake to limit studies to
one country alone. It is by studying the various phases of the native
music of each of the Celtic countries that we may hope to acquire a
complete mastery of the musical system of the Celts. Such an idea has
already been expressed many years ago by the late Bourgault Ducoudray,
and it is a pleasure for me to see that his appeal has been heard by a
school of young Breton musicians. We shall now be able to see a
powerful, original, and really national music develop in our countries,
a music which will be able to describe the poetry of our landscapes,
and to express better than any other the feelings and the soul of our
people.

PAUL DIVERRES.

.. [*] See M. Duhamel, *Les quinze modes de la musique bretonne*
       (*Annales de Bretagne*, vol. XXVI., p. 687); *ibid*., *La
       Musique Celtique* (*Buhez Breiz*), vol. I., p. 28).

----

(Source: `The Celtic Song Book, p. 281 - 287
<https://archive.org/details/the-celtic-song-book/page/n143/mode/1up>`_)
