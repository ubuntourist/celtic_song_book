.. Page i (PDF Page 4, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#######################
The Song of the Heather
#######################

.. lilyinclude:: ./song_of_the_heather.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

.. note::

   This song has been translated by the leading Irish, Scotch Gaelic,
   Manx, Welsh, Breton, and Cornish poets into their native languages
   for singing in each Celtic country and at Pan-Celtic Festivals.

----

(Source: `The Celtic Song Book, p. i
<https://archive.org/details/the-celtic-song-book/page/n3/mode/1up>`_)
