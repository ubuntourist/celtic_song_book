
.. The Celtic Song Book master file, created by
   sphinx-quickstart on Sun Sep  5 10:08:02 2021.
   You can adapt this file completely to your liking, but
   it should at least contain the root `toctree` directive.

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. container:: center

   | The

.. rst-class:: center

################
CELTIC SONG BOOK
################

.. container:: center

   | *BEING*
   | *Representative Folk Songs of the Six Celtic Nations*

   | Arranged by
   | ALFRED PERCEVAL GRAVES
   | LITT.D., F.R.S.L.

   | Author of 'The Irish Song Book'
   | 'The Irish Fairy Book'
   | 'Irish Doric'
   | Etc.

   *To the many music-lovers who know only imperfectly how splendid
   is the Celtic heritage of song, this volume will come as a happy
   surprise; whilst all folk-song enthusiasts will be glad to possess
   so comprehensive and important a collection.*

   | 1928
   | ERNEST BENN LIMITED
   | Printed in Great Britain

Table of Contents
=================

.. toctree::
   :hidden:
   :maxdepth: 2

   Dedication                    <dedication>
   Song of the Heather           <song_of_the_heather>
   Preface to the Online Edition <online>
   Preface                       <preface>
   Introduction                  <introduction>
   Irish Section                 <irish/index>
   Scotch Section                <scotch/index>
   Manx Section                  <manx/index>
   Welsh Section                 <welsh/index>
   Cornish Section               <cornish/index>
   Breton Section                <breton/index>
   (back page)                   <breton/back_page>

* :doc:`Dedication                       <dedication>`

  - :doc:`Song of the Heather            <song_of_the_heather>`

* :doc:`Preface to the Online Edition    <online>`
* :doc:`Preface                          <preface>`
* :doc:`Introduction                     <introduction>`
* :doc:`Irish Section                    <irish/index>`

  - :doc:`Erin, the Tear and the Smile       <irish/erin_tear_smile>`
  - :doc:`Go where Glory waits Thee          <irish/go_where_glory_waits_thee>`
  - :doc:`Let Erin remember the Days of Old  <irish/let_erin_remember_days_old>`
  - :doc:`Silent, oh Moyle                   <irish/silent_oh_moyle>`
  - :doc:`Avenging and Bright                <irish/avenging_bright>`
  - :doc:`The Flight of the Earls            <irish/flight_earls>`
  - :doc:`Clare's Dragoons                   <irish/clares_dragoons>`
  - :doc:`At the Mid Hour of Night           <irish/mid_hour_night>`
  - :doc:`The Little Red Lark                <irish/little_red_lark>`
  - :doc:`I've found my Bonny Babe a Nest    <irish/bonny_babe_nest>`
  - :doc:`Shule Agra                         <irish/shule_agra>`
  - :doc:`Kitty of Colraine                  <irish/kitty_coleraine>`
  - :doc:`The Lark in Clear Air              <irish/lark_clear_air>`
  - :doc:`The Snowy-Breasted Pearl           <irish/snowy_breasted_pearl>`
  - :doc:`Down by the Sally Gardens          <irish/sally_gardens>`
  - :doc:`Pastheen Fionn                     <irish/pastheen_fionn>`
  - :doc:`The Welcome                        <irish/welcome>`
  - :doc:`The Heather Glen                   <irish/heather_glen>`
  - :doc:`No, not more Welcome               <irish/no_not_more_welcome>`
  - :doc:`Little Mary Cassidy                <irish/little_mary_cassidy>`
  - :doc:`The Winding Banks of Erne          <irish/winding_banks_erne>`
  - :doc:`The Red-Haired Man's Wife          <irish/red-haired_mans_wife>`
  - :doc:`Emer's Farewell                    <irish/emers_farewell>`
  - :doc:`Happy 'tis, thou Blind, for Thee   <irish/happy_blind_thee>`
  - :doc:`His Home and His Own Country       <irish/his_home_his_own_country>`
  - :doc:`Dublin Bay                         <irish/dublin_bay>`
  - :doc:`The Meeting of the Waters          <irish/meeting_waters>`
  - :doc:`The Song of the Woods              <irish/song_woods>`
  - :doc:`The Widow Malone                   <irish/widow_malone>`
  - :doc:`Father O'Flynn                     <irish/father_oflynn>`

* :doc:`Scotch Section                   <scotch/index>`

  - :doc:`The Bonnie Brier-Bush              <scotch/bonnie_brier_bush>`
  - :doc:`The Blue Bells of Scotland         <scotch/blue_bells_scotland>`
  - :doc:`There's nae Luck about the House   <scotch/nae_luck_about_house>`
  - :doc:`Afton Water                        <scotch/afton_water>`
  - :doc:`A Man's a Man for a' That          <scotch/mans_man_that>`
  - :doc:`Annie Laurie                       <scotch/annie_laurie>`
  - :doc:`Charlie is my Darling              <scotch/charlie_darling>`
  - :doc:`Scots, wha hae wi' Wallace bled!   <scotch/scots_wha_hae_wi_wallace_bled>`
  - :doc:`The Campbells are comin'           <scotch/campbells_are_comin>`
  - :doc:`Bonnie Dundee                      <scotch/bonnie_dundee>`
  - :doc:`Robin Adair                        <scotch/robin_adair>`
  - :doc:`Jock o' Hazeldean                  <scotch/jock_hazeldean>`
  - :doc:`The Hundred Pipers                 <scotch/hundred_pipers>`
  - :doc:`Leezie Lindsay                     <scotch/leezie_lindsay>`
  - :doc:`Ye Banks and Braes o' Bonnie Doon  <scotch/banks_braes_bonnie_doon>`
  - :doc:`The Auld Hoose                     <scotch/auld_hoose>`

* :doc:`Manx Section                     <manx/index>`
* :doc:`Welsh Section                    <welsh/index>`
* :doc:`Cornish Section                  <cornish/index>`
* :doc:`Breton Section                   <breton/index>`
* :doc:`(back page)                      <breton/back_page>`

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
