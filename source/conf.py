# -*- coding: utf-8 -*-
#
# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath("."))


# -- Project information -----------------------------------------------------

project = "The Celtic Song Book"
copyright = "1928, Alfred Perceval Graves"
author = "Alfred Perceval Graves"

# The short X.Y version
version = "0.1"
# The full version, including alpha/beta/rc tags
release = "0.1"


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named "sphinx.ext.*") or your custom
# ones.
extensions = [
    "sphinx.ext.todo",
    "sphinxnotes.lilypond",
    "sphinx.ext.coverage",
    "sphinx.ext.mathjax",
    "sphinx.ext.ifconfig",
    "cloud_sptheme.ext.table_styling",
    "sphinxext.opengraph",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = "alabaster"
# html_theme = "default"
# html_theme = "agogo"
# html_theme = "basic"
# html_theme = "bizstyle"
# html_theme = "classic"
# html_theme = "epub"
# html_theme = "haiku"
# html_theme = "nature"
# html_theme = "nonav"
# html_theme = "pyramid"
# html_theme = "scrolls"
# html_theme = "traditional"
# html_theme = "sphinxdoc"
html_theme   = "cloud"
# html_theme = "custom"   # See other KJC projects

# 2022.08.31 KJC - Add a favorite icon
#html_favicon = "_static/icon-lightning.svg"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

# 2021.09.06 KJC - Remove "0.1 documentation" from header
html_title = "The Celtic Song Book"

# 2021.09.12 KJC - Increase the resolution from 300 DPI
lilypond_png_resolution = 600

# 2022.04.20 KJC - Experimenting with output formats...
#lilypond_score_format = "svg"
lilypond_audio_format = "mp3"

# -- OpenGraph options -------------------------------------------------------

ogp_site_url = "https://ubuntourist.codeberg.page/Celtic_Song_Book/"
ogp_site_name = "The Celtic Song Book (Online Edition)"
ogp_use_first_image = True
ogp_image = "https://ubuntourist.codeberg.page/Celtic_Song_Book/_static/pan_celtic_flag.png"
ogp_image_alt = "Score for Clare's Dragoons on a Pan-Celtic flag"
ogp_description_length = 57
ogp_type = "book"
ogp_custom_meta_tags = [
    """<meta property="og:description"  content="BEING Representative Folk Songs of the Six Celtic Nations" />""",
    """<meta property="og:book:author"  content="Alfred Perceval Graves" />""",
    """<meta property="og:book:release_date" content="2021-10-01" />""",
    """<meta property="og:determiner"   content="the" />""",
    """<meta property="og:book:tag"     content="music" />""",
    """<meta property="og:book:tag"     content="sheet music" />""",
    """<meta property="og:book:tag"     content="celtic" />""",
    """<meta property="og:book:tag"     content="irish" />""",
    """<meta property="og:book:tag"     content="scottish" />""",
    """<meta property="og:book:tag"     content="welsh" />""",
    """<meta property="og:book:tag"     content="manx" />""",
    """<meta property="og:book:tag"     content="cornish" />""",
    """<meta property="og:book:tag"     content="breton" />""",
    """<meta property="og:book:tag"     content="folk song" />""",
    """<meta property="og:book:tag"     content="song" />""",
    """<meta property="og:image:width"  content="1200" />""", # FB: 1200 (Twitter Card: 1024)
    """<meta property="og:image:height" content="630"  />""", # FB:  630 (Twitter Card:  512)
    """<meta name="twitter:card"        content="summary_large_image" />""",
    """<meta name="twitter:site"        content="@codeberg_org" />""",
    """<meta name="twitter:creator"     content="@ubuntourist" />""",
    """<meta name="twitter:title"       content="The Celtic Song Book (Online Edition)" />""",
    """<meta name="twitter:description" content="BEING Representative Folk Songs of the Six Celtic Nations" />""",
    """<meta name="twitter:image"       content="https://ubuntourist.codeberg.page/Celtic_Song_Book/_static/pan_celtic_flag.png" />""",
    ]

rinoh_documents = [dict(doc="index",            # top-level file (index.rst)
                        target="publication")]  # output file (publication.pdf)
