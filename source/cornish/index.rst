.. Page 255 (PDF Page 131, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

#################
CORNISH FOLK SONG
#################

My friend, Dr. Henry Jenner, M.A., F.S.A., the leading authority on
all Cornish Antiquarian Subjects, kindly supplies these notes on the
Cornish Folk Songs in Baring-Gould's *Songs of the West*, of which
Messrs. Methuen have given the melodic use in this collection.

*Limadie*. |---| This used to be sung fifty or sixty years ago by the
late Samuel Gilbert, landlord of the Falcon Inn at St. Mawgan-in-Pydar.
Its origin is unknown. It was obtained from Mr.  William Gilbert, son
of S. Gilbert.

*The Hunting of Arscott of Tetcott*. |---| This is a cheery hunting
song, with a good tune and words of the usual sporting doggerel. It
describes a run from Pencarrow in Egloshayle to Penkenner in St.
Gennys, where they seem to have gone over the cliff, after which John
Arscott became a sort of ghostly Wild Huntsman, and may be seen on
full-moon nighs with his pack in full cry from Pencarrow to Dazard in
St. Gennys. This was a popular song at hunting dinners of old time in
East Cornwall and North-West Devon. The version in *Songs of the West*
is an eclectic one, touched up by Baring-Gould.

*Widdecombe Fair*. |---| Perhaps the most popular of Devon songs, and
also sung to the same words on a variant tune in Somerset, and there
noted by Cecil Sharp.

*The Helsten Furry Dance*. |---| This is a capital tune, with curious
but rather unintelligible words. It has been included, if only for the
tune of our one really Cornish folk dance. It is well known and very
popular, especially at folk-dancing festivals.

*The Dilly Song*. |---| This, of course, is found all over the world,
and forms of it are known in French, Breton, German, Flemish, Latin,
Hebrew, Modern Greek, and other languages. It is one of the common
"cumulative" songs. The tune given by Baring-Gould is not quite the
same as a Camborne traditional tune, but of the same character. It is
the song beginning:

| Come, and I will sing you.
|   What will you sing me?
| I will sing you One, oh!
|   What is your One, oh?
| One of them is all alone, and ever will remain so.

I have about fifteen versions of the words, some of them better than
Baring-Gould's. I heard a very good version of words and tune sung
recently by a St. Ives folk-song choir. It had been collected locally.
Though not, of course, distinctively Cornish, it has survived more in
Cornwall than anywhere else.

*The Streams of Nantsian*. |---| This appears to have local colour in
it. I cannot identify *Nantsian* for certain, but it is very likely to
be Lantine in Golant, near Fowey, which is called *Lancien* in Béroul's
poem *Tristran*, and appears as *Nauntyane* in 1346 in *Feudal Aids*.
*Lan* (=monastery) and *Nant* (later *Nans*) (=valley) are often
confused in Cornish place-names, and the form *Nauntyane* would later
become *Nansyan*. Indeed, it occurs as *Nauncyan* in a document of
1283-4 in the Assize Rolls. Professor Loth (*Contributions á l'Étude
des Romans de la Table Ronde*, pp. 72-75) has a good deal to say about
the place, which, and not Tintagel, he holds to have been King Mark's
castle. One verse of the song is:

| On a rocky cliff yonder
|   A castle up-stands:
| To the seamen a wonder
|   Above the black clouds.
| 'Tis of ivory builded,
|   With diamonds glazed bright,
| And with gold it is gi ded,
|   To shine in the night.

Was this King Mark's castle? I am certainly inclined to think that
*Nantsian* is *Lantine*. If so, it is a real local song. The words
begin:

| O the streams of Nantsian
|   In two parts divide,

which is exactly what happens opposite Lantine, for there the creek
that runs down from Lerryn joins the Fowey River. The words are not of
high quality, but the interest is in their local application. There is
no castle now at Lantine, though Castle Dore, a fine earth-work, is on
the hill above it, but might it not be that this translation of a lost
Cornish song, written when the form *Nansyan* was used? On the whole,
this seems the most definitely Cornish of all these songs. At Castle
Dore an inscribed stone was found, bearing the word *Drustagui
Cunomori Filius*. Is *Drustagnus* Tristan?

*The Keenly Lode*. |---| A comic mining song about a "dowser"
(divining-rod man) who finds a lode which looks "keenly" (*i.e.*,
promising |---| a common Cornish mining expression) and floats a
company to work it. They dig and find a buried horse, whose shoes were
metal which caused the hazel-rod to turn. The dowser, however, leaves
Cornwall with a fortune, while the shareholders drop their money. The
tune is not bad, and the local colour is amusing.

*The Marigold*. |---| This is a song about a sea-fight of a ship
called *The Marigold*, Captain Sir Thomas Merrifield, of Bristol,
against Turkish rovers. It was taken down by Davies Gilbert, the
Cornish historian, from an old man of eighty-six at St. Erth (the
parish in which I am now writing) in 1830. It is of not great interest
or local character. The tune is good and rather archaic. Baring-Gould
says it is in the Dorian mode (the mode of the First Tone). I should
have said it was only, as he gives it, in G minor, but that is as may
be.

*The Lover's Tasks*. |---| a local form, once popular in Cornwall, of
the common puzzle song. It begins:

| O buy me, my Lady, a cambri shirt,
|   Whilst every grove rings with a merry antine (anthem?),
| And stitch it without any needle work,
|   And thou shalt be a true lover of mine.

The second and fourth lines are the burdens of every verse. He
sets her, and she sets him, four apparently impossible and
contradictory tasks. The tune is simple and the words amusing.

----

(Source: `The Celtic Song Book, p. 255 - 257
<https://archive.org/details/the-celtic-song-book/page/n130/mode/1up>`_)
