% Bonnie Dundee
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.28 (kjc)
%
% Source: The Celtic Song Book, p. 105
%         https://archive.org/details/the-celtic-song-book/page/n55/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Bonnie Dundee"
  composer = \markup { \italic { "Poem by" } \smallCaps "Sir Walter Scott." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 6/8
  \tempo \markup { \italic "Fairly quick." } 4 = 100
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8
    <<
      { \voiceOne a'16^\mf bf16 }
      \new Voice
      { \voiceTwo a16[( bf16]) }
    >>
    \oneVoice                                    | %   0
    c8. d16 c8 c8 d8 c8                          | %   1
    f8 e8 d8 c4
    <<
      { \voiceOne c16 bf16 }
      \new Voice
      { \voiceTwo c16[( bf16]) }
    >>
    \oneVoice                                    | %   2

    \break

    a8 c8 c,8 a'8 c8 c,8                         | %   3
    g'8. g16 g8 g4
    <<
      { \voiceOne a16[( bf16]) }
      \new Voice
      { \voiceTwo a16 bf16 }
    >>
    \oneVoice                                    | %   4

    \break

    c8 d8 c8 c8 d8 c8                            | %   5
    f8 e8 d8 c4 c16 bf16                         | %   6

    \break

    a16 c8. c,8 g'16 c8. c,8                     | %   7
    f16 f8. f8 f4 c8^\f                          | %   8

    \break

    f8 f8 f8 f8[( bf8]) a8                       | %   9
    g8 c,8 c8 c4 c8                              | %  10

    \break

    g'8 g8 g8 g8 a8 bf8                          | %  11
    bf8 a8 a8 a4 f8                              | %  12

    \break

    a8. g16 a8 bf4 bf8                           | %  13
    c8 bf8 c8 d8\fermata r8 c16 bf16             | %  14

    \break

    a8 c8 c,8 g'16 c8. c,8                       | %  15
    f16 f8. f8 f4\fermata                          %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  To the                                         | %   0
  Lords of Con -- ven -- tion 'twas              | %   1
  Clav -- er -- house spoke; Ere the             | %   2
  King's crown go down there are                 | %   3
  crowns to be broke, Then _                     | %   4
  each cav -- a -- lier who loves                | %   5
  hon -- our and me, Let him                     | %   6
  fol -- low the bon -- nets of                  | %   7
  Bon -- nie Dun -- dee, Come                    | %   8
  fill up my cup, come                           | %   9
  fill up my can, Come                           | %  10
  sad -- dle my hor -- ses, and                  | %  11
  call out my  men; Un --                        | %  12
  hook the west port, and                        | %  13
  let us gae free, For it's                      | %  14
  up wi' the bon -- nets o'                      | %  15
  Bon -- nie Dun -- dee.                         | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { "Dundee he is mounted, he rides up the street," }
      \line { "The bells they ring backward, the drums they are beat," }
      \line { "But the provost (douce man) said, \"Just e'en let it be," }
      \line { "For the toun is weel rid o' that de'il o' Dundee.\"" }
      \line { \hspace #40 { "Come fill up, etc." } }

      \vspace #0.8

      \line { "There are hills beyond Pentland, and lands beyond Forth," }
      \line { "Be there lords in the south, there are chiefs in the north;" }
      \line { "There are brave Duinnewassels, thee thousand times three," }
      \line { "Will cry, \"Hey for the bonnets o' Bonnie Dundee.\"" }
      \line { \hspace #40 { "Come fill up, etc." } }

      \vspace #0.8

      \line { "Then awa' to the hills, to the lea, to the rocks," }
      \line { "Ere I own a usurper, I'll crouch with the fox;" }
      \line { "And tremble, false whigs, in the midst o' your glee," }
      \line { "Ye hae no seen the last o' my bonnets and me." }
      \line { \hspace #40 { "Come fill up, etc." } }
    }
  }
}
