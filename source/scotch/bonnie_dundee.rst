.. Page 105 (PDF Page 56, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#############
Bonnie Dundee
#############

.. lilyinclude:: ./bonnie_dundee.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 105
<https://archive.org/details/the-celtic-song-book/page/n55/mode/1up>`_)
