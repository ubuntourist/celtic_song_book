.. Page 101 (PDF Page 54, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#####################
Charlie is my Darling
#####################

.. lilyinclude:: ./charlie_darling.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 101
<https://archive.org/details/the-celtic-song-book/page/n53/mode/1up>`_)
