.. Page 93 (PDF Page 50, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#####################
The Bonnie Brier-Bush
#####################

.. lilyinclude:: ./bonnie_brier_bush.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 93
<https://archive.org/details/the-celtic-song-book/page/n49/mode/1up>`_)
