.. Page 97 (PDF Page 52, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###########
Afton Water
###########

.. lilyinclude:: ./afton_water.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 97
<https://archive.org/details/the-celtic-song-book/page/n51/mode/1up>`_)
