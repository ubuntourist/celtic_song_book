.. Page 94 (PDF Page 51, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##########################
The Blue Bells of Scotland
##########################

.. lilyinclude:: ./blue_bells_scotland.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 94
<https://archive.org/details/the-celtic-song-book/page/n50/mode/1up>`_)
