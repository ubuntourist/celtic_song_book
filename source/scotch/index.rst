.. Page 79 (PDF Page 43, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

################
SCOTCH FOLK SONG
################

Scotland has not yet followed the example set in England, Ireland, and
Wales by establishing a Folk Song Society of its own. There are,
however, several folk-song collectors of great diligence and high
qualifications actively employed in garnering the precious remains of
their country's folk music and folk poetry. Of these the most
preeminent is Mrs. Kennedy Fraser, daughter of David Kennedy, the
famous Scottish singer from whom she received a fine musical
training. She had been a singer as well as a player of Scots songs
under his instruction when quite a child, but made no special
departure of her own as a folk-song collector and a converter of folk
into art songs until, to use her own words, "the Breton volume of
Ducoudray, with all its strangely beautiful airs on unfamiliar scales,
with its poetically and musically suggestive pianoforte accompaniments,
and its French singing translations by François Coppée," opened to her
mind a vista of the possibilities of a new song development in the
direction of a national Celtic art song.

This art song, she felt, "should incorporate faithfully within itself
the Scotto-Celtic musical heritage, while at the same time itself
growing organically out of the miniature form within it, which it thus
enlarged and enframed." The difficulty was to find a place and
opportunity for original research work, such as that which made
possible the achievement of Ducoudray. But in the year 1903 she was
introduced by John Duncan |---| a Scottish painter, who had come back
from his post as a lecturer on Celtic art in Chicago University to
study Celtic conditions at their source |---| "to the little obscure
isle of Eriskay in the Outer Hebrides." To use Mrs. Kennedy Fraser's
own words: "It is one of a long chain of isles known collectively as
the Long Island and to reach it I had to go by open fishing-boat from
the neighbouring Isle of Uist. Our fishing-boat landed me at the first
convenient point, and when I clambered up to the pure, white
shellfish-clad rock, I seemed on virgin soil. No trace of path or
print of foot was to be seen. But crossing the dame grass in the
gathering darkness we at last struck a narrow footpath, the only road
in the island, and that but recently made. Here and there, as though
dropped at random on the hillside, were long, oval-shaped cottages,
built of undressed stone |---| walls three or four feet in height, and
five to eight feet in thickness |---| the rafters of driftwood from
across the Atlantic, gathered on its western shores, resting midway on
the walls, leaving a projection two or three feet |---| a device that
protects the thatch of the roof from the tornadoes of wind which are
common on those wind-swept, treeless isles. The next morning the life
of the isle unfolded itself |---| no roads, no fences, no carts, no
wheelbarrows; burdens of all kinds were carried in creels on the backs
of the folk or in panniers on the flanks of the ponies. And of course,
in an unfenced world where everybody's cow was always getting into
everybody else's corn!"

"The brown-sailed herring-boats left every Monday morning for the
fishing and returned every Saturday; and on Sundays the whole island
turned out |---| brawny men and beautiful maidens, old wives and
bare-footed bairns |---| as their priest himself tolled the bell that
called them all to worship."

"Here at the 'ceilidhs,' round the peat fires that burned in the
middle of the floor, I shared the life of our forebears, and here
found that song and tale were the necessities, not the luxuries, of
life:

| "I saw a stranger yestereen,
| I put meat in the eating-place,
| Drink in the drinking-place,
| And music in the listening-place."

"So runs an ancient rune recovered from the Gaelic by my island
poet-collaborator, Rev. Kenneth MacLeod. He, since we joined forces,
has not merely edited the Gaelic, but out of his own life-long
'collecting' has given me freely of valuable Hebridean lore, which
but for him had been irretrievably lost."

"Now, I had gone out to the isles with serious musical intent. I am
not a mere folk song enthusiast |...| |nbsp| I brought to my research
work a specially cultivated faculty |---| the faculty of recognizing
bare melody, and, further, that of mere 'motives,' musical figures out
of which ever fresh melodies may evolve. I found remarkable airs of a
type not noted by the amateur or semi-professional collectors, who may
have worked in these regions in a desultory fashion before me. I had
been thoroughly persuaded in my own mind before going out to the isles
that there were such airs, and I was not disappointed. Indeed, the
longer I work the more inclined I am to do homage to the musical
geniuses whose work has come down to us orally through the ages more
or less intact."

"Those who know our published collections will note that we do not
there call them folk song, whatever that term may be assumed to
imply. The folk (the isles-folk) have certainly in this case preserved
for us our own musical heritage; but who the original melodists were
we shall probably never know. Be that as it may, the melodies
themselves are not merely quaint racial survivals of value due mainly
to Celtic enthusiasts: they are a valuable addition to the melodic
wealth of the world."

"When I returned from my first 'raid' on the isles my *trouvailles*
were regarded with incredulity by many in Edinburgh. 'How much of this
is genuine?' I was asked. So, that there might be no Ossian-like
controversy as to the authenticity and faithfulness of this work, I
took with me a small recording phonograph, and with its help was able
to secure most valuable variants of the melodies |---| such as are in
evidence in the different verses of 'Kishmul's Galley,' for instance."

"This research stuff derives, I believe, from many ages and many
races.  Celtic lore would seem to have much in common with Hindu
mythology and with Greek thought, and recurrent waves of Celtic
inundation swept into and over Europe. Was it from the East?"

"Partly Oriental in character, the Celts remain in the remotest isles
and peninsulas still faithful to old memories and the old tongue, and
if Hindu and Byzantine carving show affinity with that of Iona, there
are distinct traces also in the Celtic airs of kinship with Persian,
Greek, and Arabian."

"In setting the airs we have in no sense altered the melodies; we have
merely tried to set them in an harmonic and rhythmic framework of
pianoforte wrought metal, so to speak, as one would set a beautiful
stone, a 'cairngorm,' or the like, and have tried by such setting to
show the tune more clearly |---| have tried to bring out its peculiar
character."

"'In the chorus of humanity,' says Renan, 'no race equals the Celts in
penetrative notes that go to the heart.' A passionate pungency, a
rhythmical *élan* these penetrative notes have. And these floating
fragments of the ancient music lore of Gaeldom we bring not merely as
something quaint, archaic, having peculiar and perhaps fascinating
local colour and character, but as racial records that yet strike to
the roots of all life wherever and whenever found |---| an elemental,
basic, far-reaching expression of life."

The above passsages form the greater part of the address delivered by
Mrs. Kennedy Fraser at the Celtic Congress held in the Isle of Man in
July, 1924.

It was also illustrated by Miss Margret Kennedy's singing of "The Skye
Fisher's Love Song," "A Churning Lilt," "The Benbeccula Bridal,"
"Cuchullan's Lament," and "Kishmul's Galley." Mrs. Kennedy Fraser also
sang "The Fate Croon" and "A Soothing Croon from Eigg."

But while Mrs. Kennedy Fraser is the leading collector of, and the
leading artistic authority upon, the song of the Hebrides, she is
also, as her father's daughter should be, the best worth listening to
of all the living interpreters of Scotch song, whether Highland,
Lowland, or Hebridean, and I am glad to be permitted to print her
views in full, for they apply equally, though of course with allowance
for racial differences in treatment, to the singing of the national
music of the other Celtic nationalities.

"Lowland Scots song, although tonally probably a branchm an offshoot
of the Scotto-Celtic music lore of the Scots Highlands and Islands,
has yet, in feeling, much that is akin to the Saxons |...| |nbsp|
No that *parfervidum Scotorum ingenium* is wanting in Lowland song,
but just that there is a fiercer blast of Scots passion in the
Highland than in the Lowland lore."

"As to the interpretation of Scots song, no generalization will
suffice; there are so many types: the songs of reminiscences, the
dramatic narrative ballads, the love-songs, the lullabies and other
songs of occupation, and the laments."

"But for *all* song I would point out, that being one of the smaller
forms of musical and literary crystallization of thoughts and feeling,
*and these in sequence*, it calls for a very delicate judgment |---|
how best, in such short space, to give full expression to the varying
emotions without injury to the design |---| the everlasting problem in
art, and that (song being different merely in degree and not in kind
from other musical forms) *naïveté* is no more essential to traditional
song than it is to symphony, which is only an aggrandisement of song."

"I labour the point, because if you approach Scots song, Lowland,
Highland, or Hebridean, with the faintest idea that the performance of
it must be a pose, such as that of the Watteau Shepherdess period of
French life in the eighteenth century, you will miss its meaning and
scope."

"Scots song, in short, is art expressing itself i word and tune, in
short forms. It is founded, as all vital art must be, on the
manifestations of the human heart and mind. It must be psychologically
true |---| we recognize ourselves in it. It must be beautiful in
texture (tone) and convincing in design (form)."

"The singing of traditional song such as Scotland has produced is one
of the most crucial tests of the singer's art. Operatic work may cover
crudities in composition; and, in what is popularly termed 'art-song,'
so much of the interpretation is achieved by the composer's
instrumental commentary, that the singer's task |---| if he (or she)
be artist-musician enough to sing *mentally* through the accompaniment
and have an imagination that is stirred by such musical tone painting
|---| is comparatively easy. The singer of the traditional *strophic*
song, on the other hand, must by his own art, and aided only by his
own creative imagination, supply all the subtle deviations from the
normal that give a continuous, convincing, psychological sequence to
the developments of the lyrical mood or of the dramatic situation.
Such was the art of my father, David Kennedy, to which I was brought
up from childhood, and I have never ceased to wonder since at the want
of it in singers. I took it for granted! And yet it is asking a great
deal of singers who essay Scots songs to reach this ideal."

"On the interpretive side the ancient song and ballad indeed
presuppose a traditional culture, a culture which, as Yeats has
pointed out in his essay on popular poetry, cannot be taken for
granted these days, and much study and imagination. therefore, may
have to be brought to bear on the subject before it will yield its
full message. Such is one of the peculiar difficulties on the
interpretive side. On the technical side there is much to accomplish,
for it is a great mistake to imagine that simple Scots songs are
simple in performance. The voice must be cultured and controlled.  But
after studies in voice-production have been made, we are only at the
beginning of things. Although there is a fine cantabile type in Scots
song, only a few of the best of our songs can be regarded as mere
opportunities for vocal display. Indeed, in some of the character
songs and lilts (in which we are very rich) you must put your voice
in your pocket, so to speak, and bring out only so much as is required
at the moment to supply the necessary lilt and colour."

"On the purely musical side it cannot be too much insisted upon with
young singers |---| and some not so very young either! |---| that
accent and shape, beauty of form, intelligibility of phrase, and the
hypnotism of rhythm |---| which plays such an all-important part in
all art |---| can be attained only by carefully worked out
*gradations*; and that such gradations can be achieved only by fierce
economy, by cutting away as well as by adding on, by lessening the
tone-quality in one place that it may stand out in relief in another.
If, indeed, you begin a tone-curving phrase with one shadow of a shade
too much tone, you may from the first have made your intended
crescendo curve impossible. And if |---| after a point, an accent
arrived at, worked up to |---| you lean with the faintest too much
stress or too long duration on a weak *following* beat |---| a common
rhythmical feature in Scots music |---| you have wiped out again your
climax, your point, you have destroyed your lilt, blurred your melodic
shape."

"Hence one occasionally finds an *un*\ consious singer |---| with a
good voice naturally free from the faults of production |---| with
mind not concerned overmuch with voice or tone, nor hampered with a
stiffly pictured notation, giving a much better lilting rhythm than a
half-trained singer who, thinking too exclusively of tone, gives it
out in full measure, note after note, until one entirely loses the
shape, and 'cannot see the wood for the trees'."

Next to Mrs. Kennedy Fraser's four remarkable collections of over four
hundred Hebridean folk music comes Miss Frances Tolmie's collection of
105 songs of occupation from the western isles of Scotland. "This
collection," as Miss Lucy Broadwood justly writes of it, in the third
part of volume four of the *Journal* of The Folksong Society, "opens a
mine of interest and delight to musicians, poets, folklorists, and
historians, and undoubtedly forms one of the most important
contributions yet made towards the preservation of the purely
traditional music and poetry of our British Isles in general and of
Scotland in particular."

"Songs of occupation are among the most primal things in the history
of mankind, and in their simple rhythms and intervals, first evolved
by workers for their needs, we find the germs of all music and verse.
The songs in this *Journal*, which represent but a small section from
Miss Tolmie's mass of memories and lore, have not only been skillfully
taken down, translated, and annotated by a Hebridean familiar with
Highland song from earliest infancy, but have received the value of a
commentary by another Highlander, Dr. George Henderson, Lecturer on
Celtic Languages and Literature in the University of Glasgow, and well
known as an authority and writer on Celtic lore and literature."

"In addition, Miss A. G. Gilchrist, a constant student of comparative
folk song, has carefully analyzed each air here printed, and has
contributed to the *Journal* a very illuminating and suggestive essay
upon the gapped scale system, to which these pure Gaelic tunes
conform."

"It should be borne in mind that songs of occupation as a whole belong
to the *luinneag* class, which is distinct from the *laoidh* (lay),
hymn or Ossianic lay, and the *oran mor* (great song). To the latter
classes belong the great elegies and laments, songs of praise,
rhapsodies, descriptive of the beauties of nature and the like, in
which, to suit the words, the music flows in broad and majestic
streams. Patrick M'Donald writes" 'Over all the Highlands ther are
various songs, which are sung to airs suited to the nature of the
subject.  But on the western coast, benorth Middle Lorne, and in all
the Hebrides, *luinigs* are most in request. These are in general very
short, and of a plaintive cast, analogous to their best poetry; and
the are sung by the the women, not only at their diversions, but also
during almost every kind of work, where more than one person is
employed, as milking the cous and watching the folds, fulling of
cloth, grinding of grain with the quern or handmill, haymaking, and
cutting down corn. The men too, have *iorrums*, or songs for rowing,
to which they keep time with their oars, as the women likewise do in
their operations whenever the work admits of it. When the same airs
are sung in their hours of relaxation, the time is marked by the
motions of a napkin, which all the performers lay hold of. In singing,
one person leads the band; but in a certain part of the tune he stops
to take a breath, while the rest strike in and complete the air,
pronouncing it to a chorus of words and syllables generally of no
signification.'"

The late Miss Tolmie, who was descended through both parents from
Hebrides families of great distinction, thus describes the occasion of
her forming her great Scots folk-song collection:

"In the year 1900, when spending a day at Dr. Alexander Carmichael's
house at Taynuilt, near Loch Awe, I met Dr. George Henderson. In the
course of conversation, and after I had been singing some 'Puirt-a
Beul' (mouth-tunes sung for dancing) both friends expressed a wish
that I would write down all the tunes I remembered. This I promised to
do, on condition that they would get the gaps in my verses filled up."
This led to the getting together of Miss Tolmie's folk-song collection
and its publication by the Folk Song Society.

Miss Tolmie gives this graphic description of the waulking party:

"A waulking, while a useful and necessary domestic function, was also
regarded as a pleasant form of entertainment. Invitations were issued,
and the obliging guests came dressed neatly and specially for the
occasion, with bare arms and stout aprons. They took their places
|---| six to ten persons on each side, leaving elbow-room |---| at the
waulking table. This was a long board, about three feet in width,
grooved lengthways and resting on trestles. The cloth to be fulled or
thickened was slowly dealt out from a vat at one end of the board.
This vat, which contained a special liquid, was presided over by the
good-wife of the house, or some other person with experience. The wet
mass of cloth was firmly grasped by one of the waulkers and pushed
towards the person opposite, who with a similar movement returned it
to be sent to the next opposing pair. This process continued til the
cloth had gone the round of the board three or four times. When the
moisture in it had been duly absorbed, the cloth was plunged again
into the vat 'to get a drink' and go the round of the board again
until pronounced thick enough. Singing accompanied the process
throughout, songs of slow and solemn character coming first, followed
by those in quicker time and merrier. Towards the close a slow measure
was again used. The new web received its final treatment to the
accompaniment of a solemn strain of song. During the singing of it the
cloth was slowly and carefully wound round a board used to press it
and give it a finish. According to Dr. Alexander Carmichael, this
final process concludes still in some places with devout magical
movements and words of benediction on the future wearer. For example,
on seeing a young man receive a new suit, it was proper to salute him
thus: 'Gum meal thu e; gun caith thu e s'gum faigh thu bean r'a linn!'
('Mayst thou enjoy it; mayst thou wear it, and find a wife the
while!')."

"The waulking song differed from the 'duanag,' or ditty, in that its
solo verse-part, consisting usually but of one line, though sometimes
of two, was often (but not invariably) followed by a little refrain in
meaningless syllables, and was succeeded by the chorus, in which all
present, both workers and audience, joined. I once met a woman in
North Uist who told me that the doctor advised her to frequent
waulkings as the best remedy against mental depression, from which she
suffered."

"There seems to have been no fixed rule as to the point at which the
waulking songs began, whether with the solo or the chorus. A
continuous round was kept up of the three parts |---| solo verse, solo
refrain, and chorus |---| with no very marked ending."

"The reaping and rowing songs, proceeded in the same manner, were
often heard at waulkings, and were usually included in the singer's
traditional repertory. As to other music, there wass no dancing at
Killmaluag in the youth of Mary Ross, my informant, but the ordinary
expression of gladness and sympathy when a wedding occurred could not
be suppressed, and neighbours met at the house of the bride's family
and sang joyous songs. The company when singing sat in a circle, each
member of which was linked to the next by means of a handkerchief held
at the ends between them. The rhythm of the song wass vigorously marked
by waving up and down the handkerchiefs in unison."

Miss Anne G. Gilchrist adds an interesting note on the modal system of
Gaelic tunes, from which we quote: "A distinct line of demarcation may
be observed between the music of the Highlands and Lowlands of
Scotland, coinciding with the frontier lines of language and
nationality. The folk songs of the two races differ rhythmically as
the construction and poetical system of the two languages differ |---|
emotionally as the characters of the Gael and Lowlander differ |---|
and finally differ in scale; for Gaelic vocal music clings more or
less to its ancient gapped scale, and retains a characteristic
avoidance of certain notes, whereas Lowland Scottish music now
approximates in its seven-note modal construction to the folk music
of England. Lowland music has, however, been greatly enriched by
borrowings from Gaelic sources. It has hitherto been generally assumed
that the two-gapped |---| *i.e.*, five-note Scottish scale, known as
the Scottish pentatonic scale, is equivalent to our scale of C major,
with the fourth and seventh degrees absent; but a careful examination
of the tunes in Miss Tolmie's happily genuine and undoctored collection
has led me to the conclusion that this primitive pentatonic scale was
rather equivalent to the scale of C to C with the third and seventh
degree omitted (possibly built upon the three-fifths, C to G, D to A,
F to C). This is the scale upon which many Highland *iorrums* (of
which 'The Skye Boatsong' is a good and well-known example) and other
songs of labour are constructed, without the upper C to form the
complete pentatonic scale. Probably in such songs of labour we get
very near the beginnings of a nation's folk music."

While dealing with Gaelic song, mention must be made of Lachlan
Macbean's collection of "The Songs and Hymns of the Gael," with
translations and music, and an introduction by the author published
by Eneas Mackay of 43, Murray Place, Stirling, in 1900. This is an
earlier collection than Miss Tolmie's and contains twenty-five
Highland melodies printed for the first time, and the first collection
of Highland sacred music. Its secular portion is divided into Love
Songs, Songs of Home, Patriotic Songs, Songs of Grief, Humorous Songs,
Ossianic Lays, Songs og Scenery and Miscellaneous Songs, all in Gaelic
with good English verse translations by Mr. Macbean, which closely
follow the metres and rhymes of the original.

The sacred songs published by Mr. Macbean had never before been
printed, and of these he gives us thirty-six, their subjects being God
the Father, Christ in His Life and Suffering and our connection with
Him, Faith, the Christian Life, Youth, Death, Judgment, Heaven and
such national hymns as Grant's "Cry of the Gael," and Macfarland's
"Supplication." Into the third section of his book, that of Gaelic
Psalmody, Mr. Macbean has carried French, English, Welsh, and other
psalm tunes, to which the Gaelic words that he gives us are sung.

In conclusion, Miss Lucy Broadwood may thus be well quoted: "The
pathetic and wild beauty of Gaelic songs can only be realized by those
who have heard them sung. Highlanders are very commonly gifted with
fine voices, of a rich resonance, similar to that found amongst
Italians. In Gaelic the vowels are open and pure, often succeeding and
melting into each other as they do in Italian. Thus very strong *messa
de voce* vowel effects are produced, which heighten emotional
expression in an extraordinary manner. The soft gutturals are more
like those found in Dutch than in German, and trilled 'r's' are very
vocal. Gaelic words have the strong accent on the first syllable, and
in many words the last syllable is as elusive as the French mute 'e'
when properly sung. This strong accent and weak ending in Gaelic words
were observed and entirely misunderstood by Lowland Scottish and
English musicians of the late seventeenth century onwards, and it is
these manufacturers of Scottish music who are responsible for the
invention of the odious 'snap' which arouses the indignation of the
true Scot if he have anything of music in him."

"The Highlander makes marked use of the *crescendo* and the
*diminuendo* phrasing, more especially when moved, with very great
breadth and power."

"He accents vigorously, and makes much use of the *sforando* on
vowels; generally he ues grace-notes, varying them on every verse.
Some singers, chiefly the older people, orament as over-profusely as
do many of the Irish, but as a rule Gaels adorn their airs sparingly
and with musical good taste. They have a peculiar and characteristic
way of carrying on one musical interval to the next by means of a
rapid repetition and slide of the first note, producing a beautiful
and soft kind of *appoggiaturna* or *portamento* (not a 'scoop'), such
as is heard in plainsong. Very commonly, too, the Gaelic singer beats
time with his foot or feet and hands together."

----

.. toctree::

   The Bonnie Brier-Bush              <bonnie_brier_bush>
   The Blue Bells of Scotland         <blue_bells_scotland>
   There's nae Luck about the House   <nae_luck_about_house>
   Afton Water                        <afton_water>
   A Man's a Man for a' That          <mans_man_that>
   Annie Laurie                       <annie_laurie>
   Charlie is my Darling              <charlie_darling>
   Scots, wha hae wi' Wallace bled!   <scots_wha_hae_wi_wallace_bled>
   The Campbells are comin'           <campbells_are_comin>
   Bonnie Dundee                      <bonnie_dundee>
   Robin Adair                        <robin_adair>
   Jock o' Hazeldean                  <jock_hazeldean>
   The Hundred Pipers                 <hundred_pipers>
   Leezie Lindsay                     <leezie_lindsay>
   Ye Banks and Braes o' Bonnie Doon  <banks_braes_bonnie_doon>
   The Auld Hoose                     <auld_hoose.rst>

----

(Source: `The Celtic Song Book, p. 79 - 91
<https://archive.org/details/the-celtic-song-book/page/n42/mode/1up>`_)
