.. Page 109 (PDF Page 58, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##################
The Hundred Pipers
##################

.. lilyinclude:: ./hundred_pipers.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

| Oh! our sodger lads look'd braw, look'd braw,
| Wi' their tartans, kilts an' a', an' a',
| Wi' their bonnets, an' feathers, an' glitt'ring gear,
| An' pilbrochs sounding sweet an' clear.
| Will they a' return to their ain dear glen?
| Will they a' return⸺our Hieland men?
| Second-sighted Sandy look'd fu' wae,
| And mothers grat when they march'd awa'.
| Wi' a hundred pipers an' a', an' a',
| Wi' a hundred pipers an' a', an a';
| But they'll up an' gie 'em a blaw, a blaw,
| Wi' a hundred pipers an' a', an' a'.

| Oh! wa is foremaist o' a', o' a'?
| Oh wha does follow the blaw, the blaw?
| Bonnie Charlie, the king o' us a', hurra!
| Wi' his hundred pipers an' a', an' a'!
| His bonnet an' feathers he's waving high!
| His prancing steed maist seems to fly!
| The nor' wind plays with his curly hair,
| While the pipers blaw in an unco flare!
| Wi' a hundred pipers an' a', an' a',
| Wi' a hundred pipers an' a', an' a',
| We'll up an' gie 'em a blaw, a blaw,
| Wi' a hundred pipers an' a', an' a'.

| The Esk was swollen, sae red, sae deep;
| But shouther to shouther the brave lads keep;
| Twa thousand swam ower to fell English ground,
| An' danc'd themselves dry to the pilbroch's sound.
| Dumfounder'd, the English saw, they saw!
| Dumfounder'd, they heard the blaw, the blaw!
| Dumfounder'd, they a' ran awa', awa'
| Frae the hundred pipers an' a', an' a'!
| Wi' a hundred pipers an' a', an' a',
| Wi' a hundred pipers an' a', an' a',
| We'll up an' gie 'em a blaw, a blaw,
| Wi' a hundred pipers an' a', an' a'.

----

(Source: `The Celtic Song Book, p. 109
<https://archive.org/details/the-celtic-song-book/page/n57/mode/1up>`_)
