% Leezie Lindsay
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.30 (kjc)
%
% Source: The Celtic Song Book, p. 111
%         https://archive.org/details/the-celtic-song-book/page/n58/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Leezie Lindsay"
  composer = \markup { \smallCaps "Anonymous." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 3/4
  \tempo \markup { \italic "Fairly slow." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

% Bb

melody = {
  \relative c' {
    \global
    \partial 4
    <<
      { \voiceOne c8^\mf d8 }
      \new Voice
      { \voiceTwo c8[( d8]) }
    >>
    \oneVoice                                    | %   0
    f4. g8 a4                                    | %   1
    a4 f'4
    <<
      { \voiceOne c8 a8 }
      \new Voice
      { \voiceTwo c8[( a8]) }
    >>
    \oneVoice                                    | %   2
    g4 f4
    <<
      { \voiceOne c8 d8 }
      \new Voice
      { \voiceTwo c8[( d8]) }
    >>
    \oneVoice                                    | %   3

    \break

    f4. g8 a4                                    | %   4
    c4 a4 c4                                     | %   5
    d2
    <<
      { \voiceOne e8. f16 }
      \new Voice
      { \voiceTwo e8.[( f16]) }
    >>
    \oneVoice                                    | %   6
    f,4. g8 a4                                   | %   7

    \break

    c4 a4
    <<
      { \voiceOne g8. f16 }
      \new Voice
      { \voiceTwo g8.[( f16]) }
    >>
    \oneVoice                                    | %   8
    g4 a4^\< c4                                  | %   9
    d4\! f4. d8                                  | %  10
    c4( a4)^\> g8.[( f16\!])                     | %  11
    f2                                             %  12

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Will ye                                        | %   0
  gang to the                                    | %   1
  Hie -- lans, Lee -- zie                        | %   2
  Lind -- say? Wid ye                            | %   3
  gang to the                                    | %   4
  Hie -- lans wi'                                | %   5
  me? Will ye                                    | %   6
  gang to the                                    | %   7
  Hie -- lans, Lee -- zie                        | %   8
  Lind -- say, My                                | %   9
  pride and my                                   | %  10
  dar-ling to                                    | %  11
  be?                                              %  12
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺
\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { "To gang to the Hielans wi' you, sir," }
      \line { \hspace #2 { "I dinna ken how that may be," } }
      \line { "For I ken na' the lan' that ye live in," }
      \line { \hspace #2 { "Nor ken I the lad I'm gaun wi'." } }

      \vspace #0.8

      \line { "O Leezie lass, ye maun ken little" }
      \line { \hspace #2 { "If sae be that ye dinna ken me," } }
      \line { "My name is Lord Ronald MacDonald," }
      \line { \hspace #2 { "A chieftain o' high degree." } }

      \vspace #0.8

      \line { "She has kilted her coats o' green satin," }
      \line { \hspace #2 { "She has kilted them up to the knee," } }
      \line { "And she's aff wi' Lord Ronald MacDonald," }
      \line { \hspace #2 { "His bride an' his darlin' to be." } }
    }
  }
}
