% Scots, wha hae wi' Wallace bled!
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.27 (kjc)
%
% Source: The Celtic Song Book, p. 103
%         https://archive.org/details/the-celtic-song-book/page/n54/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Scots, wha hae wi' Wallace bled!"
  composer = \markup { \italic { "Poem by" } \smallCaps "Burns." }
  tagline = ""
% subtitle = "subtitle"
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key a \major
  \time 2/4
  \tempo \markup { \italic "In march time." } 4 = 120
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    e'8.^\f e16 e8. cs16                                   | %   1
    e8. fs16 a4                                            | %   2
    fs8. fs16 fs8. e16                                     | %   3
    fs8. gs16 a4                                           | %   4

    \break

    cs8. cs16 b8. a16                                      | %   5
    a8. b16 cs4                                            | %   6
    a8. fs16 fs8. e16                                      | %   7
    e2                                                     | %   8

    \break

    cs'8. cs16 cs8. b16                                     | %   9
    cs8. d16 e4                                            | %  10
    b8. b16 b8. a16                                        | %  11
    b8. cs16 d4                                            | %  12

    \break

    e8. cs16 b8. a16                                       | %  13
    a8. b16 cs4                                            | %  14
    a8. fs16 fs8. e16                                      | %  15
    e2\fermata                                             | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Scots, wha hae wi'                           | %   1
  Wal -- lace bled,                            | %   2
  Scots, wham Bruce has                        | %   3
  of -- ten led,                               | %   4
  Wel -- come to your                          | %   5
  go -- ry bed,                                | %   6
  Or to vic -- to --                           | %   7
  rie!                                         | %   8
  Now's the day an'                            | %   9
  now's the hour,                              | %  10
  See the front of                             | %  11
  bat -- tle lour;                             | %  12
  See ap -- proach proud                       | %  13
  Ed -- ward's pow'r,                          | %  14
  Chain and sla -- ve                          | %  15
  rie!                                         | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { Wha would be a traitor knave?  }
      \line { Wha would fill a coward's grave? }
      \line { Wha sae base as be a slave? }
      \line { \hspace #4 { Let him turn an' flee! } }
      \line { Wha, for Scotland's king an' law, }
      \line { Freedom's sword would strongly draw, }
      \line { Freeman stand, and freeman fa', }
      \line { \hspace #4 { Let him on wi' me! } }

      \vspace #0.8

      \line { By oppression's woes an' pains, }
      \line { By your sons in servile chains, }
      \line { We will drain our dearest veins, }
      \line { \hspace #4 { But they shall be free, } }
      \line { Lay the proud usurpers low! }
      \line { Tyrants fall in ev'ry foe! }
      \line { Liberty's in ev'ry blow! }
      \line { \hspace #4 { Let us do or dee! } }
    }
  }
}
