% The Auld Hoose
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.04.11 (kjc)
%
% Source: The Celtic Song Book, p. 112
%         https://archive.org/details/the-celtic-song-book/page/n59/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Auld Hoose"
  composer = \markup { \italic "Poem by"  \smallCaps "Lady Nairne." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key g \major
  \time 4/4
  \tempo \markup { \italic "Fairly slow." } 4 = 75
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative c' {
    \global

    \partial 4
    <<
      { \voiceOne g'8^\p a8 }
      \new Voice
      { \voiceTwo g8[( a8]) }
    >>
    \oneVoice                                | %   0
    <<
      { \voiceOne b4 }
      \new Voice
      { \voiceTwo b8 b8 }
    >>
    \oneVoice
    a8 g8
    <<
      { \voiceOne a8[( g8]) }
      \new Voice
      { \voiceTwo a8 g8 }
    >>
    \oneVoice
    e8 g8                                    | %   1
    d8 e8 g8 b8 a4. g16[( a16])              | %   2

    \break

    <<
      { \voiceOne b4 }
      \new Voice
      { \voiceTwo b8 b8 }
    >>
    \oneVoice
    a8 g8 a8 g8 e8
    <<
      { \voiceOne g8 }
      \new Voice
      { \voiceTwo g16 g16 }
    >>
    \oneVoice                                | %   3
    <<
      { \voiceOne d8 b'8 }
      \new Voice
      { \voiceTwo d,8[( b'8]) }
    >>
    \oneVoice
    a8. g16 g4 r8
    <<
      { \voiceOne g8 }
      \new Voice
      { \voiceTwo g16 g16 }
    >>
    \oneVoice                                | %   4

    \break

    <<
      { \voiceOne a8 a8^\cresc }
      \new Voice
      { \voiceTwo a4 }
    >>
    \oneVoice
    b8 d8
    <<
      { \voiceOne e8. d16 }
      \new Voice
      { \voiceTwo e8.[( d16]) }
    >>
    \oneVoice
    d8 b8^\mf                                | %   5
    d8 b8 a8. g16 a4 r8 g16[( a16])          | %   6

    \break

    b8 b8 a8 g8
    <<
      { \voiceOne e8. d16 }
      \new Voice
      { \voiceTwo e8.[( d16]) }
    >>
    \oneVoice
    d8 e8                                    | %   7
    g8 b8 a8. g16 g4\fermata                   %   8

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh! the                                     | %   0
  auld _ hoose, the auld _ hoose, What        | %   1
  tho' the rooms were wee, Oh,                | %   2
  kind _ hearts were dwelling _ there, And _  | %   3
  bairnies _ fu' o' glee; The _               | %   4
  wild rose and the jes -- sa- -- mine Still  | %   5
  hang up -- on the wa', Hoo                  | %   6
  mo -- ny che -- rish'd me -- mo- -- ries Do | %   7
  they, sweet flow'rs, re -- ca'.               %   8
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺ 
\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { "Oh! the auld laird, the auld laird," }
      \line { \hspace #2 { "Sae canty, kind and crouse," } }
      \line { "Hoo mony did he welcome to" }
      \line { \hspace #2 { "His ain wee dear auld hoose" } }
      \line { "And the leddy too, sae genty" }
      \line { \hspace #2 { "There shelter'd Scotland's heir" } }
      \line { "And clipt a lock wit' her ain han'" }
      \line { \hspace #2 { "Fra his lang yellow hair." } }

      \vspace #0.8

      \line { "The mavis still doth sweetly sing," }
      \line { \hspace #2 { "The bluebells sweetly blaw," } }
      \line { "The bonnie Earn's clear winding still," }
      \line { \hspace #2 { "But the auld hoose is awa'." } }
      \line { "The alud hoose, the auld hoose," }
      \line { \hspace #2 { "Deserted tho' ye be." } }
      \line { "There ne'er can be a new hoose" }
      \line { \hspace #2 { "Will seem sae fair to me." } }

      \vspace #0.8

      \line { "Still flourishing the auld pear tree" }
      \line { \hspace #2 { "The bairnies liked to see," } }
      \line { "And oh! hoo aften did they speer" }
      \line { \hspace #2 { "When ripe they a' wad be." } }
      \line { "The voices sweet, the wee bit feet" }
      \line { \hspace #2 { "Aye rinnin' here and there," } }
      \line { "The merry shout⸺ oh, whiles we greet" }
      \line { \hspace #2 { "To think we'll hear nae mair." } }

      \vspace #0.8

      \line { "For they are a' wide scattered noo," }
      \line { \hspace #2 { "Some to the Indies gane," } }
      \line { "And ane alas! to her lang hame⸺ " }
      \line { \hspace #2 { "Not here we'll meet again." } }
      \line { "The kirkyard, the kirkyard," }
      \line { \hspace #2 { "Wi' flow'rs ev'ry hue," } }
      \line { "Is shelter'd by the holly's shade" }
      \line { \hspace #2 { "An' the dark sombre yew." } }

      \vspace #0.8

      \line { "The setting sun, the setting sun!" }
      \line { \hspace #2 { "Hoo glorius it gae down;" } }
      \line { "The cloudy splendour rais'd oor hearts" }
      \line { \hspace #2 { "To cloudless skies aboon." } }
      \line { "The auld dial, the auld dial," }
      \line { \hspace #2 { "It tauld hoo time did pass;" } }
      \line { "The wintry winds ha'e dang it down," }
      \line { \hspace #2 { "Noo hid 'mang weeds and grass." } }
    }
  }
}
