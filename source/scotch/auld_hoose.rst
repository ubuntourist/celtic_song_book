.. Page 113 (PDF Page 60, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##############
The Auld Hoose
##############

.. lilyinclude:: ./auld_hoose.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 113
<https://archive.org/details/the-celtic-song-book/page/n59/mode/1up>`_)
