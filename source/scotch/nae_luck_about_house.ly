% There's nae Luck about the House
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.18 (kjc)
%
% Source: The Celtic Song Book, p. 96
%         https://archive.org/details/the-celtic-song-book/page/n51/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "There's nae Luck about the House"
  composer = \markup { \italic { "Attributed to" } \smallCaps "Julius Mickle." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 2/4
  \tempo \markup { \italic "Quick." } 4 = 100
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 bf'8                                      | %   0
    ef8. c16 bf8 g8                                      | %   1
    af8. \afterGrace bf16 af16 \afterGrace c8 af8 d8     | %   2
    ef8. c16 bf8 g8                                      | %   3

    \break

    f4 r8 bf8                                            | %   4
    ef8. c16 bf8. g16                                    | %   5
    af8. \afterGrace bf16 af16 \afterGrace c8 af8 ef'8   | %   6
    bf8. af16 g8 f8                                      | %   7

    \break

    ef4. bf'8                                            | %   8
    ef8. c16 bf8. g16                                    | %   9
    af8. \afterGrace bf16 af16 \afterGrace c8 af8 d8     | %  10
    ef8 c8 bf8. g16                                      | %  11

    \break

    f4 r8 bf8                                            | %  12
    ef8. c16 bf8 g8                                      | %  13
    af8. bf16 c8.\fermata ef16                           | %  14
    bf8. af16 g8 f8                                      | %  15

    \break

    ef4. ef16 f16                                        | %  16
    g4 g8. ef16                                          | %  17
    af8. af16 af8 f8                                     | %  18
    g4 g8. ef16                                          | %  19

    \break

    f4 r8 bf8                                            | %  20
    g16 g8. g8. ef16                                     | %  21
    af8. bf16 c8.\fermata ef16                           | %  22
    bf8. af16 g8 f8                                      | %  23
    ef4.\fermata                                           %  24

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  And                                     | %   0
  are ye sure the                         | %   1
  news is true? And                       | %   2
  are ye sure he's                        | %   3
  weel? Is                                | %   4
  this a time to                          | %   5
  talk o' wark? Ye                        | %   6
  jades, fling by your                    | %   7
  wheel! Is                               | %   8
  this a time to                          | %   9
  think o' wark When                      | %  10
  Col -- in's at the                      | %  11
  door? Gie                               | %  12
  me my cloak, I'll                       | %  13
  to the quay, And                        | %  14
  see him come a --                       | %  15
  shore. For there's                      | %  16
  nae luck a --                           | %  17
  bout the house, There's                 | %  18
  nae luck at                             | %  19
  a', There's                             | %  20
  lit -- tle plea -- sure                 | %  21
  in the house, When                      | %  22
  our gude -- man's a --                  | %  23
  wa'.                                      %  24
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺  
\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { Rise up and mak' a clean fireside, }
        \line { \hspace #2 { Put on the muckle pot; } }
        \line { Gie little Kate her cotton gown, }
        \line { \hspace #2 { And Jock his Sunday coat; } }
        \line { And mak' their shoon as black as slaes, }
        \line { \hspace #2 { Their hose as white as snaw; } }
        \line { It's a' to please my ain gudeman, }
        \line { \hspace #2 { For he's been long awa'. } }
        \line { \hspace #4 { For there's nae luck, etc. } }
        \combine \null \vspace #0.8
        \line { Come gie me down my bigonet, }
        \line { \hspace #2 { My bishop-satin gown; } }
        \line { And rin and tell the Bailie's wife }
        \line { \hspace #2 { That Colin's come to town; } }
        \line { My Turkey-slippers maun gae on, }
        \line { \hspace #2 { My hose o' pearl blue; } }
        \line { It's a' to please my ain gudeman, }
        \line { \hspace #2 { For he's both leal and true. } }
        \line { \hspace #4 { For there's nae luck, etc. } }
        \combine \null \vspace #0.8
        \line { Sae true his heart, sae smooth his speech, }
        \line { \hspace #2 { His breath like caller air! } }
        \line { His very foot has music in't }
        \line { \hspace #2 { As he comes up the stair; } }
        \line { And will I see his face again? }
        \line { \hspace #2 { And will I hear him speak? } }
        \line { I'm downright dizzy wi' the thought, }
        \line { \hspace #2 { In troth I'm like to greet. } }
        \line { \hspace #4 { For there's nae luck, etc. } }
      }
      \hspace #4 \raise #1 \draw-line #'(0 . -88) \hspace #4
      \left-column {
        \combine \null \vspace #0.8
        \line { There are twa hens upon the bauk }
        \line { \hspace #2 { Hae fed this month and mair, } }
        \line { Mak' haste and thraw their necks about, }
        \line { \hspace #2 { That Colin weel may fare; } }
        \line { And spread the table neat and clean, }
        \line { \hspace #2 { Gar ilka thing look braw; } }
        \line { For wha can tell how Colin fared, }
        \line { \hspace #2 { When he was far awa'. } }
        \line { \hspace #4 { For there's nae luck, etc. } }
        \combine \null \vspace #0.8
        \line { The cauld blasts o' the winter wind, }
        \line { \hspace #2 { That thirled through my heart, } }
        \line { They're a'blawn by, I hae him safe, }
        \line { \hspace #2 { Till death we'll never part; } }
        \line { But what puts parting in my head, }
        \line { \hspace #2 { It may be far awa'; } }
        \line { The present moment is our ain'  }
        \line { \hspace #2 { The neist we never saw! } }
        \line { \hspace #4 { For there's nae luck, etc. } }
        \combine \null \vspace #0.8
        \line { Since Colin's weel, I'm weel content, }
        \line { \hspace #2 { I hae nae mair to crave; } }
        \line { Could I but live to mak' him blest, }
        \line { \hspace #2 { I'm blest aboon the lave. } }
        \line { And will I see his face again? }
        \line { \hspace #2 { And will I hear him speak? } }
        \line { I'm downright dizzy wi' the thought, }
        \line { \hspace #2 { In troth I'm like to greet. } }
        \line { \hspace #4 { For there's nae luck, etc. } }
      }
    }
  }
}
