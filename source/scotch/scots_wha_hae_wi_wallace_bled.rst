.. Page 103 (PDF Page 55, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################################
Scots, wha hae wi' Wallace bled!
################################

.. lilyinclude:: ./scots_wha_hae_wi_wallace_bled.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 103
<https://archive.org/details/the-celtic-song-book/page/n54/mode/1up>`_)
