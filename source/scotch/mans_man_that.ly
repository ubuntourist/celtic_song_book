% A Man's a Man for a' That
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.25 (kjc)
%
% Source: The Celtic Song Book, p. 99
%         https://archive.org/details/the-celtic-song-book/page/n52/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "A Man's a Man for a' That"
  composer = \markup { \italic { "Poem by" } \smallCaps "Burns." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key af \major
  \time 2/4
  \tempo \markup { \italic "Moderately quick." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 ef'8^\mf                                    | %   0
    af8. bf16 af8 ef8                                      | %   1
    f8 af8 bf8 df8                                         | %   2
    c8. bf16 af8 ef8                                       | %   3

    \break

    f4 f8
    <<
      { \voiceOne ef8 }
      \new Voice
      { \voiceTwo ef16 ef16 }
    >>       
    \oneVoice                                              | %   4
    af8. bf16 af8 ef8                                      | %   5
    f8 af8 bf8. df16                                       | %   6

    \break

    c8. bf16 af8 f8                                        | %   7
    ef4 ef8 df'8                                           | %   8
    c8.([ df16]) ef8 c8                                    | %   9

    \break

    df8.([ c16]) bf8 df8                                   | %  10
    c8. df16 ef8 af,8                                      | %  11
    f4 f8 df'8                                             | %  12

    \break

    c8. df16 ef8 c8                                        | %  13
    f8 bf,8 bf8.\fermata df16                              | %  14
    c8. bf16 af8 f8                                        | %  15
    ef4 ef8                                                  %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Is                                           | %   0
  there for hon -- est                         | %   1
  pov -- er -- ty That                         | %   2
  hangs his head, and                          | %   3
  a' that? The _                               | %   4
  cow -- ard slave we                          | %   5
  pass him by, We                              | %   6
  daur be puir for                             | %   7
  a' that. For                                 | %   8
  a'.............. that, and                   | %   9
  a'.............. that, Our                   | %  10
  toils ob -- scure, and                       | %  11
  a' that; The                                 | %  12
  rank is but the                              | %  13
  gui -- nea's stamp, The                      | %  14
  man's the gowd for                           | %  15
  a' that.                                       %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { What tho' on hamely fare we dine, }
      \line { \hspace #2 { Wear hoddingrey, and a' that, } }
      \line { Gie fools their silks, and knaves their wine; }
      \line { \hspace #2 { A man's a man for 'a that. } }
      \line { For a' that and a' that, }
      \line { \hspace #2 { Their tinsel show and a' that, } }
      \line { The honest man, tho' ne'er sae puir, }
      \line { \hspace #2 { Is king o' men for a' that. } }
      \vspace #0.8
      \line { A king can mak' a belted knight, }
      \line { \hspace #2 { A marquis, duke, and a' that; } }
      \line { But an honest man's aboon his micht, }
      \line { \hspace #2 { Gude faith he maunna fa' that! } }
      \line { For a' that, and a' that, }
      \line { \hspace #2 { Their dignities, and a' that, } }
      \line { The pith o' sense, and pride o' worth, }
      \line { \hspace #2 { Are higher ranks than a' that. } }
      \vspace #0.8
      \line { Then let us pray that come it may, }
      \line { \hspace #2 { As come it will, for a' that, } }
      \line { That sense and worth, o'er a' the earth, }
      \line { \hspace #2 { May bear the gree, and a' that. } }
      \line { For a' that, and a' that, }
      \line { \hspace #2 { It's comin' yet for a' that, } }
      \line { When man to man, the warld o'er, }
      \line { \hspace #2 { Shall brithers be for a' that. } }
    }
  }
}
