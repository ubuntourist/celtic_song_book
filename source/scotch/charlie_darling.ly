% Charlie is my Darling
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.25 (kjc)
%
% Source: The Celtic Song Book, p. 101
%         https://archive.org/details/the-celtic-song-book/page/n53/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Charlie is my Darling"
  composer = \markup { \smallCaps "Anonymous." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 4/4
  \tempo \markup { \italic "Quick." } 4 = 100
% \mergeDifferentlyDottedOn
% \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 g'8^\f                                      | %   0
    c,8. d16 ef8. f16 g4 c8. g16                           | %   1
    af4 c8. af16 g4 c8. g16                                | %   2

    \break

    c,8. d16 ef8. f16 g4 c8. d16                           | %   3
    ef4 d8. c16 c4.                                          %   4

    \bar "||"
    \break

    \partial 8 c8                                          |
    b8 g8 a8 b8
    <<
      { \voiceOne c8.([ d16]) }
      \new Voice
      { \voiceTwo c8. d16 }
    >>
    \oneVoice
    ef8 c8                                                 | %   5
    <<
      { \voiceOne b8. }
      \new Voice
      { \voiceTwo b16 b8 }
    >>
    \oneVoice
    g16 a8 b8 c4 r8 d8                                     | %   6

    \break

    ef8. d16 ef8. c16
    <<
      { \voiceOne bf8([ g8]) }
      \new Voice
      { \voiceTwo bf8 g8 }
    >>
    \oneVoice
    ef8
    <<
      { \voiceOne f16([ g16]) }
      \new Voice
      { \voiceTwo f16 g16 }
    >>
    \oneVoice                                              | %   7
    <<
      { \voiceOne af8.([ f16]) }
      \new Voice
      { \voiceTwo af8. f16 }
    >>
    \oneVoice
    g8. ef16 d4 ef8.\fermata([ d16])                       | %   8

    \break

    c8. d16 ef8. f16 g4 c8. g16                            | %   9
    af4 c8. af16 g4 c8. c,16                               | %  10

    \break

    c8. d16 ef8. f16 g4 c8. d16                            | %  11
    ef4 d8. c16 c4.                                          %  12

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh!                                          | %   0
  Char -- lie is my dar -- ling, my            | %   1
  dar -- ling, my dar -- ling, Oh!             | %   2
  Char -- lie is my dar -- ling, The           | %   3
  young Che -- va -- lier.                       %   4
  'Twas                                        | %
  on a Mon -- day morn -- _ ing, Right         | %   5
  ear -- _ ly in the year, When                | %   6
  Char -- lie came to our _ town, The _        | %   7
  young _ Che -- va -- lier. Oh!               | %   8
  Char -- lie is my dar -- ling, my            | %   9
  dar -- ling, my dar -- ling, Oh!             | %  10
  Char -- lie is my dar -- ling, The           | %  11
  young Che -- va -- lier                        %  12
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { As he cam' marchin' up the street, }
      \line { \hspace #2 { The pipes play'd loud and clear; } }
      \line { And a' the folk cam' rinnin' out }
      \line { \hspace #2 { To meet the Chevalier. } }
      \line { \hspace #4 { Oh! Charlie, etc.  } }
      \vspace #0.8
      \line { Wi' Hieland bonnets on their heads, }
      \line { \hspace #2 { And claymores bright and clear } }
      \line { They cam' to fight for Scotland's right }
      \line { \hspace #2 { And the young Chevalier. } }
      \line { \hspace #4 { Oh! Charlie, etc.  } }
      \vspace #0.8
      \line { They've left their bonnie Hieland hills, }
      \line { \hspace #2 { Their wives and bairnies dear, } }
      \line { To draw the sword for Scotland's Lord, }
      \line { \hspace #2 { The young Chevalier. } }
      \line { \hspace #4 { Oh! Charlie, etc.  } }
      \vspace #0.8
      \line { Oh! there were mony beating hearts, }
      \line { \hspace #2 { And mony a hope and fear; } }
      \line { And mony were the pray'rs put up }
      \line { \hspace #2 { For the young Chevalier. } }
      \line { \hspace #4 { Oh! Charlie, etc.  } }
    }
  }
}
