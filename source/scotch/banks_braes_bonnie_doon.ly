% Ye Banks and Braes o' Bonnie Doon
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.04.11 (kjc)
%
% Source: The Celtic Song Book, p. 112
%         https://archive.org/details/the-celtic-song-book/page/n59/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Ye Banks and Braes o' Bonnie Doon"
  composer = \markup { \italic "Poem by"  \smallCaps "Burns." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key g \major
  \time 6/8
  \tempo \markup { \italic "Moderately slow." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative c' {
    \global

    \partial 8 d8^\mp                        | %   0
    g4 g8 a8[( g8]) a8                       | %   1
    b8[( d8]) b8 a8[( g8]) a8                | %   2

    \break

    b8.[( a16]) g8 g8[( e8]) d8              | %   3
    d8[( e8]) g8 a4
    <<
      { \voiceOne b16[( a16)] }
      \new Voice
      { \voiceTwo d,8 }
    >>
    \oneVoice                                | %   4
    g4 g8 a8[( g8]) a8                       | %   5

    \break

    b8[( d8]) b8 a8.[( g16]) a8              | %   6
    b8.[( a16]) g8 g8[( e8]) d8              | %   7

    \break

    d8[( e8]) g8 g4 b8                       | %   8
    d4 e8 d8[( b8]) g8                       | %   9
    d'4 e8 d8[( b8]) g8                      | %  10

    \break

    d'8[( b8]) g8 d'8[( b8]) g8              | %  11
    e'8[( d8]) c16[( b16]) a4\fermata d,8    | %  12
    g4 g8 a8[( g8])  a8                      | %  13

    \break

    b8[( d8]) b8 a8[( g8]) a8                | %  14
    b8.[( a16]) g8 g8[( e8]) d8              | %  15
    d8[( e8]) g8 g4\fermata                    %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Ye                                      | %   0
  banks and braes o'                      | %   1
  bon -- nie Doon, How                    | %   2
  can ye bloom sae                        | %   3
  fresh and fair? How _                   | %   4
  can ye chaunt, ye                       | %   5
  lit -- tle birds, And                   | %   6
  I'm.... sae wea -- ry                   | %   7
  fu' o' care? Ye'll                      | %   8
  break my heart, ye                      | %   9
  war -- bling bird That                  | %  10
  war -- bles on the                      | %  11
  flow -- 'ry thorn, Ye                   | %  12
  mind me o' de --                        | %  13
  part -- ed joys, De --                  | %  14
  part -- ed nev -- er                    | %  15
  to re -- turn.                            %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { "Oft ha'e I roved by bonnie Doon," }
      \line { \hspace #2 { "By morning and by ev'ning shine," } }
      \line { "To hear the birds sing o' their loves," }
      \line { \hspace #2 { "As fondly once I sang o' mine." } }
      \line { "Wi' lightsome heart I stretch'd my hand" }
      \line { \hspace #2 { "And pu'd a rosebud from the tree;" } }
      \line { "But my fause lover stole the rose" }
      \line { \hspace #2 { "And left, and left the thorn wi' me." } }
    }
  }
}
