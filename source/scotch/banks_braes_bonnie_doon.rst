.. Page 112 (PDF Page 59, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

#################################
Ye Banks and Braes o' Bonnie Doon
#################################

.. lilyinclude:: ./banks_braes_bonnie_doon.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 112
<https://archive.org/details/the-celtic-song-book/page/n59/mode/1up>`_)
