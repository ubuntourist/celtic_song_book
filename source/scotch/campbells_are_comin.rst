.. Page 104 (PDF Page 56, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

########################
The Campbells are comin'
########################

.. lilyinclude:: ./campbells_are_comin.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 104
<https://archive.org/details/the-celtic-song-book/page/n55/mode/1up>`_)
