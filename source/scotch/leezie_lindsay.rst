.. Page 111 (PDF Page 58, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

##############
Leezie Lindsay
##############

.. lilyinclude:: ./leezie_lindsay.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 111
<https://archive.org/details/the-celtic-song-book/page/n58/mode/1up>`_)
