% The Campbells are comin'
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.27 (kjc)
%
% Source: The Celtic Song Book, p. 104
%         https://archive.org/details/the-celtic-song-book/page/n55/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Campbells are comin'"
  composer = \markup { \smallCaps { "Traditional." } }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 6/8
  \tempo \markup { \italic "In march time." } 4 = 120
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8 c'8^\mf                           | %   0
    e8 g8 a8 g8 e8 c8                            | %   1
    e4 e8 e4 d8                                  | %   2

    \break

    e8 g8 a8 g8 e8 c8                            | %   3
    d4 d8 d4 c8                                  | %   4
    e8 g8 a8 g8 e8 c8                            | %   5

    \break

    e8. f16 e8 c'8 d8 e8                         | %   6
    c8 a8 c8 g8 e8 c8                            | %   7
    e4^\< e8\! e4\fermata                          %   8

    \once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
    \mark \markup { \small "FINE" }

    \bar "||"
    \break

    \partial 8 g8^\f                             | %   9
    c4 c8
    <<
      { \voiceOne  c8. d16 }
      \new Voice
      { \voiceTwo c8.[( d16]) }
    >>
    \oneVoice
    e8                                           | %  10
    g,4 g8 g8.[( e16]) g8                        | %  11

    \break

    c4 c8
    <<
      { \voiceOne  c8. d16 }
      \new Voice
      { \voiceTwo c8.[( d16]) }
    >>
    \oneVoice
    e8                                           | %  12
    a,4 a8 a4 g8                                 | %  13
    g8.[( a16]) b8 c8.[( b16]) a8                | %  14

    \break

    <<
      { \voiceOne g8. a16 }
      \new Voice
      { \voiceTwo g8.[( a16]) }
    >>
    \oneVoice
    b8
    <<
      { \voiceOne c8 d8 }
      \new Voice
      { \voiceTwo c8[( d8]) }
    >>
    \oneVoice
    e8                                           | %  15
    <<
      { \voiceOne c8[( a8]) }
      \new Voice
      { \voiceTwo c8 a8 }
    >>
    \oneVoice
    c8
    <<
      { \voiceOne g8[( e8]) }
      \new Voice
      { \voiceTwo g8 e8 }
    >>
    \oneVoice
    c8                                           | %  16
    e4 e8 e4                                       %  17

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  The                                            | %   0
  Camp -- bells are com -- in' o --              | %   1
  ho, o -- ho, The                               | %   2
  Camp -- bells are com -- in' o --              | %   3
  ho, o -- ho. The                               | %   4
  Camp -- bells are com -- in' To                | %   5
  bon -- nie Loch -- le -- ven; The              | %   6
  Camp -- bells are com -- in' o --              | %   7
  ho, o -- ho.                                     %   8
  Up --                                          | %   9
  on the Lom -- onds I                           | %  10
  lay, I lay, __ Up --                           | %  11
  on the Lom -- onds I                           | %  12
  lay, I lay, I                                  | %  13
  look -- ed down to                             | %  14
  bon -- nie Loch -- le -- ven, And              | %  15
  saw _ three bon -- _ nie                       | %  16
  pi -- pers play.                                 %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { Great Argyle, he goes before, }
      \line { He makes the cannons and guns to roar, }
      \line { Wi' sound o' trumpet, pipe, and drum, }
      \line { The Campbells are comin', o-ho, o-ho. }

      \vspace #0.8

      \line { The Campbells they are a' in arms, }
      \line { Their loyal faith and truth to show; }
      \line { Wi' banners rattlin' in the wind, }
      \line { The Campbells are comin', o-ho, o-ho. }
    }
  }
}
