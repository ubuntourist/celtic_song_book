% Robin Adair
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.28 (kjc)
%
% Source: The Celtic Song Book, p. 106
%         https://archive.org/details/the-celtic-song-book/page/n56/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Robin Adair"
  poet = \markup { \italic "Poem by"  \smallCaps "Burns." }
  composer = \markup { \italic "Irish and Scotch form of melody." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key af \major
  \time 3/4
  \tempo \markup { \italic "Moderately slow." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

% BEAD (the flats)

melody = {
  \relative {
    \global

    ef'4^\p f4 g4                            | %   1
    af4. bf8 c4                              | %   2
    ef,4 f4 g4                               | %   3
    af2 r4                                   | %   4

    \break

    ef4 f4 g4                                | %   5
    af4. bf8 c4                              | %   6
    ef,4 f4 g4                               | %   7
    af2 r4                                   | %   8

    \break

    c4^\cresc c4 c4                          | %   9
    df4. f,8 f4                              | %  10
    c'4 c4 bf4                               | %  11
    af4. f8 ef4                              | %  12

    \break

    ef'4^\f df8[( c8]) bf8[( af8])           | %  13
    af4. bf8 c4                              | %  14
    ef,4 f4 g4                               | %  15
    af2.\fermata                             | %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  What's this dull                        | %   1
  town to me?                             | %   2
  Ro -- bin's not                         | %   3
  near.                                   | %   4
  What was't I                            | %   5
  wish'd to see,                          | %   6
  What wish'd to                          | %   7
  hear?                                   | %   8
  Where all the                           | %   9
  joy and mirth                           | %  10
  Made this town                          | %  11
  heav'n on earth?                        | %  12
  Oh, they're all                         | %  13
  fled with thee,                         | %  14
  Ro -- bin A --                          | %  15
  dair.                                   | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺  
\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { What made th' asssembly shine? }
        \line { \hspace #8 { Robin Adair. } }
        \line { What made the ball so fine? }
        \line { \hspace #8 { Robin was there. } }
        \line { What when the play was o'er, }
        \line { What made my heart so sore? }
        \line { Oh, it was parting with }
        \line { \hspace #8 { Robin Adair. } }
      }
      \hspace #4 \raise #1 \draw-line #'(0 . -25) \hspace #4
      \left-column {
        \combine \null \vspace #0.8
        \line { But now thou'rt cold to me, }
        \line { \hspace #8 { Robin Adair. } }
        \line { But now thou'rt cold to me, }
        \line { \hspace #8 { Robin Adair. } }
        \line { Yet he I lov'd so well }
        \line { Still in my heart shall dwell; }
        \line { Oh, I can ne'er forget }
        \line { \hspace #8 { Robin Adair. } }
      }
    }
  }
}
