% Jock o' Hazeldean
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.03.30 (kjc)
%
% Source: The Celtic Song Book, p. 107
%         https://archive.org/details/the-celtic-song-book/page/n56/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Jock o' Hazeldean"
  composer = \markup { \italic { "Poem by" } \smallCaps "Sir Walter Scott." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key e \major
  \time 2/4
  \tempo \markup { \italic "Fairly slow." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

% FCGD

melody = {
  \relative {
    \global
    \partial 8 e''8^\mf                          | %   0
    b8 gs8 fs8. e16                              | %   1
    e8 gs8 b,8 cs8                               | %   2
    e8 e8 a8. gs16                               | %   3

    \break

    gs4( fs8) e'8                                | %   4
    b8 gs8 fs8. e16                              | %   5
    e8 gs8 b,8 cs8                               | %   6
    e8 gs8 fs8. e16                              | %   7

    \break

    e4. gs8                                      | %   8
    a8. gs16 a8 b8                               | %   9
    cs8 b8 e8 cs8                                | %  10
    b8 gs8 \tuplet 3/2 { fs8^\<[( gs8]) b8\! }   | %  11

    \break

    cs4. e8                                      | %  12
    b8 gs8 fs8. e16^\>                           | %  13
    e8 gs8\! b,8\fermata cs8                     | %  14
    e8 gs8 fs8. e16                              | %  15
    e4.\fermata                                    %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Why                                            | %   0
  weep ye by the                                 | %   1
  tide, la -- dye? Why                           | %   2
  weep ye by the                                 | %   3
  tide? I'll                                     | %   4
  wed ye to my                                   | %   5
  youn -- gest son, And                          | %   6
  ye shall be his                                | %   7
  bride; And                                     | %   8
  ye shall be his                                | %   9
  bride, la -- dye, Sae                          | %  10
  come -- ly to be                               | %  11
  seen, But                                      | %  12
  aye she loot the                               | %  13
  tears down fa' For                             | %  14
  Jock o' Ha -- zel --                           | %  15
  dean.                                          | %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺
\markup {
  \hspace #10 {
    \column {
      \vspace #0.8

      \line { "Now let this wilfu' grief be done," }
      \line { \hspace #2 { "And dry that cheek so pale," } }
      \line { "Young Frank is chief of Errington," }
      \line { \hspace #2 { "And lord of Langleydale," } }
      \line { "His step is first in peaceful ha'," }
      \line { \hspace #2 { "His sword in battle keen⸺" } }
      \line { "But aye she loot the tears down fa'" }
      \line { \hspace #2 { "For Jock o' Hazeldean." } }

      \vspace #0.8

      \line { "A chain o' gold ye shall not lack," }
      \line { \hspace #2 { "Nor braid to bind your hair," } }
      \line { "Nor mettled hound, nor managed hawk," }
      \line { \hspace #2 { "Nor palfrey fresh and fair;" } }
      \line { "And you, the foremost o' them a'," }
      \line { \hspace #2 { "Shall ride our forest queen⸺" } }
      \line { "But aye she loot the tears down fa'" }
      \line { \hspace #2 { "For Jock o' Hazeldean." } }

      \vspace #0.8

      \line { "The kirk was deck'd at morning tide," }
      \line { \hspace #2 { "The taper glimmer'd fair," } }
      \line { "The priest and bridegroom wait the bride," }
      \line { \hspace #2 { "And dame and knight are there." } }
      \line { "They sought her baith by bow'r and ha'," }
      \line { \hspace #2 { "The lady was not seen;" } }
      \line { "She's o'er the border and awa'" }
      \line { \hspace #2 { "Wi' Jock o' Hazeldean." } }
    }
  }
}
