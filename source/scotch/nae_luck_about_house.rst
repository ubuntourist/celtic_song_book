.. Page 96 (PDF Page 52, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

################################
There's nae Luck about the House
################################

.. lilyinclude:: ./nae_luck_about_house.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 96
<https://archive.org/details/the-celtic-song-book/page/n51/mode/1up>`_)
