.. Page 100 (PDF Page 54, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

############
Annie Laurie
############

.. lilyinclude:: ./annie_laurie.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 100
<https://archive.org/details/the-celtic-song-book/page/n53/mode/1up>`_)
