.. Page 106 (PDF Page 57, Left)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: obscure

###########
Robin Adair
###########

.. lilyinclude:: ./robin_adair.ly
   :nofooter:
   :noedge:
   :audio:
   :controls: top

----

(Source: `The Celtic Song Book, p. 106
<https://archive.org/details/the-celtic-song-book/page/n56/mode/1up>`_)
