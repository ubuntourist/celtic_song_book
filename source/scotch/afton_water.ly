% Afton Water
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.24 (kjc)
%
% Source: The Celtic Song Book, p. 97
%         https://archive.org/details/the-celtic-song-book/page/n51/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Afton Water"
  composer = \markup { \italic "Poem by"  \smallCaps "Burns." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key e \major
  \time 3/4
  \tempo \markup { \italic "Moderately slow." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global

    \partial 4 b4^\p                         | %   0
    e4 gs4 fs8([ e8])                        | %   1
    fs8([ gs8]) a4 gs8([ fs8])               | %   2
    e4 gs4 fs8([ e8])                        | %   3

    \break

    e4( ds4) b4                              | %   4
    e4 gs4 fs8([ e8])                        | %   5
    fs8([ gs8]) b4 e8([ cs8])                | %   6

    \break

    b4 a8([ gs8]) fs8([ gs8])                | %   7
    e2 b'8([ cs8])                           | %   8
    d4 e8([ d8]) cs8([ b8])                  | %   9

    \break

    b4 cs4 e4                                | %  10
    e4 gs,4 fs8([ e8])                       | %  11
    e4( ds4) b4                              | %  12
    e4 gs4 fs8([ e8])                        | %  13

    \break

    fs8([ gs8]) b4 e8([ cs8])                | %  14
    b4 a8([ gs8]) fs8([ gs8])                | %  15
    e2\fermata                                 %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Flow                                    | %   0
  gen -- tly, sweet                       | %   1
  Af -- ton, a --                         | %   2
  mang thy green                          | %   3
  braes, Flow                             | %   4
  gen -- tly, I'll                        | %   5
  sing thee a                             | %   6
  song in thy                             | %   7
  praise; My                              | %   8
  Ma -- ry's a --                         | %   9
  sleep by thy                            | %  10
  mur -- mur -- ing                       | %  11
  stream. Flow                            | %  12
  gent -- ly, sweet                       | %  13
  Af -- ton, dis --                       | %  14
  turb not her                            | %  15
  dream.                                    %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Thou stock-dove, whose echo resounds thro' the glen, }
      \line { Ye wild whistling blackbirds in yon thorny den, }
      \line { Thou green-crested lapwing, thy screaming forbear, }
      \line { I charge you disturb not my slumbering fair. }
      \vspace #0.8
      \line { How lofty, sweet Afton, thy neighbouring hills, }
      \line { Far marked with the courses of clear winding rills; }
      \line { There daily I wander as morn rises high, }
      \line { My flocks and my Mary's sweet cot in my eye. }
      \vspace #0.8
      \line { How pleasant thy banks and green valleys below, }
      \line { Where wild in the woodlands the primroses blow! }
      \line { There oft as mild ev'ning creeps over the lea, }
      \line { The sweet-scented birk shades my Mary and me. }
      \vspace #0.8
      \line { Thy crystal stream, Afton, how lovely it glides, }
      \line { And winds by the cot where my Mary resides! }
      \line { How wanton thy waters her snowy feet lave, }
      \line { As gath'ring sweet flow'rets she stems thy clear wave! }
      \vspace #0.8
      \line { Flow gently, sweet Afton, amang thy green braes, }
      \line { Flow gently, sweet river, the theme of my lays; }
      \line { My Mary's asleep by  thy murmuring stream, }
      \line { Flow gently, sweet Afton, disturb not her dream. }
    }
  }
}
