% The Bonnie Brier-Bush
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.23 (kjc)
%
% Source: The Celtic Song Book, p. 93
%         https://archive.org/details/the-celtic-song-book/page/n49/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Bonnie Brier-Bush"
  composer = \markup { \smallCaps "Anonymous" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key g \major
  \time 4/4
  \tempo \markup { \italic "In moderate time." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global

    \partial 4  b'4^\mf                      | %   0
    <<
      { \voiceOne d,8. d16 d8 e8 }
      \new Voice
      { \voiceTwo d4 d8([ e8]) }
    >>
    \oneVoice
    d8([ e8]) g8. d16                        | %   1
    e4 a8.([ b16]) a4 g8([ a16 b16])         | %   2

    \break

    <<
      { \voiceOne d,4 d8. e16 d8 e8 }
      \new Voice
      { \voiceTwo d8. d16 d8.([ e16]) d8([ e16]) }
    >>
    \oneVoice
    g8. d16                                  | %   3
    e4 g8.([ a16]) g4 r8
    <<
      { \voiceOne b8 }
      \new Voice
      { \voiceTwo b16 b16 }
    >>
    \oneVoice                                | %   4

    \break

    c8.^\cresc d16 e8 c8
    <<
      { \voiceOne  b8.([ c16]) }
      \new Voice
      { \voiceTwo b8. c16 }
    >>
    \oneVoice
    d8 b8                                    | %   5
    <<
      { \voiceOne c8 b8 a8 g8  }
      \new Voice
      { \voiceTwo c8([ b8]) a8([ g8]) }
    >>
    \oneVoice
    a4 g8^\> a16([ b16])\!                   | %   6

    \break

    <<
      { \voiceOne d,16\p d8. d8. e16 d8 e8 g8. d16 }
      \new Voice
      { \voiceTwo d4 d8.([ e16]) d8([ e8]) g8.([ d16]) }
    >>
    \oneVoice                                | %   7
    e4 g8.([ a16]) g4                          %   8

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  There                                   | %   0
  grows a bon -- nie brier -- bush in     | %   1
  our kail -- yard, And...                | %   2
  white _ are the blos -- soms on't in    | %   3
  our kail -- yard, Like _                | %   4
  wee bit white cock -- ades _ for our    | %   5
  loy -- al Hie -- land lads; And the     | %   6
  lass -- es lo'e the bon -- nie bush in  | %   7
  our kail -- yard.                         %   8
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { But were they a' true that were far awa'? }
      \line { Oh! were they a' true that were far awa'?}
      \line { They drew up wi' glaíket Englishers at Carlisle ha', }
      \line { And forgot auld friends when far awa'. }
      \vspace #0.8
      \line { Ye'll come nae mair, Jamie, where aft ye hae been, }
      \line { Ye'll come nae mair, Jamie, to Athol Green; }
      \line { Ye lo'ed owre weel the dancin' at Carlisle ha', }
      \line { And forgot the Hieland hill that were far awa'. }
      \vspace #0.8
      \line { He's comin' frae the north that's to fancy me, }
      \line { He's comin' frae the north that's to fancy me, }
      \line { A feather in his bonnet, and a ribbon at his knee; }
      \line { He's a bonnie Hieland laddie, and you be na he. }
    }
  }
}
