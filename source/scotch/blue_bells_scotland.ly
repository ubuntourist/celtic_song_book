% The Blue Bells of Scotland
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.24 (kjc)
%
% Source: The Celtic Song Book, p. 94
%         https://archive.org/details/the-celtic-song-book/page/n50/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Blue Bells of Scotland"
  composer = \markup { \smallCaps "Anonymous" }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key ef \major
  \time 4/4
  \tempo \markup { \italic "Moderately slow, but flowing." } 4 = 80
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global

    \partial 8  bf'8^\mf                     | %   0
    ef4 d8 c8 bf4 c8 d16([ ef16])            | %   1
    g,8 g8 af8 f8 ef4. bf'8                  | %   2

    \break

    ef4 d8 c8 bf4 c8 d16([ ef16])            | %   3
    g,8 g8 af8 f8 ef4 r8 bf'8                | %   4

    \break

    g8. ef16 g8 bf8
    <<
      { \voiceOne ef4 }
      \new Voice
      { \voiceTwo ef8 ef8 }
    >>
    \oneVoice
    c8 d16([ ef16])                          | %   5
    d8 bf8 c8 a8^\< bf4. c16 d16\!           | %   6

    \break

    <<
      { \voiceOne ef4 }
      \new Voice
      { \voiceTwo ef8. ef16 }
    >>
    \oneVoice
    d8 c8 bf4
    <<
      { \voiceOne c8([ d16 ef16]) }
      \new Voice
      { \voiceTwo c8 d16([ ef16]) }
    >>
    \oneVoice                                | %   7
    g,8 g8 af8 f8 ef4 r8 bf'8                | %   8

    \break

    g8. ef16 g8 bf8
    <<
      { \voiceOne ef4 }
      \new Voice
      { \voiceTwo ef8 ef8 }
    >>
    \oneVoice
    c8 d16([ ef16])                          | %   9
    d8 bf8 c8 a8 bf4. c16 d16                | %  10

    \break

    <<
      { \voiceOne ef4 }
      \new Voice
      { \voiceTwo ef8. ef16 }
    >>
    \oneVoice
    d8 c8 bf4
    <<
      { \voiceOne c8([ d16 ef16]) }
      \new Voice
      { \voiceTwo c8 d16([ ef16]) }
    >>
    \oneVoice                                | %  11
    g,8 g8 af8 f8 ef4.\fermata                 %  12

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Oh!                                          | %   0
  where, tell me where, is your                | %   1
  High -- land lad -- die gone? Oh!            | %   2
  where, tell me where, is your                | %   3
  High -- land lad -- die gone? He's           | %   4
  gone with stream -- ing ban -- _ ners where  | %   5
  no -- ble deeds are done, And it's           | %   6
  oh! _ in my heart I................. _       | %   7
  wish him safe at home. He's                  | %   8
  gone with stream -- ing ban -- _ ners where  | %   9
  no -- ble deeds are done, And it's           | %  10
  oh! _ in my heart I................. _       | %  11
  wish him safe at home.                         %  12
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

\markup {
  \hspace #10 {
    \column {
      \vspace #0.8
      \line { Oh! where, tell me where, did your Highland laddie dwell? }
      \line { Oh! where, tell me where, did your Highland laddie dwell? }
      \line { He dwelt in bonnie Scotland, where blooms the sweet blue bell, }
      \line { And it's oh! in my heart I lo'e my laddie well. }
      \line { He dwelt in bonnie Scotland, where blooms the sweet blue bell, }
      \line { And it's oh! in my heart I lo'e my laddie well. }
      \vspace #0.8
      \line { Oh! what, tell me what, does your Highland laddie wear? }
      \line { Oh! what, tell me what, does your Highland laddie wear? }
      \line { A bonnet with a lofty plume, and on his breast a plaid, }
      \line { And it's oh! in my heart I lo'e my Highland lad. }
      \line { A bonnet with a lofty plume, and on his breast a plaid, }
      \line { And it's oh! in my heart I lo'e my Highland lad. }
      \vspace #0.8
      \line { Oh! what, tell me what, if your Highland lad be slain? }
      \line { Oh! what, tell me what, if your Highland lad be slain? }
      \line { Oh no! true love will be his guard and bring him safe again, }
      \line { For it's oh! my heart would break if my Highland lad were slain. }
      \line { Oh no! true love will be his guard and bring him safe again, }
      \line { For it's oh! my heart would break if my Highland lad were slain. }
    }
  }
}
