% Annie Laurie
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.11.25 (kjc)
%
% Source: The Celtic Song Book, p. 100
%         https://archive.org/details/the-celtic-song-book/page/n53/mode/1up

\version "2.22.1"
\language "english"


\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "Annie Laurie"
  composer = \markup { \smallCaps "Anonymous." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key c \major
  \time 2/4
  \tempo \markup { \italic "Moderately slow." } 4 = 70
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8
    <<
      { \voiceOne r8^\mf }
      \new Voice
      { \voiceTwo e'8 }
    >>
    \oneVoice                                            | %   0
    c8
    <<
      { \voiceOne c16 c16 }
      \new Voice
      { \voiceTwo c8 }
    >>
    \oneVoice
    c'8 c8                                               | %   1
    b8 a8 r8
    <<
      { \voiceOne a8 }
      \new Voice
      { \voiceTwo a16 a16 }
    >>
    \oneVoice                                            | %   2
    g8. f16
    <<
      { \voiceOne e8([ d16]) }
      \new Voice
      { \voiceTwo e16 e16[ d16] }
    >>
    \oneVoice
    c16                                                  | %   3

    \break

    e8( d4)
    <<
      { \voiceOne e16 d16 }
      \new Voice
      { \voiceTwo e16([ d16]) }
    >>
    \oneVoice                                            | %   4
    c8. c16 c'8 c8                                       | %   5
    b8 a8 r8 a8                                          | %   6
    g8. e16 e8. d16                                      | %   7

    \break

    c4 r8 g'8                                            | %   8
    c8. c16 d8. d16                                      | %   9
    e4 r8
    <<
      { \voiceOne g,8 }
      \new Voice
      { \voiceTwo g16 g16 }
    >>
    \oneVoice                                            | %  10
    c8.^\cresc c16 d8. d16                               | %  11

    \break

    e4^\sf e8^\p d8                                      | %  12
    c8 b8 a8 c16([ a16])                                 | %  13
    g8 e8 e8.([ d16])                                    | %  14
    c16([ c'8])^\pp 
    \tempo \markup { \italic "Slower." } 4 = 70
    e,16 e8. d16                                         | %  15
    c4.\fermata                                            %  16

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  _                                       | %   0
  Max -- well -- ton braes are            | %   1
  bon -- nie, Where _                     | %   2
  ear -- ly fa's...... _ the              | %   3
  dew, And it's                           | %   4
  there that An -- nie                    | %   5
  Lau --rie Gie'd                         | %   6
  me her pro -- mise                      | %   7
  true⸺  Gie'd                            | %   8
  me her pro -- mise                      | %   9
  true, Which _                           | %  10
  ne'er for -- got will                   | %  11
  be; And for                             | %  12
  bon -- nie An -- nie                    | %  13
  Lau -- rie I'd                          | %  14
  lay me doon and                         | %  15
  dee.                                      %  16
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺  
\markup {
  \fill-line {
    \concat {
      \left-column {
        \combine \null \vspace #0.8
        \line { Her brow is like the snaw-drift, }
        \line { \hspace #2 { Her neck is like the swan, } }
        \line { Her face it is the fairest }
        \line { \hspace #2 { That e'er the sun shone on⸺   } }
        \line { \hspace #2 { That e'er the sun shone on, } }
        \line { \hspace #2 { And dark blue is her e'e; } }
        \line { And for bonnie Annie Laurie }
        \line { \hspace #2 { I'd lay me doon and dee. } }
      }
      \hspace #4 \raise #1 \draw-line #'(0 . -25) \hspace #4
      \left-column {
        \combine \null \vspace #0.8
        \line { Like dew on the gowan lying }
        \line { \hspace #2 { It is the fa' o' her fairy feet; } }
        \line { And like the winds in summer sighing, }
        \line { \hspace #2 { Her voice is low and sweet⸺   } }
        \line { \hspace #2 { Her voice is low and sweet,  } }
        \line { \hspace #2 { And she's a' the world to me; } }
        \line { And for bonnie Annie Laurie }
        \line { \hspace #2 { I'd lay me doon and dee. } }
      }
    }
  }
}
