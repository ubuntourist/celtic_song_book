% The Hundred Pipers
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2022.04.05 (kjc)
%
% Source: The Celtic Song Book, p. 109
%         https://archive.org/details/the-celtic-song-book/page/n57/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = "The Hundred Pipers"
  composer = \markup { \italic { "Poem by" } \smallCaps "Lady Nairne." }
  tagline = ""
% instrument = \markup { \smallCaps { "Soprano" } }
% arranger = "arranger"
% copyright = "copyright"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key f \major
  \time 6/8
  \tempo \markup { \italic "Quick." } 4 = 100
  \mergeDifferentlyDottedOn
  \mergeDifferentlyHeadedOn
}

melody = {
  \relative {
    \global
    \partial 8
    <<
      { \voiceOne f'16^\f g16 }
      \new Voice
      { \voiceTwo f16[( g16]) }
    >>
    \oneVoice                                    | %   0
    a4 c,8 
    <<
      { \voiceOne c8 d8 }
      \new Voice
      { \voiceTwo c8[( d8]) }
    >>
    \oneVoice
    c8                                           | %   1
    d4 f8 f4
    <<
      { \voiceOne d'16 d16 }
      \new Voice
      { \voiceTwo d8 }
    >>
    \oneVoice                                    | %   2

    \break

    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8 c8 }
    >>
    \oneVoice
    a8
    <<
      { \voiceOne a8 g8 }
      \new Voice
      { \voiceTwo a8[( g8]) }
    >>
    \oneVoice
    f8                                           | %   3
    g4 g8 g4
    <<
      { \voiceOne f16[( g16]) }
      \new Voice
      { \voiceTwo f16 g16 }
    >>
    \oneVoice                                    | %   4
    <<
      { \voiceOne a4 }
      \new Voice
      { \voiceTwo a8 a8 }
    >>
    \oneVoice
    c,8 c8 d8 c8                                 | %   5

    \break

    d4 f8 f4
    <<
      { \voiceOne d'16 d16 }
      \new Voice
      { \voiceTwo d8 }
    >>
    \oneVoice                                    | %   6
    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8 c8 }
    >>
    \oneVoice
    a8
    <<
      { \voiceOne g8 a8 }
      \new Voice
      { \voiceTwo g8[( a8]) }
    >>
    \oneVoice
    g8                                           | %   7
    f4 f8 f4
    <<
      { \voiceOne a16 bf16 }
      \new Voice
      { \voiceTwo a16[( bf16]) }
    >>
    \oneVoice                                    | %   8

    \break

    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8 c8 }
    >>
    \oneVoice
    c8 c8 a8 c8                                  | %   9
    c4 f8 f4
    <<
      { \voiceOne e16[( d16]) }
      \new Voice
      { \voiceTwo e16 d16 }
    >>
    \oneVoice                                    | %  10

    \break

    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8 c8 }
    >>
    \oneVoice
    a8
    <<
      { \voiceOne a8 g8 }
      \new Voice
      { \voiceTwo a8[( g8]) }
    >>
    \oneVoice
    f8                                           | %  11
    a4 g8 g4
    <<
      { \voiceOne a16[( bf16]) }
      \new Voice
      { \voiceTwo a16 bf16 }
    >>
    \oneVoice                                    | %  12

    \break

    <<
      { \voiceOne c4 }
      \new Voice
      { \voiceTwo c8. c16 }
    >>
    \oneVoice
    c8
    <<
      { \voiceOne c8[( d8]) }
      \new Voice
      { \voiceTwo c8 d8 }
    >>
    \oneVoice
    e8                                           | %  13
    f4 f8 f4
    <<
      { \voiceOne e16 d16 }
      \new Voice
      { \voiceTwo e16[( d16]) }
    >>
    \oneVoice                                    | %  14
    c4 a8 g8 a8 g8                               | %  15

    \break

    f4 f8 f4^\ff g16 a16                         | %  16
    a4 c,8 c8 d8 c8                              | %  17
    d4 f8 f4 d'16 d16                            | %  18

    \break

    c4 a8 a8 g8 f8                               | %  19
    g4 g8 g4
    <<
      { \voiceOne f16[( g16]) }
      \new Voice
      { \voiceTwo f16 g16 }
    >>
    \oneVoice                                    | %  20
    a4 c,8 c8 d8 c8                              | %  21

    \break

    d4 f8 f4 d'16 d16                            | %  22
    c4 a8 g8 a8 g8                               | %  23
    f4 f8 f4                                       %  24

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5
  Wi' a                                          | %   0
  hun -- dred pi -- pers an'                     | %   1
  a', an' a', Wi' a                              | %   2
  hun -- _ dred pi -- pers an'                   | %   3
  a', an' a', We'll _                            | %   4
  up _ an' gie 'em a                             | %   5
  blaw, a blaw, Wi' a                            | %   6
  hun -- _ dred  pi -- pers an'                  | %   7
  a', an' a'; Oh its                             | %   8
  ower _ the Bor -- der a --                     | %   9
  wa', a -- wa', It's _                          | %  10
  ower _ the Bor -- der a --                     | %  11
  wa', a -- wa', We'll _                         | %  12
  on an' we'll march _ to                        | %  13
  Car -- lisle Ha'. Wi' its                      | %  14
  yetts, its cas -- tell an'                     | %  15
  a', an' a', Wi' a                              | %  16
  hun -- dred pi -- pers an'                     | %  17
  a', an' a', Wi' a                              | %  18
  hun -- dred pi -- pers an'                     | %  19
  a', an' a'; We'll _                            | %  20
  up an' gie 'em a                               | %  21
  blaw, a blaw, Wi' a                            | %  22
  hun -- dred pi -- pers an'                     | %  23
  a', an' a'.                                      %  24
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \vspace #0.8
  \draw-hline
}

% ⸺
%\markup {
%  \hspace #10 {
%    \column {
%      \vspace #0.8
%
%      \line { "Oh! our sodger lads look'd braw, look'd braw," }
%      \line { "Wi' their tartans, kilts an' a', an' a'," }
%      \line { "Wi' their bonnets, an' feathers, an' glitt'ring gear," }
%      \line { "An' pilbrochs sounding sweet an' clear." }
%      \line { "Will they a' return to their ain dear glen?" }
%      \line { "Will they a' return⸺our Hieland men?" }
%      \line { "Second-sighted Sandy look'd fu' wae," }
%      \line { "And mothers grat when they march'd awa'." }
%      \line { "Wi' a hundred pipers an' a', an' a'," }
%      \line { "Wi' a hundred pipers an' a', an a';" }
%      \line { "But they'll up an' gie 'em a blaw, a blaw," }
%      \line { "Wi' a hundred pipers an' a', an' a'." }
%
%      \vspace #0.8
%
%      \line { "Oh! wa is foremaist o' a', o' a'?" }
%      \line { "Oh wha does follow the blaw, the blaw?" }
%      \line { "Bonnie Charlie, the king o' us a', hurra!" }
%      \line { "Wi' his hundred pipers an' a', an' a'!" }
%      \line { "His bonnet an' feathers he's waving high!" }
%      \line { "His prancing steed maist seems to fly!" }
%      \line { "The nor' wind plays with his curly hair," }
%      \line { "While the pipers blaw in an unco flare!" }
%      \line { "Wi' a hundred pipers an' a', an' a'," }
%      \line { "Wi' a hundred pipers an' a', an' a'," }
%      \line { "We'll up an' gie 'em a blaw, a blaw," }
%      \line { "Wi' a hundred pipers an' a', an' a'." }
%
%      \vspace #0.8
%
%      \line { "The Esk was swollen, sae red, sae deep;" }
%      \line { "But shouther to shouther the brave lads keep;" }
%      \line { "Twa thousand swam ower to fell English ground," }
%      \line { "An' danc'd themselves dry to the pilbroch's sound." }
%      \line { "Dumfounder'd, the English saw, they saw!" }
%      \line { "Dumfounder'd, they heard the blaw, the blaw!" }
%      \line { "Dumfounder'd, they a' ran awa', awa'" }
%      \line { "Frae the hundred pipers an' a', an' a'!" }
%      \line { "Wi' a hundred pipers an' a', an' a'," }
%      \line { "Wi' a hundred pipers an' a', an' a'," }
%      \line { "We'll up an' gie 'em a blaw, a blaw," }
%      \line { "Wi' a hundred pipers an' a', an' a'." }
%    }
%  }
%}
