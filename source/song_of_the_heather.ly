% The Song of the Heather
% Transcribed by Kevin Cole <ubuntourist@hacdc.org> 2021.09.06 (kjc)
%
% Source: The Celtic Song Book, p. i
%         https://archive.org/details/the-celtic-song-book/page/n3/mode/1up

\version "2.22.1"
\language "english"

\paper {
  oddHeaderMarkup = \markup \null
  evenHeaderMarkup = \markup \null
  page-breaking = #ly:one-page-breaking

  #(define fonts
     (set-global-fonts
      #:roman "Open Sans"
      #:sans "Open Sans"
      #:typewriter "DejaVu Sans Mono"
      ; unnecessary if the staff size is default
      #:factor (/ staff-height pt 20)
      ))
}

\header {
  title = \markup { "The Song of the Heather" }
  tagline = \markup {
    \left-column {
      \draw-hline
      \wordwrap {
        \super { "*" }
        This song has been translated by the leading Irish, Scotch Gaelic, Manx, Welsh, Breton, and Cornish poets into their native languages for singing in each Celtic country and at Pan-Celtic Festivals.
      }
    }
  }
  subtitle = "(A Rallying Song of the Six Celtic Nations)"
  arranger = \markup {
    \center-column {
      "Arranged by CHARLES WOOD"
      "from Brian Boru's March."
      \italic
      "(By special permission of Messrs. Boosey.)"
    }
  }
  instrument = \markup { \smallCaps { "Soprano" } }
% copyright = "copyright"
% composer = "composer"
% metre = "metre"
% opus = "opus"
% piece = "piece"
% poet = "poet"
% texidoc = "All header fields with special meanings."
% enteredby = "KJC"
% source = "urtext"
}

\layout {
  \autoBeamOff
}

global = {
  \key bf \major
  \time 3/4
  \tempo \markup { \italic "Allegro energico." } 4 = 140
% \override TupletBracket.tuplet-slur = ##t              % Not available til v. 2.21
  \override TupletBracket.bracket-visibility = ##t
% \tupletUp
}

melody = {
  \relative {
    \global
    \partial 4 r4                             | %   0
    R2.                                       | %   1
    r4 r4 d''8^\f([ c8])                      | %   2
    bf4. a8 g4                                | %   3
    g2 d'8([ c8])                             | %   4

    \break

    bf4. a8 g4                                | %   5
    g2 c8([ bf8])                             | %   6
    a4. g8 f4                                 | %   7
    f2 c'8([ bf8])                            | %   8

    \break

    a4. g8 f4                                 | %   9
    f2 d'8([ c8])                             | %  10
    bf4. a8 g4                                | %  11
    g2 d'8([ c8])                             | %  12

    \break

    bf4. a8 g4                                | %  13
    g2 g'8 g8                                 | %  14
    d4. ef8 d4                                | %  15
    d2 c4                                     | %  16

    \break

    bf8 g4. g4                                | %  17
    g2 r4                                     | %  18
    R2.                                       | %  19
    r4 r4 f4^\mf                              | %  20
    g4 bf4 c4                                 | %  21

    \break

    d2 \tuplet 3/2 { ef8( d8) c8 }            | %  22
    d4 d4 \tuplet 3/2 { ef8( d8 c8) }         | %  23
    d4 c4 a4                                  | %  24

    \break

    f4 a4 bf4                                 | %  25
    c2 \tuplet 3/2 { d8( c8) a8 }             | %  26
    c2 \tuplet 3/2 { d8( c8) a8 }             | %  27
    c4 a4 f4^\f                               | %  28

    \break

    g4 bf4 c4                                 | %  29
    d2 \tuplet 3/2 { ef8( d8) c8 }            | %  30
    d4 d4 \tuplet 3/2 { ef8( d8 c8) }         | %  31
    d4 bf4 g4                                 | %  32

    \break

    bf4^\markup { \italic "marcato" } a4 g4   | %  33
    d'2.~                                     | %  34
    d2.~                                      | %  35
    d2 \tuplet 3/2 { ef8( d8) c8 }            | %  36
    bf8 g4. g4                                | %  37
    g2 r4                                       %  38

    \bar "|."
  }
}

words = \lyricmode {
  \override LyricSpace.minimum-distance = #1.5

  A  blos -- som there blows
  That scoffs at the snows,
  And fa -- ces, root -- fast,
  The rage of the blast;
  Yet sweet -- ens a sod
  No slave ev -- er trod,
  Since the moun -- tains up -- reared
  Their al -- tars to God.
  That flow'r of the free
  Is the hea -- ther, the hea -- ther! It
  springs where the sea And the land leap to -- geth -- er. Six
  Na -- tions are we, Yet be -- neath its bright fea -- ther,
  To -- day we are one............................... Where -- so -- ev -- er we be.
}

\score {
  <<
    \new Staff \with {midiInstrument = #"flute"} { \melody }
    \addlyrics { \words }
  >>
}

\markup {
  \draw-hline
}

\markup {
  \fill-line {
    \hspace #0.1  % Increase left margin
    \column {
      \combine \null \vspace #0.8 % adds vertical spacing between verses
      \line {
        \column {
          "Our blossom is red"
          "As the life-blood we shed,"
          "For Liberty's cause,"
          "Against ailen laws;"
          "When Lochiel and O'Neill"
          "And Llewellyn drew steel"
          "For Alba's and Erin's"
          "And Cambria's weal."
        }
      }
      \combine \null \vspace #0.8 % adds vertical spacing between verses
      \line {
        \column {
          "Then our couch, when we tired,"
          "Was the heather, the heather!"
          "Its becaon we fired"
          "In blue and black weather;"
          "Its mead-cup inspired,"
          "When we pledged it together"
          "To the Prince of our choice"
          "Or the maid most admired."
        }
      }
    }

    \raise #1 \draw-line #'(0 . -52) \hspace #10

    \column {
      \combine \null \vspace #0.8 % adds vertical spacing between verses
      \line {
        \column {
          "Let the Saxon and Dane"
          "Bear rule o'er the plain,"
          "On the hem of God's robe"
          "Is our sceptre and globe;"
          "For the Lord of all Light"
          "Stood revealed on the height,"
          "And to Heaven from the Mount"
          "Rose up in men's sight."
        }
      }
      \combine \null \vspace #0.8 % adds vertical spacing between verses
      \line {
        \column {
          "And the blossom and bud"
          "Of the heather, the heather,"
          "Is like His dear blood,"
          "Dropped hither and thither,"
          "From all evil to purge"
          "And evermore urge"
          "Each son of the Celt"
          "To the goal of all good"
        }
      }
    }
  }
}

\markup {
  \fill-line {
    \hspace #0.1  % Increase left margin
    \column {
      \combine \null \vspace #0.8 % adds vertical spacing between verses
      \line { ALFRED PERCEVAL GRAVES. }
    }
  }
}
