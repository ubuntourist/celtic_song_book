.. Page iii (PDF Page 5, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

#######
PREFACE
#######

The idea of bringing out a selection of Folk Songs of the Six Celtic
Nations |---| the Irish, Scots, and Manx Gaels, and the Cymry of
Wales, Cornwall, and Brittany |---| was mooted by me at the first
Pan-Celtic Congress held in Dublin in 1901. It is at last realized in
this volume.

Within its necessarily narrow limits, it has not been easy to do
complete justice to the main objects I set before me when compiling
it. These were to put into print the most beautiful typical airs of
the Celtic nations over the worthiest native words which had been
written to them, and to provide as satisfactory English or French
translations of these as I could obtain for those of my readers who
were not Breton or Welsh or Manx or Scottish Gaelic scholars: for at
the outset I had learnt from leading authorities that so far no Gaelic
words had been found worthy to mate the Irish melodies Moore and other
Anglo-Irish lyricists had helped to immortalize. Hence the Irish
language had to be regarded as for the time being lyrically derelict
for my purpose.

Most of the Welsh songs had already been partnered by English words;
for a few that remained without what appeared to me adequate
translations, I have endeavoured to supply them.

The Breton songs selected are provided with fitting French or English
translations; the French Breton and Breton songs of Théodore Botrel
and François Jaffrenou (*Taldir*) have, of course, been left to stand
alone.

The Highland and Island airs, owing to the use of traditional native
words |---| or thanks to living Scots Gaelic poets, such as Mr.
Kenneth McLeod, with whom Miss Frances Tolmie and Mrs. Kennedy Fraser
have been in touch |---| are largely provided with Gaelic words, and
some of these have been rendered into English by Miss Tolmie and
myself. I am, moreover, greatly indebted to Miss Lucy Broadwood and
Miss A. G. Gilchrist and above all to Mrs. Kennedy Fraser for allowing
me to use extracts from their authoritative essays on the special
characteristics of Scotch Gaelic music and how it may best be sung.

Though the language of Mann is not strongly represented in this
volume, it has yet been found possible to provide Manx words of merit
for not a few of its songs, and for these translations are given by
Miss Mona Douglas, Mr. Philip Caine, and myself.

I owe much to Professor Diverres for his authoritative account of his
country's music which introduces the Breton section of this volume, as
well as for his discriminating assistance in the choice of Breton airs
and words, and the help he has giveen me when translating the latter.

Much asssistance of the same kind I have met with from Miss A. G.
Gilchrist, Miss Mona Douglas, and Mr. J. E. Quayle, B.Mus., when
choosing Manx airs, and from Mr. Henry Jenner when selecting the Folk
Songs in the Cornish Section and for his introduction to it, while to
Miss Frances Tolmie and Mrs. Kennedy Fraser I have to make most
grateful acknowledgment for the generous way in which they have put
airs of their selection at my service and advised me upon their origin
and their history. And here I must express my unfeigned sorrow at the
passing away last Christmas of Miss Tolmie, who will never be
forgotten for what she has done as one of the most enthusiastic and
intelligent preservers of Gaelic Folk Tunes and Folk Songs of our
times, and, indeed, of all time.

It only remains to express obligation to Messrs. Boosey for the use of
words of mine that have already appeared by their permission in "The
Irish Song Book," as also for the use of the words of "Father
O'Flynn," although not to the air as modified by Sir Charles Stanford,
but to the original air which I myself collected in County Kerry. To
the same firm I am obliged for the use of my words to the Manx and
Welsh Folk airs in this volume, and for those of "The Song of the
Heather."

I have to thank Messrs. Methuen and Co. for the use of ten of the airs
and accompanying words from the Baring-Gould's "The Songs of the
West"; Mrs. Kennedy Fraser for allowing me to print three of her
"Songs of the Outer Hebrides"; and the late Miss Frances Tolmie and
the Committee of the Folk Song Society for leave to use six of Miss
Tolmie's Highland Gaelic airs and words; Henry Lemoine and Company of
Paris for the use of seventeen of L.A. Bourgault-Ducondray's "Melodies
Populaires de Basse-Bretagne"; and, finally, the late Théodore Botrel
and François Jaffrenou, "Taldir" for Breton Folk Tunes collected by
them and adorned by their beautiful words.

----

(Source: `The Celtic Song Book, p. iii - iv
<https://archive.org/details/the-celtic-song-book/page/n4/mode/1up>`_)
