.. Page 181 (PDF Page 94, Right)

.. include:: <isonum.txt>
.. include:: /_static/isonum.txt

.. rst-class:: center

###############
WELSH FOLK SONG
###############

Welsh national music owes its origins to three sources |---| the use
of the harp, ballad singing, and the folk song proper, which, unlike
what remains of the art of the professional ballad singers, sprang
directly from the heart of the Welsh people through the lips of her
local poets, and was thoroughly Welsh in its characteristics.

Of these three influences upon Welsh music and song, that of the harp
had for long been the most potent. Not that the Welsh had been without
vocal excellence early in their history. On the contrary, there is
absolute evidence of the Welsh singing in four parts, as they still do
so readily and tunefully, as early as the days of Giraldus Cambrensis,
in the latter half of the twelfth century. For whilst giving the palm
for harp music to the Irish harpists of his day, he speaks of the
remarkable popularity of singing in parts amongst his own compatriots.
But the Welsh harp, which was originally of leather strung with wire,
afterwards of wood strung with hair, had developed by the fourteenth
century into an instrument of superior structure and of much greater
compass than the Irish harp. This adapted it to the diatonic scale,
and therefore enabled it to take on the finer developments of
mediaeval music. Side by side with the use of the Welsh harp for
merely instrumental purposes, as the medium of battle marches,
lamentations, and epithalamia, the Welsh or triple harp, which was
played with the left hand uppermost, was the vehicle for *penillon*
singing, specially cultivated by the North Welsh. It is not so
improbable that this very remarkable form of vocalization had its
origin on the battle-field, where the bards of the clan incited their
chieftains to heroic feats of arms to the accompaniment of the harp,
or even a band of harps. The musical situation down to comparatively
recent times in the Principality may be said, therefore, to have been
dominated by the harp. But such was the sense of melody inherent in
the Welsh harpist that many of his airs readily lent themselves to
song, though, strangely enough, their utilization in that respect,
except in a very few instances, came from outside Wales; and we have
the curious phenomenon of Thompson, a Scott, carrying harp tunes out
of Wales to be set to the words of Scottish and English lyrical
writers by German composers, such as Haydn and Beethoven. Meantime,
beautiful folk songs were being evolved by the people of South Wales
without any form of publication. These were carried |---| or, rather,
the airs were carried |---| into England by Welsh musicians, and there
published either separately or in connection with ballad operas. Thus
Welsh music crossed the borders of Wales, and became popular in Great
Britain and abroad.

Then followed a great setback to the use of the harp and the singing
of folk ballads and folk songs. That setback was caused by the Welsh
Methodist revival, which tabooed all dance music and practically all
secular songs. The converted Welsh ceased to sing their folk songs
except in remote parts of the country, and their children, therefore,
had no opportunity of learning them. There was some compensation,
however, in the introduction of the more serious airs into the service
of the Church, and the fact that beautiful Welsh airs are an outcome,
in a more solemn form, of Welsh diatonic and modal music.

But change came about the middle of last century. Two Welsh bards,
"Ceiriog" and "Talhairn," started writing beautiful Welsh words to the
harp and to folk melodies, which began to be collected by musical
experts. Brinley Richard's *Songs of Wales* embodies some of the best
of these Welsh efforts; but many of the English versions of the Welsh
words in that collection are either indifferent as verse or fail to
catch the Welsh national spirit, What were the earlier collections of
Welsh airs on which Welsh singers had thus to rely?

The first collection, compiled and arranged by John Parry, the blind
harpist of Ruabon, and Ivan Williams, a London teacher of music, came
out in three parts between the years 1742 and 1781.

In 1784 Edward Jones, Bardd y Brenin, the King's Bard, published *The
Relic* which, with *The Bardic Museum*, contained a large number of
melodies not previously published. Then came John Parry, or the Bard
Alaw, another London musician, and musical director of the Vauxhall
Gardens, who published volumes of Welsh melodies with English words by
Mrs. Hermans and other well-known writers, some of which were used in
his ballad operas, particularly in his "Welsh Girl" and the "Trip to
Wales." In 1809 appeared Dr. Crotch's *Forty Examples of Welsh Airs*
without words, and the first of the eight volumes published by the
Scot (Thompson) containing ninety airs, with lyrics written by
Englishmen, and arrangements by Haydn, Beethoven, and Kozeluch. Many
of these tunes were mutilated by Thompson. In 1829 appeared a
collection of harp tunes containing a number of unpublished airs,
badly arranged by their producer, Richard Roberts, the blind harpist
of Caernarvon, and execrable English words. To generalize this period:

All the collections are by North Walians, most of them harpists; the
melodies were harp tunes, either purely instrumental or adapted to the
singing of *penillion* in the peculiar North Welsh style. Many of the
tunes printed were of English origin, and there is an almost total
absense of Welsh words in these collections. But we know, as
subsequent collections prove, that there was a great store of folk
songs with words and tunes of their own existing at that time, more
especially in South Wales. Why were neither the melodies nor the words
recorded?

Principal Davies has shown that the simple lyrics were despised by the
regular bards because of the absence of all trace of cynghanedd, or
verse consonance. An analogous reason accounts for the neglect of the
tunes by the musician. The folk song only began to come into its own
when Miss Maria Jane Williams of Aberpergon, in the year 1844-45,
brought out two collections of hitherto unpublished songs which has
been sent to the Abergavenny Eisteddfod. For she obtained these songs
from the people, recorded them exactly as she got them, did not reject
those exhibiting ancient modes, and did not displace the original
words. The collection of John Thomas, of about the same date, has some
interesting tunes, but harks back to the old practice which omitted
the words of the songs and even tampered with the tunes, though the
accompaniments to them were simple and tasteful. The next period of
private folk-song gathering is concerned with the collections of
Brinley Richards, John Thomas Davidson, Owen Alaw (the *Gems of Welsh
Melody*), Dr. Joseph Parry, and above all, the interesting collection
of Nicholas Bennet and Emlyn Evans (1861), and the popular edition of
*Welsh National Songs* by the latter musician.

Mention has already been made of harp music specially suited to the
singing of *penillion*. This is a method of singing, not only
exclusively Welsh, but confessedly of great antiquity. The word is so
spelt as to tempt the rash versifier to rhyme it with "pillion" or
"postillion." Its method is even stranger than its spelling. A harper
plays some well-known Welsh melody; the singer after the first few
bars improvises a harmony to the air, timing his stanzas, or
*penillion*, so as to end each one with the close of the air. It will
be seen, therefore, that the harp does not accompany the voice |---|
the voice accompanies the harp. The instrument certainly leads, but
plays the same tune throughout, independently of the singer. But both
harper and singer must have a keen sense of rhythm, time and accent,
for the voice has to strike out at the proper beat or fraction of a
beat as the length of the metre may demand. If the singer fails to
accomplish this, he puts himself out of court. It is another
peculiarity of the *penillion* for the singer to render his verses in
a different time than from that of the melody itself. Thus the singer
will set metres in 6/8 time, while the harper plays in 3/4 time. Or,
again stanzas which demand a 6/8 rhythm may even be sung to common
time on the harp, the singing conveying to the ear what is called
"cross-accentuation," or striking against the beats of the melody.

*Penillion* singing has been hitherto almost entirely confined to
North Wales; but its introduction amongst the older children in the
schools throughout the Principality, and with surprising success,
makes it probable that it will become as popular in the South as in
North Wales.

As has been pointed out by Dr. Lloyd Williams and others, many of the
Welsh folk songs are sung to modal tunes, and are more especially in
the Dorian Mode.

In dealing with these tunes to be found amongst the older Welsh songs
one must be careful how to regard them, for they are *sui generis*.
Dr. Alfred Daniel very appositely deals with the question thus in a
valuable paper on vocal traditions in Wales in the Transactions of the
Honourable Society of Cymmrodorion fo the year 1909-10: "Take the
position of the Highland bagpiper. I wonder whether any of you have
ever heard the Old Hundredth played slowly on the great pipes in what
those learned in pipe music call the imperfect scale? I have, and the
memory of the affliction has lingered with me many a long year. Why
was it an affliction? Because the intervals of the scale on the pipes
were very different from those required in the Old Hundredth; but for
certain classes of pipe music |---| coronachs, pibrochs, laments |---|
the intervals in the so-called imperfect scale are simply and definitely
right. You cannot write 'imperfect scale' bagpipe music on the
'modulator scale' at all; and if you can do it on the staff notation,
it is only because the reproduction from the printed page is done on
the bagpipes. If you try to play a page of bagpipe music on the violin
|---| unless, indeed, you are a West Highland violinist |---| or try
to sing it, then the trained bagpiper who hears the reproduction
thoroughly despises it, and says it is wrong and has no 'bite' in
it. A bagpiper does not use the same scale as the tonic solfa-ist or
the staff-notation-ist; and musically he and they do not think the
same thoughts, have not the same preconceptions of what should be, do
not speak the same musical language, and, in short, are musically
aliens from one another. And yet each is right in his own place and
from his own point of view. Each is the product of his environment
and of a development which can be traced far back into history."

Curiously enough, in the highest Roman Catholic and Church of England
congregations the old English modes either prevail or struggle for
existence. In Welsh churches, and chapels even, choirs, harmoniums, or
organs now guide the singing; harmony in four parts and full chords
reign supreme, and the queer wail of the old unison singing of the
minor mode or Dorian tunes, and the thrill that this produced, have
almost become mere memories. For I am assured that to this day the
enlightened musical directors of the "Cymanfacedd," or the church
congregational singing, live in apprehension of the intervention of
aged voices singing in the old traditional scale, as I put it |---|
out of tune as they deem it |---| and disconcertingly introducing
undesired caco-coustical effects. It says much for the accuracy of ear
of the old folk, as well a for their indomitable persistence, that any
of them have been able to adhere to the old intervals under the
circumstances.

It wass my personal interest in the collection of Irish, English and
Manx folk songs, writing words to the tunes which were without them,
and the circumstance of finding myself across the Welsh border at
Harlech, that attracted me to the closer study of the beautiful Welsh
music. I had followed it with delight when played upon the harp by my
Irish wife, but I had not entered into its special characteristics,
nor had I till then possessed the means, since acquired, of knowing to
what extent Welsh folk song had been systematically collected.

I soon, however, made up my mind that Dr. Joseph Parry's suggestion
that his collection of songs |---| 180 Welsh folk songs |---| was the
last word as to Welsh airs and folk words worth collecting was an
altogehter erroneous assumption. I knew the Welsh to be a race of high
musical taste and old musical traditions, and I also surmised from my
experience of Irish and Manx folk-song collecting that Dr.  Parry's
180 Welsh airs probably only represented a twentieth part of the sound
body of Welsh folk music. I knew of some 6,000 Irish airs and of over
300 in the little Isle of Mann. Was Wales to be credited with only 180
of any value? I began to make enquiries. I got into communication with
Dr. Lloyd Williams, Director of Music at the University College of
Bangor, and its Principal, now Sir Harry Reichel, and found that, like
myself, they were not only beginning to doubt that Welsh folk songs
were so few, but had begun to give practical demonstration of the
opposite view by looking up manuscript collections of unpublished
Welsh airs, and taking down hitherto unrecorded Welsh folk songs from
the lips of Welsh folk singers. I had noted that Ceiriog Hughes, the
Welsh Burns, and the diligent collector of Welsh folk songs, had
spoken of 1,172 Welsh airs being in existence, of which only a
fraction had appeared in collections. Moreover, I had found that
Mrs. Mary Davies, best known as Mary Davis, the famous Welsh singer,
had been presented, as a wedding gift, with a collection of Welsh
airs, long lost, which I believe had obtained the second prize as a
collection of Welsh folk songs at the Llangollen Eisteddfod of 1858.

As a result of these investigations and the further co-operation of
Sir William Preece, Sir Harry Reichel, Dr. Lloyd Williams, Robert
Bryan, Llew Tegid, and others, it was decided to consider the question
of forming a Welsh folk-song society. There was another reason which
made the opportunity a specially appropriate one. The Board of
Education had wisely decided that for the future the folk songs of the
four nations of the United Kingdom should form the basis of the
musical instruction of the scholars in our public elementary schools,
and a list of the most suitable of folk and national songs belonging
to England, Scotland and Wales was drawn up. In this list some 35
Welsh airs were included in a total of 200; and Messrs. Boosey, the
publishers of Brinley Richard's collection of Welsh airs, undertook
the publication of *The National Song Book*, to be edited on these
lines, by the late Sir Charles Villiers Stanford. But when the
Welshmen and others, who were setting the Welsh folk-song movement
going, were consulted about the English and Welsh words to be included
in the Welsh section of this work, they pointed out that few of the
English words to the Welsh airs in Brinley Richard's collection were
worthy of the Welsh originals, and that some of the latter were not of
sufficient literary merit to deserve to stand side by side with the
lyrics of Burns, Moore, and other leading British and Irish
song-writers. Correspondence between the publishers and the Welsh
experts led to an arrangement for the provision in the Welsh section
of *The National Song Book* of English words of higher quality, and of
good Welsh words for those of the rediscovered folk tunes that were
without them.

Further *pourparlers* with the same publishers led to the production
by them of a series of Welsh melodies, arranged by Dr. Arthur
Somervell and Dr. Lloyd Williams, to Welsh words by the leading Welsh
lyricists, in which the spirit of the original should be as faithfully
followed as the Sassenach tongue permits. This series, in two parts,
was published by Messrs. Boosey, and at once leaped into public favor.
Meantime the Honourable Society of Cymmrodorion had got wind of what
we were about, and Sir Arthur Reichel and myself were invited to read
papers on Welsh folk song before one of its sections at the National
Eisteddfod in 1906. The meeting was held in the Carnarvon County Hall,
under the presidency of Sir William Pereece, K.C.B., F.R.S., and was
well attended. Sir Harry Reichel's paper was not musically illustrated,
but dealt comprehensively with the Welsh folk-song collections,
published and unpublished, lying open to musical treatment, and
containing within their pages a fine foundation upon which a national
school of Welsh music might be built. Upon this aspect of the value of
folk music Sir Harry Reichel laid the fullest stress, offering
interesting parallels of what had been done in this direction by the
great Continental composers.

My own paper was musically illustrated by a small party of singers,
chosen from the Eisteddfod choir, as well as by two distinguished
professional singers, Miss Grace Roberts, of Liverpool, and Mr.
Maldwyn Evans, of Bangor. The large audience were as delighted as they
were surprised by this production of a considerable group of Welsh
folk songs hitherto entirely unknown to them, contributed by the
activities of Dr. Lloyd Williams, Mr. Robert Bryan, Mr. Silyn Roberts,
and others.

The Welsh folk-song society thus started has never looked behind. It
has reached a membership of nearly four hundred folk-song lovers. Its
journal is edited with consumate skill by Dr. Lloyd Williams, and has
had the literary assistance of distinguished Welsh men of letters,
including Professor T. Gwyn Jones, of Aberystwyth. Year by year the
Welsh folk-song journal has appeared, and its pages contain hundreds
of freshly discovered Welsh folk tunes and words, made doubly
interesting by illuminating comments upon them, written by the leading
folk-song experts. Sir William Preece was the Society's first
President, and his daughter, Miss Amy Preece, acted with Mrs. Mary
Davies as joint secretary. His mantle has passed to Mrs. Mary Davies,
now deservedly a Doctor of Music, while Mrs. Herbert Lewis, now Lady
Lewis, succeeded her as the Society's secretary. These two ladies have
in addition done most important work in collecting folk songs in
different parts of the Principality, and lecturing upon them with
telling musical illustrations by vocalists whom they have taught the
true art of folk singing. Dr. Lloyd Williams, who began his folk-song
work by training his musical students at Bangor in the art of
folk-song collecting, has done much lecturing himself from an
Eisteddfod or university chair and from many a village platform.
Another acive folk-song collector is Mrs. Gwyneddon Davies; and the
enthusiasm of Mr. Phillip Thomas has done much to carry the best folk
songs into Welsh elementary schools.

The next step in this movement will no doubt be the appearance of
Welsh folk opera; and when instrumental music in Wales reaches the
level of its vocal music, a Welsh national opera, with a fine and full
national Welsh orchestra, may be confidently looked forward to.

ALFRED PERCEVAL GRAVES.

----

(Source: `The Celtic Song Book, p. 181 - 190
<https://archive.org/details/the-celtic-song-book/page/n93/mode/1up>`_)
