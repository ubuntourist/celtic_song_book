.. include:: <isonum.txt>
.. include:: /_static/isonum.txt
.. |wink| unicode:: U+1F609 .. WINKING FACE

.. role:: embiggen

.. rst-class:: center

#############################
PREFACE TO THE ONLINE EDITION
#############################

During the **Not So Great Pandemic of 2020 -** :embiggen:`∞`, I found
myself looking for sheet music to Celtic songs. (I don't recall if I
had a specific one in mind, or if it was a more generic search.) In
any case, I stumbled across a scanned copy of this aged, weather-worn
book, `The Celtic Song Book`_ which I then uploaded to the Internet
Archive.

I found it quite difficult to read in its scanned format. I had
previously considered learning to typeset music as a means of possibly
becoming facile enough to actually **read** scores. As is so often the
case, the Free / Libre Open Source Software community provides a
complier specifically designed for that purpose: `LilyPond`_.  I
decided, in a fit of whimsy that I will probably regret, that
converting the PDF to something considerably more readable |---| not
to mention with added audio tracks and links |---| was just the right
excuse to move forward with my learning. This document is the result.

In my completely unqualified opinion, the introductions have not
weathered the test of time well. First, the language seems a bit dated
|---| as I'm sure my prose will be eventually |---| with an
overabundance of nationalistic pride and self-pity. It was possibly
justified at the time of writing. Times have changed.

Second, I am guessing that the author submitted a hand-written
manuscript that was then dutifully typed in or typeset without the
benefit of an editor. The phrasing often seems unnecessarily
convoluted, with way too many run-on sentences, misplaced
prepositional phrases and commas. Egads! The commas! 😱

Nonetheless, I have attempted to remain faithful to the original
manuscript, even in places where I feel there are typographical errors
or other mistakes unless those mistakes are blatently obvious. I'm
merely a transcriber of the original work. I leave it to others to
spot typos and spelling errors that I have undoubtedly introduced, as
well as any factual mistakes in the original.

I believe the introductions will be of more interest to the historical
researcher rather than to folks like me whose main intent is to learn
the songs by having the tunes and lyrics in a readable form, along
with the added bonus of computer-generated audio for each score.  Most
of the introductions strike me as being the author quoting a colleague
who is quoting another colleague, with lots of opining and critiquing
of the songs included and excluded from this collection.

I don't have the energy or patience (yet) to chase down all of the various
works by the author's colleagues or lesser mortals who are snubbed. But,
again, the introductions provide a starting point for those so inclined.

As this was originally published in 1928, it is my understanding that
it has fallen into the public domain, and my additions are licenced
under the Creative Commons Share Alike with Attributions (`CC BY-SA
4.0`_). It is my sincere hope that this will attract the attention of
some actual scholars and become a living, growing document.

.. rst-class:: left

.. note::

    The bottom of each page includes a link to the first page of
    corresponding source in the PDF version hosted on the `Internet
    Archive`_

The tools
=========

For those wishing to contribute, the source **git** repository lives
on `Codeberg`_ because I rather like their `mission statement`_. I
used the following software tools:

.. table::
   :widths: 1 1
   :header-alignment: center center
   :column-alignment: left center
   :column-dividers:  single single single

   +-------------------------+---------+
   |          SOFTWARE       | VERSION |
   +=========================+=========+
   | `Python`_               | 3.8.10  |
   +-------------------------+---------+
   | `Sphinx`_               | 4.1.2   |
   +-------------------------+---------+
   | `sphinxnotes-lilypond`_ | 1.3     |
   +-------------------------+---------+
   | `cloud-sptheme`_        | 1.10.1  |
   +-------------------------+---------+
   | `GNU LilyPond`_         | 2.22.1  |
   +-------------------------+---------+
   | `Frescobaldi`_          | 3.0.0   |
   +-------------------------+---------+
   | `GNU Emacs`_ [*]_       | 26.3    |
   +-------------------------+---------+

.. note::

    A few caveats regarding the audio playback:

    1. The Lilypond extension to Sphinx does not handle repeats
       (voltas) in the audio production. So, the audio tracks don't
       repeat even if the scores say they should. One and done.

    2. The tempo markings were unhelpful to me, as "With feeling"
       vs. "With expression" doesn't tell me much about how the two
       differ. In order to generate audio, I had to put in specific
       BPM, and often just guessed. So, I imagine several of those
       could use improvement. (For example, "Clare's Dragoons" sounds
       great fast, but I don't know if it's singable at that
       speed. Others seem to drag at the slow speeds I've set.) NOTE:
       The ellipsis at the end of the playback bar offers the ability
       to change the speed of playback based on multiples of the speed
       I've chosen (e.g. 0.5, 0.75, 1.50, etc.).

    3. The default instrument is piano, but listening to it play those
       tunes back on piano just sounded all wrong. I tried setting it
       them to violin, which was better, but ultimately, flute felt
       best of all |---| possibly due to a certain bias. |wink| Even
       so, due to the nature of the software being more about
       typesetting than audio, there are tiny "gaps" in the audio,
       making some longer notes sound like two or more shorter
       notes. I didn't try all the possible instruments, and the
       instrumentation is chosen on a song-by-song basis. So, if
       there's a majority vote to change particular tunes to other
       instruments, that can be accommodated... The choice of
       instruments can be found at `Lilypond Notation - A.6 MIDI
       instruments`_

----

KEVIN COLE, 2021

.. [*] The One True Editor |wink|
.. _The Celtic Song Book: https://archive.org/details/the-celtic-song-book
.. _Internet Archive: https://archive.org/
.. _LilyPond: https://lilypond.org/
.. _CC BY-SA 4.0: https://creativecommons.org/licenses/by-sa/4.0/
.. _Codeberg: https://codeberg.org/ubuntourist/celtic_song_book/
.. _mission statement: https://blog.codeberg.org/codebergorg-launched.html
.. _Python: https://www.python.org/downloads/
.. _Sphinx: https://pypi.org/project/Sphinx/
.. _sphinxnotes-lilypond: https://pypi.org/project/sphinxnotes-lilypond/
.. _cloud-sptheme: https://pypi.org/project/cloud_sptheme/
.. _GNU LilyPond: https://lilypond.org/download.html
.. _Frescobaldi: https://frescobaldi.org/download
.. _GNU Emacs: https://www.gnu.org/software/emacs/download.html#gnu-linux
.. _Lilypond Notation - A.6 MIDI instruments:  https://lilypond.org/doc/v2.22/Documentation/notation/midi-instruments
